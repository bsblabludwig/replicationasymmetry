function result = fcn_combineMatrixIntoStatistics(numeratorType1, numeratorType2, denominatorType1, denominatorType2)

if (sum(numeratorType1(:) > denominatorType1(:))>0 || sum(numeratorType2(:) > denominatorType2(:))>0)
    fprintf('numeratorType1:\n')
    numeratorType1
    fprintf('denominatorType1:\n')
    denominatorType1
    fprintf('numeratorType2:\n')
    numeratorType2
    fprintf('denominatorType2:\n')
    denominatorType2    
    error('ERROR: more mutations than locations where they could occur.\n');
end
nSamples = size(numeratorType1, 2);

ratioType1 = numeratorType1./denominatorType1;
ratioType2 = numeratorType2./denominatorType2;
result.mutType1 = sum(numeratorType1, 1)';
result.mutType2 = sum(numeratorType2, 1)';
result.mfType1 = (sum(numeratorType1, 1)./sum(denominatorType1, 1))';
result.mfType2 = (sum(numeratorType2, 1)./sum(denominatorType2, 1))';
result.asymmetryDifference = (nanmean(ratioType1 - ratioType2, 1))';
result.asymmetryPValue = NaN*ones(nSamples, 1);
for iSample = 1:nSamples
    result.asymmetryPValue(iSample) = signtest(...
        ratioType1((~isnan(ratioType1(:,iSample)) & ~isnan(ratioType2(:,iSample))), iSample), ...
        ratioType2((~isnan(ratioType1(:,iSample)) & ~isnan(ratioType2(:,iSample))), iSample));
end

%%
% numeratorType1 = 2*magic(6); numeratorType1 = numeratorType1(1:5,1:3)
% numeratorType2 = 2*numeratorType1
% denominatorType1 = 2*numeratorType1
% denominatorType2 = 3*numeratorType2