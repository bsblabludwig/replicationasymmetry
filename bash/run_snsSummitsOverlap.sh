#!/bin/bash

##################################################
# SGE SUBMISSION SCRIPT run_snsSummitsOverlap.sh #
##################################################

# Run in current working directory
#$ -cwd
# Use bash
#$ -S /bin/bash
# Export all variables present at job submission time
#$ -V
# Merge STDOUT and STDERR
#$ -j y
# Set the output file for STDOUT
#

# What is the name of your job?
#
#$ -N snsSummitsOverlap

# Set the destination email address
#$ -M marketa.tomkova@ndm.ox.ac.uk
# Set the conditions under which you wish to receive an email (b:begin, e:end, a:aborted, s:suspended)
#$ -m eas

#####################################
# SGE DIRECTIVES - Always set these
#####################################

# Expected RUNTIME
#
# Enter the expected runtime for your job. Specification of a shorter run time
# will cause the scheduler to be more likely to schedule your job sooner, but
# note that your job will be terminated if it exceeds the specified runtime.
#
# Format: hours:minutes:seconds, e.g.: 72:0:0
#$ -l h_rt=10504:00:00

# Expected HARD MEMORY LIMIT (Per Slot)
#
# Enter the expected memory usage of your job.  Specification of a
# smaller memory requirement will cause the scheduler to be more
# likely to schedule your job sooner, but note that your job will be
# terminated if it exceeds the specified allocation.
#
# NB. This is the per slot memory limit, e.g. for a 2 slot job a 4G
# request would use 8G in total.
#
# Format: Memory required, defaults to MB, e.g. specify 4096 or 4G
# for 4 gigabytes.
#$ -l h_vmem=16G

# Specify parallel environment.
#
# Request parallel environment followed by the number of slots required.
# For example 'smp-verbose 2' will allocate 2 slots for the job. The
# more slots requsted the longer the job could take to launch and
# remember that you can't request more slots than your highest slot
# machine allows. 
#$ -pe smp 1

############################
# LOAD ENVIRONMENT MODULES
############################
# e.g.:
# module load apps/bowtie2/2.1.0/gcc-4.4.7	# module load apps/bowtie2/2.2.3/gcc-4.4.7
# module load apps/samtools/1.0
# module load apps/bedtools/2.20.1
# module load apps/cutadapt/1.5
# module load apps/R/3.1.1
# module load apps/oraclejdk/8.0.20/bin
# module load apps/bowtie/1.1.0/gcc-4.4.7
# module load apps/fastqc/0.11.2/noarch
# module load apps/macs/2.1.0/gcc-4.4.7+numpy-1.8.0


###############################
# APPLICATION LAUNCH COMMANDS
###############################

echo "Script run_snsSummitsOverlap.sh ..."

RUN_BASIC=false
RUN_MERGE_AND_INTERSECT=true

DIRECTORY="Besnard"
OUTPUT_DIRECTORY_MAPPED=$DIRECTORY"/output03mapped_Foulk_200_5_50" 
OUTPUT_FILE_BASIC=$OUTPUT_DIRECTORY_MAPPED"/intersectedSummits.bed"
TMP_FILE="${OUTPUT_DIRECTORY_MAPPED}/${DIRECTORY}_tmp_summits.bed"
MERGED_BEDS=$OUTPUT_DIRECTORY_MAPPED"/mergedPeaks.bed"
MERGED_AND_INTERSECTED_BEDS=$OUTPUT_DIRECTORY_MAPPED"/mergedPeaks_intersectedWithSamples.bed"
FINAL_SUMMITS=$OUTPUT_DIRECTORY_MAPPED"/summitsInOverlap.txt"
FINAL_ORI=$OUTPUT_DIRECTORY_MAPPED"/ORI_inMajority_${DIRECTORY}_Foulk_200_5_50.bed"

module load apps/bedtools/2.20.1



########### BASIC #############
if [ "$RUN_MERGE_AND_INTERSECT" == true ]; then		
	####### For each sample, create bedfile with region of the peak and position of the summit
	for SAMPLE_NAME_SUFFIX in "HeLa_rep1" "HeLa_rep2" "iPS_rep1" "iPS_rep2" "hESC_rep1" "hESC_rep2" "IMR_rep1" "IMR_rep2"; do #"HeLa_rep1" "HeLa_rep2" "iPS_rep1" "iPS_rep2" "hESC_rep1" "hESC_rep2" "IMR_rep1" "IMR_rep2"
		PEAKS_XLS="${OUTPUT_DIRECTORY_MAPPED}/${DIRECTORY}_${SAMPLE_NAME_SUFFIX}_peaks.xls"
		PEAKS_BED_OUTPUT="${OUTPUT_DIRECTORY_MAPPED}/${DIRECTORY}_${SAMPLE_NAME_SUFFIX}_peaks.bed"
		tail -n +28 $PEAKS_XLS | cut -f 1,2,3,5 > $PEAKS_BED_OUTPUT	# Originally grep -v "^#" $PEAKS_XLS  (but empty lines and header remained) # WAS 24!!!
		sh printFile.sh $PEAKS_BED_OUTPUT
	done
	##### PEAKS_XLS:
	#### chr	start	end	length	abs_summit	pileup	-log10(pvalue)	fold_enrichment	-log10(qvalue)	name
	#### chr1	11827	11906	80	11853	13.00	4.93416	3.21691	2.65457	Besnard_hESC_rep2_peak_1
	#### chr1	12058	12243	186	12122	25.00	12.17046	5.00770	9.37429	Besnard_hESC_rep2_peak_2
	#### chr1	12455	12604	150	12546	30.00	12.27944	4.41847	9.47833	Besnard_hESC_rep2_peak_3
	
	####### Merge all the regions (of all samples) into one bedfile
	echo -n "" > $MERGED_BEDS
	for SAMPLE_NAME_SUFFIX in "HeLa_rep1" "HeLa_rep2" "iPS_rep1" "iPS_rep2" "hESC_rep1" "hESC_rep2" "IMR_rep1" "IMR_rep2"; do #"HeLa_rep1" "HeLa_rep2" "iPS_rep1" "iPS_rep2" "hESC_rep1" "hESC_rep2" "IMR_rep1" "IMR_rep2"
		PEAKS_BED_OUTPUT="${OUTPUT_DIRECTORY_MAPPED}/${DIRECTORY}_${SAMPLE_NAME_SUFFIX}_peaks.bed"
		cut -f 1,2,3 $PEAKS_BED_OUTPUT >> $MERGED_BEDS
	done 
	sh printFile.sh $MERGED_BEDS
	
	####### Sort and use bedtools merge for finalisation of the merged bedfile.
	sort -k1,1 -k2,2n $MERGED_BEDS > $MERGED_BEDS".tmp"
	bedtools merge -i $MERGED_BEDS".tmp" > $MERGED_BEDS
	rm $MERGED_BEDS".tmp"
	sh printFile.sh $MERGED_BEDS
	
	####### Intersect the merged file with each of the samples.
	listFiles="${MERGED_BEDS}"
	for SAMPLE_NAME_SUFFIX in "HeLa_rep1" "HeLa_rep2" "iPS_rep1" "iPS_rep2" "hESC_rep1" "hESC_rep2" "IMR_rep1" "IMR_rep2"; do #"HeLa_rep1" "HeLa_rep2" "iPS_rep1" "iPS_rep2" "hESC_rep1" "hESC_rep2" "IMR_rep1" "IMR_rep2"
		PEAKS_BED_OUTPUT="${OUTPUT_DIRECTORY_MAPPED}/${DIRECTORY}_${SAMPLE_NAME_SUFFIX}_peaks.bed"
		MERGED_INTERSECTED_PEAKS_BED="${OUTPUT_DIRECTORY_MAPPED}/${DIRECTORY}_${SAMPLE_NAME_SUFFIX}_peaks_intersectWithMerged.bed"
		MERGED_INTERSECTED_PEAKS_SUMMITS_BED="${OUTPUT_DIRECTORY_MAPPED}/${DIRECTORY}_${SAMPLE_NAME_SUFFIX}_peaks_intersectWithMerged_onlySummits.bed"
		bedtools intersect -wa -wb -a $MERGED_BEDS -b $PEAKS_BED_OUTPUT -sorted -loj | sort -u -k1,1 -k2,2n > $MERGED_INTERSECTED_PEAKS_BED # If more features overlap, only one is kept.
		sh printFile.sh $MERGED_INTERSECTED_PEAKS_BED
		cut -f 7 $MERGED_INTERSECTED_PEAKS_BED > $MERGED_INTERSECTED_PEAKS_SUMMITS_BED
		sh printFile.sh $MERGED_INTERSECTED_PEAKS_SUMMITS_BED
		listFiles="${listFiles} $MERGED_INTERSECTED_PEAKS_SUMMITS_BED"
	done
	echo "listFiles: ${listFiles}"
	paste $listFiles > $MERGED_AND_INTERSECTED_BEDS
	sh printFile.sh $MERGED_AND_INTERSECTED_BEDS
	# ============
	# listFiles: Besnard/output03mapped_200_5_50/mergedPeaks.bed Besnard/output03mapped_200_5_50/Besnard_iPS_rep1_peaks_intersectWithMerged_onlySummits.bed Besnard/output03mapped_200_5_50/Besnard_iPS_rep2_peaks_intersectWithMerged_onlySummits.bed Besnard/output03mapped_200_5_50/Besnard_hESC_rep1_peaks_intersectWithMerged_onlySummits.bed Besnard/output03mapped_200_5_50/Besnard_hESC_rep2_peaks_intersectWithMerged_onlySummits.bed Besnard/output03mapped_200_5_50/Besnard_IMR_rep1_peaks_intersectWithMerged_onlySummits.bed Besnard/output03mapped_200_5_50/Besnard_IMR_rep2_peaks_intersectWithMerged_onlySummits.bed
	# FILE_NAME Besnard/output03mapped_200_5_50/mergedPeaks_intersectedWithSamples.bed:
	# 547869 Besnard/output03mapped_200_5_50/mergedPeaks_intersectedWithSamples.bed
	# chr1	10014	10471	10108	.	10152	.	10167	10179
	# chr1	11679	12288	12129	12120	11840	11853	12103	11855
	# chr1	12418	12744	12553	12557	12558	12546	12541	12562
	# chr1	13175	13283	13224	.	.	.	13235	13214
	# chr1	13349	13770	13412	13451	13467	.	13411	13447
	# chr1	16780	17775	17490	17496	17492	17477	17473	17499
	# chr1	18074	18353	18281	18280	18275	.	18281	18287
	# chr1	19277	19562	19472	19433	19457	.	19361	19358
	# chr1	21145	21577	21443	21307	21252	21315	21302	21236
	# chr1	21618	21712	.	.	21628	.	.	.
	# ...
	# chrY	59354372	59354678	59354448	.	59354468	.	59354447	59354612
	# chrY	59354921	59355956	59355475	59355278	59355595	59355265	59355466	59355474
	# chrY	59356561	59356699	59356608	.	.	.	59356607	59356627
	# chrY	59358966	59359422	59359317	.	.	.	59359317	59359342
	# chrY	59359489	59359624	.	.	.	.	59359498	59359547
	# chrY	59360093	59360287	59360209	59360191	59360190	59360202	59360218	59360204
	# chrY	59360461	59360721	59360519	59360568	59360569	.	59360528	59360544
	# chrY	59360777	59361013	59360848	59360885	59360876	.	.	.
	# chrY	59362325	59362530	59362432	59362420	59362403	.	59362472	.
	# chrY	59362788	59363599	59362904	59363275	59362901	59363542	59363226	59362886
	# ============


	####### Select peaks covered in five of six samples and print chromosome number and all the summits, i.e. 7 columns (then in matlab we will take median of them)
	awk '
	{chr=$1; peakStart=$2; peakEnd=$3; nSamplesWithPeak=0
	for (iField = 4; iField <= NF; iField++) {
		if ($iField == ".") {
			$iField = -1
		} else {
			nSamplesWithPeak = nSamplesWithPeak +1;
		}
	}	
	if (nSamplesWithPeak >= NF-4 && chr !~ "_" && chr !~ "M") {
		chrNumber = substr(chr, 4); 
		if (chrNumber == "X") {chrNumber = 23}
		if (chrNumber == "Y") {chrNumber = 24}
		$1 = chrNumber
		print
	}
	}' $MERGED_AND_INTERSECTED_BEDS > $FINAL_SUMMITS
	sh printFile.sh $FINAL_SUMMITS
	
	# FILE_NAME Besnard/output03mapped_200_5_50/summitsInOverlap.txt:
	# 99839 Besnard/output03mapped_200_5_50/summitsInOverlap.txt
	# 1 11679 12288 12129 12120 11840 11853 12103 11855
	# 1 12418 12744 12553 12557 12558 12546 12541 12562
	# 1 13349 13770 13412 13451 13467 -1 13411 13447
	# 1 16780 17775 17490 17496 17492 17477 17473 17499
	# 1 18074 18353 18281 18280 18275 -1 18281 18287
	# 1 19277 19562 19472 19433 19457 -1 19361 19358
	# 1 21145 21577 21443 21307 21252 21315 21302 21236
	# 1 29080 29418 29209 29235 29210 -1 29205 29227
	# 1 29497 29702 29625 29593 29582 29567 29639 -1
	# 1 36404 36975 36564 36597 36578 36623 36650 36555
	# ...
	# 24 59016955 59017323 59017076 59017046 59017074 -1 59017121 59017096
	# 24 59032491 59032887 59032576 -1 59032559 59032702 59032721 59032615
	# 24 59240127 59241156 59240944 59240264 59240245 59240791 59240263 59240971
	# 24 59331719 59332152 59331898 -1 59331935 59331865 59331934 59331870
	# 24 59337711 59338034 59337854 59337877 59337851 -1 59337786 59337875
	# 24 59353146 59353488 59353274 59353343 59353324 59353293 59353365 59353335
	# 24 59354921 59355956 59355475 59355278 59355595 59355265 59355466 59355474
	# 24 59360093 59360287 59360209 59360191 59360190 59360202 59360218 59360204
	# 24 59360461 59360721 59360519 59360568 59360569 -1 59360528 59360544
	# 24 59362788 59363599 59362904 59363275 59362901 59363542 59363226 59362886
	# ============
	
	####### Select peaks covered in five of six samples and print the peaks into a bedfile
	awk '
	{chr=$1; peakStart=$2; peakEnd=$3; nSamplesWithPeak=0
	for (iField = 4; iField <= NF; iField++) {
		if ($iField == ".") {
			$iField = -1
		} else {
			nSamplesWithPeak = nSamplesWithPeak +1;
		}
	}	
	if (nSamplesWithPeak >= NF-4 && chr !~ "_" && chr !~ "M") {
		# chrNumber = substr(chr, 4); 
		# if (chrNumber == "X") {chrNumber = 23}
		# if (chrNumber == "Y") {chrNumber = 24}
		# $1 = chrNumber
		# print
		printf "%s\t%d\t%d\n", chr, peakStart, peakEnd
	}
	}' $MERGED_AND_INTERSECTED_BEDS > $FINAL_ORI
	sh printFile.sh $FINAL_ORI
fi


########### BASIC #############
if [ "$RUN_BASIC" == true ]; then	
	SAMPLE_NAME_SUFFIX="iPS_rep1"
	CURRENT_FILE1_orig="${OUTPUT_DIRECTORY_MAPPED}/${DIRECTORY}_${SAMPLE_NAME_SUFFIX}_summits.bed"
	CURRENT_FILE1="${OUTPUT_DIRECTORY_MAPPED}/${DIRECTORY}_${SAMPLE_NAME_SUFFIX}_summits.properBed"
	cut -f 1,2,3,5 $CURRENT_FILE1_orig > $CURRENT_FILE1
	sh printFile.sh $CURRENT_FILE1
	
	for SAMPLE_NAME_SUFFIX in "iPS_rep2" "hESC_rep1" "hESC_rep2" "IMR_rep1" "IMR_rep2"; do #"HeLa_rep1" "HeLa_rep2" "iPS_rep1" "iPS_rep2" "hESC_rep1" "hESC_rep2" "IMR_rep1" "IMR_rep2"
		CURRENT_FILE2_orig="${OUTPUT_DIRECTORY_MAPPED}/${DIRECTORY}_${SAMPLE_NAME_SUFFIX}_summits.bed"
		CURRENT_FILE2="${OUTPUT_DIRECTORY_MAPPED}/${DIRECTORY}_${SAMPLE_NAME_SUFFIX}_summits.properBed"
		cut -f 1,2,3,5 $CURRENT_FILE2_orig > $CURRENT_FILE2	
		sh printFile.sh $CURRENT_FILE2
		
		# Intersecting
		echo "Running ========== bedtools intersect -a $CURRENT_FILE1 -b $CURRENT_FILE2 > $OUTPUT_FILE_BASIC ========="
		bedtools intersect -a $CURRENT_FILE1 -b $CURRENT_FILE2 -sorted > $OUTPUT_FILE_BASIC		
		sh printFile.sh $OUTPUT_FILE_BASIC
		
		# Preparation for the next loop
		cp $OUTPUT_FILE_BASIC $TMP_FILE
		CURRENT_FILE1=$TMP_FILE
	done
	rm $TMP_FILE
fi

echo "Script run_snsSummitsOverlap.sh FINISHED."

