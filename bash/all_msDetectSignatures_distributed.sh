#!/bin/bash -l

################################################
# SCRIPT all_msDetectSignatures_distributed.sh #
################################################

OUTPUT_FILE="jobs_all_msDetectSignatures_distributed_"`date '+%Y_%m_%d-%H_%M'`".txt"

echo "Running all_msDetectSignatures_distributed.sh for the following files:" >> $OUTPUT_FILE

function run_signatures { # uses isStrandSpecific, totalIterationsPerCore, nRuns, inputFileName, expName
	echo "Running run_signatures $isStrandSpecific $totalIterationsPerCore $nRuns $inputFileName $expName..."
	if [ "$isStrandSpecific" == "true" ]; then
		strandText="strandSpec"
	else
		strandText="strandUnspec"
	fi
	
	for numberProcessesToExtract in $(seq 2 1 7); do 
		JOBS_ALL_RUNS="x"
		runDataFilePrefix="'res_${expName}_${numberProcessesToExtract}k_${strandText}_${nRuns}runs'"
		
		JOB_ID_msDetectSignatures_distributed=`qsub -hold_jid $JOBS_ALL_RUNS -terse run_msDetectSignatures_distributed.sh $totalIterationsPerCore $numberProcessesToExtract $inputFileName $nRuns $runDataFilePrefix $isStrandSpecific`
		echo "$JOB_ID_msDetectSignatures_distributed: qsub -hold_jid $JOBS_ALL_RUNS -terse run_msDetectSignatures_distributed.sh $totalIterationsPerCore $numberProcessesToExtract $inputFileName $nRuns $runDataFilePrefix $isStrandSpecific" >> $OUTPUT_FILE
	done
}

function run_strandSpecificSignatures { # uses  totalIterationsPerCore, nRuns, expName
	isStrandSpecific="true"
	inputFileName="'input/${expName}_replication_asymmetry_genomes_LeadLagg_separately.mat'"
	run_signatures
}


function run_strandUnspecificSignatures { # uses  totalIterationsPerCore, nRuns, expName
	isStrandSpecific="false"
	inputFileName="'input/${expName}_replication_asymmetry_genomes_LeadLagg_together.mat'"
	run_signatures
}


totalIterationsPerCore=10
nRuns=100


expName="MSI_19"; run_strandSpecificSignatures; run_strandUnspecificSignatures;
expName="POLE_14"; run_strandSpecificSignatures; run_strandUnspecificSignatures;
expName="blood_lymphoid_274"; run_strandSpecificSignatures; run_strandUnspecificSignatures;
expName="bone_98"; run_strandSpecificSignatures; run_strandUnspecificSignatures;
expName="breast_560"; run_strandSpecificSignatures; run_strandUnspecificSignatures; 
expName="colorectum_35"; run_strandSpecificSignatures; run_strandUnspecificSignatures;  
expName="gastric_90"; run_strandSpecificSignatures; run_strandUnspecificSignatures;  
expName="liver_303"; run_strandSpecificSignatures; run_strandUnspecificSignatures;   
expName="lung_squamous_36"; run_strandSpecificSignatures; run_strandUnspecificSignatures;    
expName="oesophagus_adenocarcinoma_219"; run_strandSpecificSignatures; run_strandUnspecificSignatures;     
expName="oesophagus_squamous_7"; run_strandSpecificSignatures; run_strandUnspecificSignatures;    
expName="oral_25"; run_strandSpecificSignatures; run_strandUnspecificSignatures;    
expName="ovary_93"; run_strandSpecificSignatures; run_strandUnspecificSignatures;     
expName="pancreas_406"; run_strandSpecificSignatures; run_strandUnspecificSignatures;     
expName="prostate_294"; run_strandSpecificSignatures; run_strandUnspecificSignatures;     
expName="thyroid_26"; run_strandSpecificSignatures; run_strandUnspecificSignatures;     
expName="lung_adenocarcinoma_24"; run_strandSpecificSignatures; run_strandUnspecificSignatures;      
expName="kidney_clear_cell_95"; run_strandSpecificSignatures; run_strandUnspecificSignatures;     
expName="blood_myeloid_56"; run_strandSpecificSignatures; run_strandUnspecificSignatures;      
expName="brain_237"; run_strandSpecificSignatures; run_strandUnspecificSignatures;      
expName="skin_183"; run_strandSpecificSignatures; run_strandUnspecificSignatures;      

echo "Script all_msDetectSignatures_distributed.sh FINISHED." >> $OUTPUT_FILE



cat $OUTPUT_FILE
