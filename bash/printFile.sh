#!/bin/bash -l

# function printFile {
	FILE_NAME=$1
	# Print FILE_NAME
	echo "FILE_NAME ${FILE_NAME}:"
	wc -l $FILE_NAME
	head $FILE_NAME
	echo "..."
	tail $FILE_NAME
	echo "============"
# }