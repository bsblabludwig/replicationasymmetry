#!/bin/bash -l

#######################################
# SGE SUBMISSION SCRIPT run_snsMap.sh #
#######################################

# Run in current working directory
#$ -cwd
# Use bash
#$ -S /bin/bash
# Export all variables present at job submission time
#$ -V
# Merge STDOUT and STDERR
#$ -j y
# Set the output file for STDOUT
#

# What is the name of your job?
#
#$ -N snsMap

# Set the destination email address
#$ -M marketa.tomkova@ndm.ox.ac.uk
# Set the conditions under which you wish to receive an email (b:begin, e:end, a:aborted, s:suspended)
#$ -m eas

#####################################
# SGE DIRECTIVES - Always set these
#####################################

# Expected RUNTIME
#
# Enter the expected runtime for your job. Specification of a shorter run time
# will cause the scheduler to be more likely to schedule your job sooner, but
# note that your job will be terminated if it exceeds the specified runtime.
#
# Format: hours:minutes:seconds, e.g.: 72:0:0
#$ -l h_rt=10504:00:00

# Expected HARD MEMORY LIMIT (Per Slot)
#
# Enter the expected memory usage of your job.  Specification of a
# smaller memory requirement will cause the scheduler to be more
# likely to schedule your job sooner, but note that your job will be
# terminated if it exceeds the specified allocation.
#
# NB. This is the per slot memory limit, e.g. for a 2 slot job a 4G
# request would use 8G in total.
#
# Format: Memory required, defaults to MB, e.g. specify 4096 or 4G
# for 4 gigabytes.
#$ -l h_vmem=16G

# Specify parallel environment.
#
# Request parallel environment followed by the number of slots required.
# For example 'smp-verbose 2' will allocate 2 slots for the job. The
# more slots requsted the longer the job could take to launch and
# remember that you can't request more slots than your highest slot
# machine allows. 
#$ -pe smp 7

############################
# LOAD ENVIRONMENT MODULES
############################
# e.g.:
# module load apps/bowtie2/2.1.0/gcc-4.4.7	# module load apps/bowtie2/2.2.3/gcc-4.4.7
# module load apps/samtools/1.0
# module load apps/bedtools/2.20.1
# module load apps/cutadapt/1.5
# module load apps/R/3.1.1
# module load apps/oraclejdk/8.0.20/bin
# module load apps/bowtie/1.1.0/gcc-4.4.7
# module load apps/fastqc/0.11.2/noarch
# module load apps/macs/2.1.0/gcc-4.4.7+numpy-1.8.0
module load apps/bamtools/2.3.0/gcc-4.4.7


###############################
# APPLICATION LAUNCH COMMANDS
###############################

DIRECTORY=$1
SAMPLE_NAME=$2
PAIRED=$3
GENOME_REFERENCE_PATH=$4
DIRECTIONAL=$5
GENOME_SIZES_UCSC=$6

if [ -z "$DIRECTORY" ] ||  [ -z "$SAMPLE_NAME" ] || [ -z "$PAIRED" ] || [ -z "$GENOME_REFERENCE_PATH" ] || [ -z "$DIRECTIONAL" ] || [ -z "$GENOME_SIZES_UCSC" ]; then
    echo "Not enough parameters." 1>&2
    exit 1
fi

echo "Script run_snsMap.sh $DIRECTORY $SAMPLE_NAME $PAIRED $GENOME_REFERENCE_PATH $DIRECTIONAL $GENOME_SIZES_UCSC ..."

SAMPLE_PATH=$DIRECTORY"/output02trimmed/"$SAMPLE_NAME
OUTPUT_DIRECTORY_LOGS=$DIRECTORY"/output00logs"
OUTPUT_DIRECTORY_MAPPED=$DIRECTORY"/output03mapped"
MAPPED_FILE=$OUTPUT_DIRECTORY_MAPPED"/${SAMPLE_NAME}_mapped.bam"
DIR_SORTED_COORDINATE=$DIRECTORY"/output04sortedByCoordinate"; 	[ -d $DIR_SORTED_COORDINATE ] || mkdir $DIR_SORTED_COORDINATE
FILE_SORTED_COORDINATE=$DIR_SORTED_COORDINATE"/${SAMPLE_NAME}_sortedByCoordinate.bam"
MEMORY_SIZE="-Xmx10G"
PICARD_JAR_DIRECTORY="/users/mtomkova/bin/picard-tools/picard-tools-2.1.0"

[ -d $OUTPUT_DIRECTORY_LOGS ] || mkdir $OUTPUT_DIRECTORY_LOGS
[ -d $OUTPUT_DIRECTORY_MAPPED ] || mkdir $OUTPUT_DIRECTORY_MAPPED

if [ "$DIRECTIONAL" == true ]; then
	PARAM_DIRECTIONAL=""
else
	PARAM_DIRECTIONAL="--non_directional"
fi

runMapping=false


### Preparation of Bowtie2 indexes for hg19/
# bowtie2-build "../hg19/hg19_full.fa" hg19_full_bowtie2
# ls *.bt2
# mv *.bt2 ../hg19/


######### MAPPING ##########
if [ "$runMapping" == true ]; then
	if [ "$PAIRED" == true ]; then
		#### Align: Align trimmed reads to the prepared reference genome ##### paired-end 
		echo "Running: ===========bowtie2 -p 3 -x ${GENOME_REFERENCE_PATH} -1 ${SAMPLE_PATH}_1_val_1.fq.gz -2 ${SAMPLE_PATH}_2_val_2.fq.gz | samtools view -bS - > ${MAPPED_FILE} ==========="
		bowtie2 -p 3 -x ${GENOME_REFERENCE_PATH} -1 ${SAMPLE_PATH}"_1_val_1.fq.gz" -2 ${SAMPLE_PATH}"_2_val_2.fq.gz" | samtools view -bS - > ${MAPPED_FILE}
	else
		##### Align: Align trimmed reads to the prepared reference genome ##### single-end -U 
		echo "Running: ===========bowtie2 -p 3 -x ${GENOME_REFERENCE_PATH} -U ${SAMPLE_PATH}_trimmed.fq.gz | samtools view -bS - > ${MAPPED_FILE} ==========="
		bowtie2 -p 3 -x ${GENOME_REFERENCE_PATH} -U ${SAMPLE_PATH}"_trimmed.fq.gz" | samtools view -bS - > ${MAPPED_FILE}
	fi
	
	MAX_RECORDS=`ulimit -n | awk '{if ($1 <= 1024) {print 10*500000} else {print 500000}}'`

	echo "Sorting java ${MEMORY_SIZE} -jar ${PICARD_JAR_DIRECTORY}/picard.jar SortSam INPUT=${MAPPED_FILE} OUTPUT=${FILE_SORTED_COORDINATE} SORT_ORDER=coordinate TMP_DIR=$DIR_SORTED_COORDINATE MAX_RECORDS_IN_RAM=$MAX_RECORDS"
	java $MEMORY_SIZE -jar "${PICARD_JAR_DIRECTORY}/picard.jar" SortSam INPUT=$MAPPED_FILE OUTPUT=$FILE_SORTED_COORDINATE SORT_ORDER=coordinate TMP_DIR=$DIR_SORTED_COORDINATE MAX_RECORDS_IN_RAM=$MAX_RECORDS

	echo "RUNNING samtools index $FILE_SORTED_COORDINATE..."
	samtools index $FILE_SORTED_COORDINATE
fi


echo "RUNNING bamtools split -in $FILE_SORTED_COORDINATE -reference  ..."
bamtools split -in $FILE_SORTED_COORDINATE -reference 

echo "Script run_snsMap.sh FINISHED."
