function decipherMutationalProcesses_distributed(totalIterationsPerCore, numberProcessesToExtract, inputFileName, nRuns, runDataFilePrefix, isStrandSpecific)
% Ludmil B. Alexandrov
% Cancer Genome Project
% Wellcome Trust Sanger Institute
% la2@sanger.ac.uk
%
% This software and its documentation are copyright 2012 by the
% Wellcome Trust Sanger Institute/Genome Research Limited. All rights are reserved.
% This software is supplied without any warranty or guaranteed support whatsoever.
% Neither the Wellcome Trust Sanger Institute nor Genome Research Limited
% is responsible for its use, misuse, or functionality.

addpath('source/');
addpath('plotting/');
%% Define function specific constants
processesDistance = 'cosine';
removeWeakMutationTypes = 0.01; % removes weak mutation types, i.e. reduces the dimmensions

%% Simple validation of the input params
% myCluster = parcluster('local')
% delete(myCluster.Jobs)

% parpool
% fprintf('Good, line 24.\n');
% parpoolObject = gcp % create a parpool
% % try
% fprintf('Good, line 27.\n');
%     if isempty(parpoolObject)
%         totalCoresAvailable = 0;
%     else
%         totalCoresAvailable = parpoolObject.NumWorkers;
%     end
%     %    totalCores = matlabpool('size');
%     if ( totalCoresAvailable == 0)
%         error( 'decipherMutationalProcesses: Please initialize a matlabpool!' );
%     end
% 
% if ( totalCoresAvailable < totalCores)
%     error( 'decipherMutationalProcesses: Please specify the number of cores lower or equal of available cores on the machine!' );
% end

if ( exist('totalIterationsPerCore', 'var') == 0 )
    error( 'decipherMutationalProcesses: Please specify the number of iterations that need to be performed for each lab!' );
end

if ( exist('numberProcessesToExtract', 'var') == 0 )
    error( 'decipherMutationalProcesses: Please specify the number of processes that need to be extracted!' );
end

if ( exist('inputFileName', 'var') == 0 )
    error( 'decipherMutationalProcesses: Please specify an input file!' );
end

if ( exist('nRuns', 'var') == 0 )
    error( 'decipherMutationalProcesses: Please specify the number of runs!' );
end

if ( exist('runDataFilePrefix', 'var') == 0 )
    error( 'decipherMutationalProcesses: Please specify a prefix (with path) for the intermediate files!' );
end

if ( exist('isStrandSpecific', 'var') == 0 )
    error( 'decipherMutationalProcesses: Please specify whether it is a strand specific version!' );
end

fprintf('Running decipherMutationalProcesses_distributed(%d, %d, %s, %d, %s, %d)...', totalIterationsPerCore, numberProcessesToExtract, inputFileName, nRuns, runDataFilePrefix, isStrandSpecific);

%% Load the input file and validate its fields
input = load(inputFileName);
if ( isfield(input, 'cancerType') == 0 || isfield(input, 'originalGenomes') == 0 ...
        || isfield(input, 'sampleNames') == 0 || isfield(input, 'subtypes') == 0 ...
        || isfield(input, 'types') == 0)
    error( 'decipherMutationalProcesses: Please specify an input file containing the variables: cancerType, originalGenomes, sampleNames, subtypes, types!' );
end

dataDirectory = 'output/'; % TODO CHECK DIR?
runDataFilePrefixPath = [dataDirectory, runDataFilePrefix];
outputFileName = [runDataFilePrefixPath, '.mat'];

[allProcesses, allExposures, genomeErrors, genomesReconstructed, idx, idxS, processes, processesStd, exposures, exposureStd, processStab, processStabAvg, clusterCompactness] = ... ...
    deconvolute_distributed(input.originalGenomes, totalIterationsPerCore, numberProcessesToExtract, processesDistance, removeWeakMutationTypes, nRuns, outputFileName, input);


% Record the stability and average Frobenius reconstruction error
csvwrite([runDataFilePrefixPath, '.stability.txt'], mean(processStabAvg));
csvwrite([runDataFilePrefixPath, '.reconstructionError.txt'], norm(input.originalGenomes - processes*exposures, 'fro'));


%% Plotting the signatures of mutational processes
if (isStrandSpecific)
    plotSignaturesWithStrandBias(processes, allProcesses, idx, input, runDataFilePrefixPath);
else
    plotSignatures(processes, input, allProcesses, idx, processStabAvg, runDataFilePrefixPath);
end

%% Plotting the signature exposures
plotSignaturesExposureInSamples(exposures, input, runDataFilePrefixPath);


%%
% delete(parpoolObject)
end