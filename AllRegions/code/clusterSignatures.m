%% Evalute process stability
function [centroidsSignatures, centroidSignaturesStd, idx, idxS, processStab, processStabAvg, clusterCompactness] = clusterSignatures(allSignatures, nTotalSignatures, nReplicates, distanceName)

% Based on evaluateStability.m by:
% Ludmil B. Alexandrov
% Cancer Genome Project
% Wellcome Trust Sanger Institute
% la2@sanger.ac.uk
%
% This software and its documentation are copyright 2012 by the
% Wellcome Trust Sanger Institute/Genome Research Limited. All rights are reserved.
% This software is supplied without any warranty or guaranteed support whatsoever. 
% Neither the Wellcome Trust Sanger Institute nor Genome Research Limited 
% is responsible for its use, misuse, or functionality.

                                          
  % Defining function constants
  BIG_NUMBER = 100;
  CONVERG_ITER = 10;
  CONVERG_CUTOFF = 0.005; % cosine distance
  TOTAL_INIT_CONDITIONS = 5;
  
  % Clustering mutational processes using custom clustering procedure
  minClusterDist = BIG_NUMBER;
  totalIter = size(allSignatures, 2) / nTotalSignatures;
  idx = zeros(size(allSignatures, 2), 1);
  clusterCompactness = zeros(nTotalSignatures, totalIter);
  iStartigDataSet = 1 : nTotalSignatures : size(allSignatures,2);
  iStartingDataSet = iStartigDataSet(randperm(totalIter));
  
  for iInitData = 1 : min(TOTAL_INIT_CONDITIONS, totalIter) % size(Wall, 2)
      iStartingData = iStartingDataSet(iInitData);
      iEnd = iStartingData + nTotalSignatures - 1;
      centroidsSignatures = allSignatures(:, iStartingData:iEnd);
      centroidsTest = rand(size(centroidsSignatures));
      countIRep = 0;
      
      for iRep = 1 : nReplicates
          allDist = squareform( pdist(cat(2, centroidsSignatures, allSignatures)', distanceName) );
          centroidDist = allDist( 1:size(centroidsSignatures, 2), (size(centroidsSignatures, 2)+1): size(allDist, 2) )';

          jRange = randperm(nTotalSignatures);
          for jIndex = 1 : nTotalSignatures
            j = jRange(jIndex);
            for i = 1 : nTotalSignatures : size(allSignatures, 2)
              iRange = i: (i + nTotalSignatures - 1);
                  [noUsed, Ind] = min( centroidDist(iRange, j) );
                  centroidDist(iRange(Ind), : ) = BIG_NUMBER;
                  idx( iRange(Ind) ) = j;      
             end
          end

          maxDistToNewCentroids = 0;
          for i = 1 : size(centroidsSignatures, 2);
            centroidsSignatures(:, i) = mean( allSignatures(:, idx == i), 2 );
            maxDistToNewCentroids = max(maxDistToNewCentroids, pdist( cat(2, centroidsSignatures(:, i), centroidsTest(:, i))', 'cosine'));
          end

          if (  maxDistToNewCentroids < CONVERG_CUTOFF )
              countIRep =  countIRep + 1;
          else
              countIRep = 0;
              centroidsTest = centroidsSignatures;
          end
          
          if (  countIRep == CONVERG_ITER )
             break;
          end

      end
      
      for i = 1 : size(centroidsSignatures, 2);
          clusterDist = squareform(pdist( cat(2, centroidsSignatures(:,i), allSignatures(:, idx ==i) )', distanceName));
          clusterCompactness(i, :) = clusterDist(1, 2:size(clusterDist, 2));
      end
     

      if ( minClusterDist > mean(clusterCompactness(:)) )
          centroidsFinal = centroidsSignatures;
          idxFinal = idx;
          clusterCompactnessFinal = clusterCompactness;
      end
  end
  
  centroidsSignatures = centroidsFinal';
  idx = idxFinal;
  clusterCompactness = clusterCompactnessFinal;
    
  centDist = mean( clusterCompactness, 2 );
  
  [noUsed, centDistInd] = sort(centDist, 'ascend'); % centDistSorted not used
  clusterCompactness = clusterCompactness(centDistInd,:);
  centroidsSignatures = centroidsSignatures(centDistInd, :);
  idxNew = idx;
  
  for i = 1 : nTotalSignatures
    idxNew(idx == centDistInd(i)) = i;
  end
  idx = idxNew;   
  
  if ( nTotalSignatures > 1)
      processStab = silhouette(allSignatures', idx, distanceName);
      processStabAvg = zeros(1, nTotalSignatures);
      for i = 1 : nTotalSignatures
        processStabAvg(i) = mean ( processStab(idx==i) ); 
      end
  else
      allDist = squareform( pdist(cat(2, centroidsSignatures', allSignatures)', distanceName) );
      processStab = 1 - allDist( 1:size(centroidsSignatures', 2), (size(centroidsSignatures', 2)+1): size(allDist, 2) )';
      processStabAvg = mean(processStab);
  end
  
  centroidSignaturesStd = zeros( size(centroidsSignatures) );
  for i = 1 : nTotalSignatures
      centroidSignaturesStd(i,:) = std( allSignatures(:, idx == i), [], 2 );
  end
  
  centroidsSignatures = centroidsSignatures';
  centroidSignaturesStd = centroidSignaturesStd';
  
  idxS = zeros ( size(idx) );
  for i = 1 : nTotalSignatures : size(allSignatures, 2)
      iEnd = i + nTotalSignatures - 1;
      idxG = idx(i : iEnd);
      
       for j = 1 : nTotalSignatures
          idxS(i + j - 1, :) = find(idxG == j);
       end
      
  end

end
