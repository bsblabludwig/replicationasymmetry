% function labelRepel(xValues, yValues, labels, fontSize, backgroundColor)

xValues = 1:10;
yValues = rand(10, 1);
labels = cellstr(num2str(xValues', 'N=%-d'));
fontSize = 12;
backgroundColor = [1,1,1];
fig = figure(1); clf;
plot(xValues, yValues, 'o', 'MarkerFaceColor', 'c');
figCurrent = gcf;
gcaCurrent = gca;
nLabels = length(labels);
if (size(xValues, 2) > 1)
    xValues = xValues';
end
if (size(yValues, 2) > 1)
    yValues = yValues';
end
hold on;
%%
% set(hLegend, 'Visible', 'off'); set(hLegend, 'Visible', 'on');
imgStruct = getframe(gcaCurrent); img3DMatrix = imgStruct.cdata/255;  %(2:end-1, 2:end-1, :)
imgIsBackgroundColor = (img3DMatrix(:,:,1) == backgroundColor(1) & img3DMatrix(:,:,2) == backgroundColor(2) & img3DMatrix(:,:,3) == backgroundColor(3));
imgIsBackgroundColor = (flipud(imgIsBackgroundColor))';
xLimVal = get(gcaCurrent, 'XLim'); yLimVal = get(gcaCurrent, 'YLim');
hRectangle = rectangle('Position', [xLimVal(1), yLimVal(1), xLimVal(2)-xLimVal(1), yLimVal(2)-yLimVal(1)], 'FaceColor', 'w', 'EdgeColor', 'none');

xMiddle = mean(xLimVal);
yMiddle = mean(yLimVal);
gca_xMin = xLimVal(1);
gca_xWidth = xLimVal(2) - xLimVal(1);
gca_yMin = yLimVal(1);
gca_yWidth = yLimVal(2) - yLimVal(1);

image_xWidth = size(imgIsBackgroundColor, 1);
image_yWidth = size(imgIsBackgroundColor, 2);

labelWidth = NaN*ones(nLabels, 1);
labelHeight = NaN*ones(nLabels, 1);
tableLabels = table(labels, labelWidth, labelHeight); clear labelWidth labelHeight
for iLabel = 1:nLabels
    % iLabel = 1;
    hText = text(xMiddle, yMiddle, labels{iLabel}, 'FontSize', fontSize); drawnow;
    tmp_imgStruct = getframe(gcaCurrent); tmp_img3DMatrix = tmp_imgStruct.cdata/255; %(2:end-1, 2:end-1, :)
    tmp_imgIsBackgroundColor = (tmp_img3DMatrix(:,:,1) == backgroundColor(1) & tmp_img3DMatrix(:,:,2) == backgroundColor(2) & tmp_img3DMatrix(:,:,3) == backgroundColor(3));
    
    xProjection = max(~tmp_imgIsBackgroundColor, [], 1);
    yProjection = max(~tmp_imgIsBackgroundColor, [], 2);
    % size(xProjection)
    % size(yProjection)
    xMin = find(xProjection, 1, 'first');
    xMax = find(xProjection, 1, 'last');
    yMin = find(yProjection, 1, 'first');
    yMax = find(yProjection, 1, 'last');
    
    tableLabels.labelWidth(iLabel) = round(xMax - xMin);
    tableLabels.labelHeight(iLabel) = round(yMax - yMin);
    
    delete(hText);
    
    
    % fig2 = figure(223); clf;
    % imagesc(imgIsBackgroundColor-tmp_imgIsBackgroundColor);
    % imagesc(tmp_imgIsBackgroundColor);
    % figure(figCurrent);
end
tableLabels.xValue = round(1+image_xWidth*(xValues-gca_xMin)/gca_xWidth);
tableLabels.yValue = round(1+image_yWidth*(yValues-gca_yMin)/gca_yWidth);
tableLabels.xValueOriginal = tableLabels.xValue;
tableLabels.yValueOriginal = tableLabels.yValue;

delete(hRectangle);

hTexts = text(xValues, yValues, labels, 'FontSize', fontSize);
%%
% matrixWorking = imgIsBackgroundColor;
% fig2 = figure(222); clf; colormap('gray'); imagesc(flipud(matrixWorking'));

tableLabels.xValue = tableLabels.xValueOriginal;
tableLabels.yValue = tableLabels.yValueOriginal;

xScale = 5;
yScale = 5;

isCurrentBackground = imgIsBackgroundColor;

for iStep = 1:10
    tableLabels.xForce = zeros(nLabels, 1);
    tableLabels.yForce = zeros(nLabels, 1);
    
    for iLabel = 1:nLabels-1
        isCurrentBackground = imgIsBackgroundColor;
        for jLabel = [1:iLabel-1,iLabel+1:nLabels]
            xIndices = round(tableLabels.xValue(jLabel)+(0:tableLabels.labelWidth(jLabel)));
            yIndices = 2+round(tableLabels.yValue(jLabel)+(-tableLabels.labelHeight(jLabel)/2:tableLabels.labelHeight(jLabel)/2));
            
            xIndices(xIndices<1 | xIndices > image_xWidth) = [];
            yIndices(yIndices<1 | yIndices > image_yWidth) = [];
            isCurrentBackground(xIndices, yIndices) = 0;
        end
        %     fig2 = figure(224); clf; colormap('gray'); imagesc(flipud(isCurrentBackground'));
        %     figure(figCurrent);
        
        xIndices = round(tableLabels.xValue(iLabel)+(0:tableLabels.labelWidth(iLabel)));
        yIndices = 2+round(tableLabels.yValue(iLabel)+(-tableLabels.labelHeight(iLabel)/2:tableLabels.labelHeight(iLabel)/2));
        
        xIndices(xIndices<1 | xIndices > image_xWidth) = [];
        yIndices(yIndices<1 | yIndices > image_yWidth) = [];
        % matrixWorking(xIndices, yIndices) = 0;
        % fig2 = figure(223); clf; colormap('gray'); imagesc(flipud(matrixWorking'));
        
        currentBox = isCurrentBackground(xIndices, yIndices);
        if (sum(~currentBox(:)) > 0)
            xProjection = sum(~currentBox, 1); xMean = sum((1:length(xProjection)).*xProjection)/sum(xProjection); xMid = length(xProjection)/2;
            yProjection = sum(~currentBox, 2); yMean = sum((1:length(yProjection))'.*yProjection)/sum(yProjection); yMid = length(yProjection)/2;
            xDirection = xMean-xMid;
            yDirection = yMean-yMid;
            forceLength = sqrt(xDirection^2 + yDirection^2);
            xDirection = xDirection/forceLength;
            yDirection = yDirection/forceLength;
            tableLabels.xForce(iLabel) = tableLabels.xForce(iLabel) + xScale*xDirection;
            tableLabels.yForce(iLabel) = tableLabels.yForce(iLabel) + yScale*yDirection;
        end
    end
    
    tableLabels.xValue = tableLabels.xValue + tableLabels.xForce;
    tableLabels.yValue = tableLabels.yValue + tableLabels.yForce;
    
    tableLabels.gca_xValue = gca_xMin + gca_xWidth*(tableLabels.xValue-1)/image_xWidth;
    tableLabels.gca_yValue = gca_yMin + gca_yWidth*(tableLabels.yValue-1)/image_yWidth;
    
    figure(figCurrent);
    delete(hTexts);
    hTexts = text(tableLabels.gca_xValue, tableLabels.gca_yValue, labels, 'FontSize', fontSize);
    if (exist('hStep', 'var'))
        delete(hStep);
    end
    hStep = text(gca_xMin, gca_yMin, sprintf('Step = %d', iStep), 'VerticalAlignment', 'bottom');
    drawnow; pause(0.5);
    
end
%%
figure(figCurrent);
delete(hTexts);
delete(hStep);
% hold off;

% fig2 = figure(222);
% imagesc(imgIsBackgroundColor);
% figure(figCurrent);

