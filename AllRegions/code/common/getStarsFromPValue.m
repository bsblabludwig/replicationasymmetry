function [nStars, starsText, pValue] = getStarsFromPValue(pValueL, pValueR, addTailDirection)

pValue = min(pValueL, pValueR);

pValue_oneStar = 0.05;
pValue_twoStars = 0.01;
pValue_threeStars = 0.001;

starsText = ''; nStars = 0;

if (pValue < pValue_oneStar)
    starsText = '*'; nStars = 1;
end
if (pValue < pValue_twoStars)
    starsText = '**'; nStars = 2;
end
if (pValue < pValue_threeStars)
    starsText = '***'; nStars = 3;
end

if (nargin < 3 || addTailDirection)
    if (pValueL < pValueR)
        starsText = ['L', starsText];
    else
        starsText = ['R', starsText];
    end
end