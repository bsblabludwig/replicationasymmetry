% function plotExposureResults_directionalSignatures(replData, territoriesSource, decomposedSignatures)

%%
% Here we will plot signature plot, H ori plot, B ori plot, repl timing barplot, histogram, etc.
nSignatures = replData.nSignatures;
signatureNames = replData.signatureNames;
% signatureShortNames = strrep(strrep(signatureNames, 'Signature ', ''), 'Signature', 'S');
signatureNames = strrep(signatureNames, 'Signature', 'Signature ');
tissueNames = unique(replData.tableCancerTypes.Tissue); nTissues = length(tissueNames);

finalSignatures = decomposedSignatures.finalSignatures;
finalSignatures.nSignatures = length(finalSignatures.signaturesNames);
structClusters = decomposedSignatures.structClusters;
types = decomposedSignatures.types;
subtypes = decomposedSignatures.subtypes;
strandSpecText = 'strandSpec';


%%
% imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_combined/']; createDir(imagesPath);
imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_finalFigures/']; createDir(imagesPath);
font_size = 12;
font_name = 'Helvetica';% 'Lucida Sans'; % Console
colours.leadingDark = [1,0,0];
colours.laggingDark = [0,0,1];
colours.leading = [1,.3,.3];
colours.lagging = [.3,.3,1];
colours.leadingNotSignif = (2+colours.leading)/3;
colours.laggingNotSignif = (2+colours.lagging)/3;
colours.titleSignificant = 0*[1,1,1];
colours.titleNotSignif = 0.7*[1,1,1];
colours.patternColormap = jet(6)*0.7;
colours.plus = [.75,.1,1];
colours.minus = [.1,.75,.5];
colours.tissuesColourmap = hsv(nTissues);
%%
% posTitle.xShift = 0.04;
% posTitle.yShift = 0.01;
% posTitle.width = 0.02;
% posTitle.height = 0.02;
% posTitle.fontSize = 18;
% posTitle.letters = {'a', 'b', 'c', 'd', 'e', 'f'};

iSignaturesType = 2;
nSignaturesPerPlot = 5;
if (iSignaturesType == 1)
    iFigS = 13;
else
    iFigS = 19;
end
for iFirstSignature = 1:nSignaturesPerPlot:nSignatures
    currentListSignatures = iFirstSignature:(iFirstSignature+nSignaturesPerPlot-1); iFigS = iFigS + 1;
    currentListSignatures(currentListSignatures>nSignatures) = [];
    nR = nSignaturesPerPlot; nC = 1; iS = 1; xS = 1; yS = 0.9; xB = 0.1; yB = 0.05; xM = 0.02; yM = 0.02;
    fig = figure(1); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition', [0 0 25 30],'units','normalized','outerposition',[0 0 1 1]);
    
    for iSignature = currentListSignatures
        positionVector = myGeneralSubplot(nR,nC,iS,xS,yS,xB,yB,xM,yM); iS = iS + 1; hold on; set(gca, 'Color', 'none'); pos.x = positionVector(1); pos.y = positionVector(2); pos.width = positionVector(3); pos.height = positionVector(4);
        plot_xLabel = false;
        plotAllMutationTypes = true;
        title_text = ''; %signatureNames{iSignature};
        yLabel_text = signatureNames{iSignature};
        maxYLimVal = 20;
        textScaleFactor = 0.8;
        if (iSignature == currentListSignatures(end))
            plot_xLabel = true;
        end
        
        if (iSignaturesType == 1)
            plot_signature([finalSignatures.splitSignaturesA(:,iSignature); finalSignatures.splitSignaturesB(:,iSignature)], subtypes.(strandSpecText), types.(strandSpecText), plot_xLabel, 'flipped', title_text, yLabel_text, plotAllMutationTypes, font_size+4, font_name, maxYLimVal, textScaleFactor);
            
        else
            iCluster = finalSignatures.clusterIndex(iSignature);
            lstSignatureNames = unique(structClusters.strandSpec.allSignatureCTs(structClusters.strandSpec.finalClusters == iCluster & ~structClusters.strandSpec.allSignatureIsOfficial));
            %title_text = strrep(strjoin(lstSignatureNames), '_', '\_');
            signatureToPlot = structClusters.strandSpec.finalCentroidsSignatures(:,iCluster); maxVal = 100*max(abs(signatureToPlot));
            plot_signature(signatureToPlot, subtypes.(strandSpecText), types.(strandSpecText), plot_xLabel, 'normal', title_text, yLabel_text, colours, font_size+4, font_name);
            ylim([0, maxVal]); set(gca, 'YTick', []);
        end
        %         axes('Position', [pos.x - posTitle.xShift, pos.y + pos.height + posTitle.yShift, posTitle.width, posTitle.height]); axis off;
        %         text(.5,.5,posTitle.letters{iS-1},'FontWeight', 'bold', 'FontSize', posTitle.fontSize+2, 'HorizontalAlignment', 'center');
    end
    mySaveAs(fig, imagesPath, sprintf('FigS%d', iFigS));
end