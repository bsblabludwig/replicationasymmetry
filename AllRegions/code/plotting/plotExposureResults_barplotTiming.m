function plotExposureResults_barplotTiming(replData, territoriesSource, signatureResults)

nSignatures = replData.nSignatures;
tissueNames = unique(replData.tableCancerTypes.Tissue); nTissues = length(tissueNames);
% patternNames = replData.patternNames;
% maxPattern = length(patternNames);
% structAllSamples = replData.structAllSamples;
tableAllSamples = replData.tableAllSamples;
% nTotalSamples = size(tableAllSamples, 1);
% signatureExposuresSimple = replData.signatureExposuresSimple;
nValues = replData.dataFields.nValues;
% signatureExposuresComplex = replData.signatureExposuresComplex;



colours.leading = [1,.3,.3];
colours.lagging = [.3,.3,1];
colours.leadingNotSignif = (2+colours.leading)/3;
colours.laggingNotSignif = (2+colours.lagging)/3;
colours.titleSignificant = 0*[1,1,1];
colours.titleNotSignif = 0.7*[1,1,1];
colours.patternColormap = jet(6)*0.7;
colours.tissuesColourmap = hsv(nTissues);
signatureNames = replData.signatureNames;
signatureExposuresSimple_LeadLagg = replData.signatureExposuresSimple.LeadLagg;


% imagesPath = ['images/',datestr(now, 'mmdd'),'_replPrepareTables/',territoriesSource,'/']; createDir(imagesPath);
imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_replPrepareTables/']; createDir(imagesPath);

% iTissue = 1;
% [~, listSamples] = sort(replData.tableAllSamples.Tissue);
% isSampleThisTissue = true(size(replData.tableAllSamples, 1), 1); %ismember(replData.tableAllSamples.Tissue, tissueNames{iTissue})';
% simpleType = 'LeadLagg';

% summedExposures = (signatureExposuresSimple.(simpleType).type1 + signatureExposuresSimple.(simpleType).type2);
% listSamples& summedExposures(iSignature,:) > 0

font_size = 12; font_name = 'Lucida Console';
%% BARPLOTS OF CORRELATION WITH REPLICATION TIMING ___________________________________________________
complexType = 'ReplicationTiming_LeadLagg';     nCurrentValues = nValues.ReplicationTiming_LeadLagg;    labelsExtremes = {'early', 'late'};
% complexType = 'NOS_LeadLagg';     nCurrentValues = nValues.NOS_LeadLagg;    labelsExtremes = {'low', 'high'};
typeNames = {'type1Minustype2', 'totalMF', 'type1', 'type2'};
printNames = {'leading - lagging', 'total exposure', 'leading', 'lagging'};
signatureExposuresComplex_cell = replData.signatureExposuresComplex.(complexType);
for iTissue = [-1,0,1,2]%:nTissues
    if (iTissue > 0)
        tissue = tissueNames{iTissue};
        isThisT = strcmp(tableAllSamples.Tissue, tissue)';
    elseif (iTissue < 0)
        tissue = 'notPOLE';
        isThisT = ~strcmp(tableAllSamples.Tissue, 'POLE')';
    else
        tissue = 'all tissues';
        isThisT = true(1, size(tableAllSamples, 1));
    end
    minMeanExposure = 10; % 1000
    for iType = 1:2%1:2%:4%1:2%1:2
        typeName = typeNames{iType};
        typeNamePrint = printNames{iType};
        
        fig = figure(2); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition',2*[0 0 30 20],'units','normalized','outerposition',[0 0 1 1]);
        nC = 5; nR = ceil(nSignatures/nC); iS = 1; xS = .9; yS = .7;
        for iSignature = 1:nSignatures
            myGeneralSubplot(nR,nC,iS,xS,yS); iS = iS + 1; hold on;
            isSampleUsed = isThisT & signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure; % Only samples which are exposed to this signature are taken into account.
            plot_yLabel = typeNamePrint;
            plot_xLabel = 'replication timing';
            if (strcmp(typeName, 'type1Minustype2'))
                title_text = sprintf('%s%s', signatureNames{iSignature}, signatureResults.direction{iSignature});
            else
                title_text = signatureNames{iSignature};
            end
            plot_replbar(signatureExposuresComplex_cell, iSignature, isSampleUsed, signatureResults, typeName, nCurrentValues, labelsExtremes, plot_xLabel, plot_yLabel, title_text, colours, font_size, font_name)
        end
        suptitle(strrep([territoriesSource, ' ', tissue, ' ', complexType], '_', ' '));
        mySaveAs(fig, imagesPath, ['signatureExposuresComplex_', territoriesSource, '_', complexType, '_', typeName, '_', tissue, '_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);
    end
end
%%
if (false)
    % iSignature = 16; signatureNames{iSignature}
    % matrixToPlot = NaN*ones(nValues.ReplicationTiming_LeadLagg, nTotalSamples);
    slopePerSignPatient = NaN*ones(nSignatures, nTotalSamples);
    interceptPerSignPatient = NaN*ones(nSignatures, nTotalSamples);
    pValPerSignPatient = NaN*ones(nSignatures, nTotalSamples);
    lateOverEarly = NaN*ones(nSignatures, nTotalSamples);
    minMeanExposure = 10;
    for iSignature = 1:nSignatures
        fprintf('%d: %s\n', iSignature, signatureNames{iSignature});
        for iSample = 1:nTotalSamples
            if (signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, iSample)>minMeanExposure)
                currentValues = NaN*ones(nValues.ReplicationTiming_LeadLagg, 1);
                for iValue = 1:nValues.ReplicationTiming_LeadLagg
                    currentValues(iValue) = signatureExposuresComplex.ReplicationTiming_LeadLagg{iValue}.totalMF(iSignature, iSample);
                end
                linearModel = fitlm((1:nValues.ReplicationTiming_LeadLagg), currentValues);
                slopePerSignPatient(iSignature, iSample) = linearModel.Coefficients{2,1};
                interceptPerSignPatient(iSignature, iSample) = linearModel.Coefficients{1,1};
                pValPerSignPatient(iSignature, iSample) = coefTest(linearModel, [0,1]); % the same as from corr(1:4, currentValues)
                lateOverEarly(iSignature, iSample) = linearModel.Fitted(end)/linearModel.Fitted(1);
            end
        end
    end
    %%
    fig = createMaximisedFigure(1); hold on;
    matrixToPlot = lateOverEarly; matrixToPlot(pValPerSignPatient >= 0.05) = NaN;
    imagesc(matrixToPlot(:,listSamples)); colorbar;
    
    set(gca, 'YTick', 1:nSignatures, 'YTickLabel', signatureNames, 'XTick', [], 'YDir', 'reverse');
    % for iTissue = 1:nTissues
    %     listSamplesInCT = find(strcmp(tableAllSamples.Tissue(listSamples), tissueNames{iTissue}));
    %     if (~isempty(listSamplesInCT))
    %         plot([min(listSamplesInCT), max(listSamplesInCT)], 0.8*[1,1], 'Color', colours.tissuesColourmap(iTissue,:)*0.95, 'LineWidth', 2);
    %         plot(0.5+max(listSamplesInCT)*[1,1], 0.5+[0, size(matrixToPlot, 1)], 'Color', colours.tissuesColourmap(iTissue,:)*0.95, 'LineWidth', 0.5);
    %         text(mean(listSamplesInCT), 0, strrep(tissueNames{iTissue},'_',' '), 'Rotation', 45, 'Color', colours.tissuesColourmap(iTissue,:)*0.95, 'VerticalAlignment', 'bottom');
    %     end
    % end
    mySaveAs(fig, imagesPath, ['slopeReplTiming_', territoriesSource, '_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);
end
%%