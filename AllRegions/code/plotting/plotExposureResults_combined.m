% function plotExposureResults_combined(replData, territoriesSource, decomposedSignatures, signatureExposuresComplex_cell_Besnard1k)

%%

%%
% Here we will plot signature plot, H ori plot, B ori plot, repl timing barplot, histogram, etc.
nSignatures = replData.nSignatures;
signatureNames = replData.signatureNames;
signatureShortNames = strrep(strrep(signatureNames, 'Signature ', ''), 'Signature', 'S');
signatureNames = strrep(signatureNames, 'Signature', 'Signature ');
tissueNames = unique(replData.tableCancerTypes.Tissue); nTissues = length(tissueNames);
tableAllSamples = replData.tableAllSamples;
signatureExposuresSimple_LeadLagg = replData.signatureExposuresSimple.LeadLagg;
signatureExposuresSimple_matrix = replData.signatureExposuresSimple.LeadLagg.type1Minustype2;
signatureExposuresComplex_cell_ORI_Haradhvala = replData.signatureExposuresComplex.DistanceFromORI_LeadLagg;

binSize_Haradhvala = 20000;
binSize_Besnard1k = 1000;
nORIValues_Haradhvala = length(signatureExposuresComplex_cell_ORI_Haradhvala); %replData.dataFields.nValues.DistanceFromORI_LeadLagg;
nORIValues_Besnard1k = length(signatureExposuresComplex_cell_Besnard1k);
ORI_text_Haradhvala = 'left/right transition';
ORI_text_Besnard = 'ORI';

finalSignatures = decomposedSignatures.finalSignatures;
finalSignatures.nSignatures = length(finalSignatures.signaturesNames);
types = decomposedSignatures.types;
subtypes = decomposedSignatures.subtypes;
strandSpecText = 'strandSpec';
%%
complexType = 'ReplicationTiming_LeadLagg';     nCurrentValues_RT = replData.dataFields.nValues.ReplicationTiming_LeadLagg;    labelsExtremes = {'early', 'late'};
% complexType = 'NOS_LeadLagg';     nCurrentValues = nValues.NOS_LeadLagg;    labelsExtremes = {'low', 'high'};
% typeNames = {'type1Minustype2', 'totalMF', 'type1', 'type2'};
% printNames = {'leading - lagging', 'total exposure', 'leading', 'lagging'};
signatureExposuresComplex_cell_RT_diff = replData.signatureExposuresComplex.(complexType);
%%
% imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_combined/']; createDir(imagesPath);
imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_finalFigures/']; createDir(imagesPath);
minMeanExposure = 10;
font_size = 12;
font_name = 'Helvetica';% 'Lucida Sans'; % Console
colours.leadingDark = [1,0,0];
colours.laggingDark = [0,0,1];
colours.leading = [1,.3,.3];
colours.lagging = [.3,.3,1];
colours.leadingNotSignif = (2+colours.leading)/3;
colours.laggingNotSignif = (2+colours.lagging)/3;
colours.titleSignificant = 0*[1,1,1];
colours.titleNotSignif = 0.7*[1,1,1];
colours.patternColormap = jet(6)*0.7;
colours.plus = [.75,.1,1];
colours.minus = [.1,.75,.5];
colours.tissuesColourmap = hsv(nTissues);
%% clear signatureResults
pValue = NaN*ones(nSignatures, 1);
signatureResults = table(pValue);
signatureResults.meanExposure = NaN*ones(nSignatures, 1);
signatureResults.medianExposure = NaN*ones(nSignatures, 1);
signatureResults.hasStrandAsymmetry = false(nSignatures, 1);
signatureResults.colourTitle = zeros(nSignatures, 3);
signatureResults.colourPositiveBars = zeros(nSignatures, 3);
signatureResults.colourNegativeBars = zeros(nSignatures, 3);
signatureResults.colourGCA = zeros(nSignatures, 3);
signatureResults.titleText = cell(nSignatures, 3);
for iSignature = 1:nSignatures
    isSampleUsed = signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure; % Only samples which are exposed to this signature are taken into account.
    exposuresDifference = signatureExposuresSimple_matrix(iSignature, isSampleUsed);
    signatureResults.pValue(iSignature) = signtest(exposuresDifference); %signrank
    signatureResults.meanExposure(iSignature) = mean(exposuresDifference);
    signatureResults.medianExposure(iSignature) = median(exposuresDifference);
end
[~, ~, ~, qValues]=fdr_bh(signatureResults.pValue);
signatureResults.qValue_BenjaminiHochberg = qValues;
signatureResults.qValue_Bonferroni = signatureResults.pValue*nSignatures;
signatureResults.isLeading = (signatureResults.qValue_Bonferroni < 0.05 & signatureResults.medianExposure > 0); %signatureResults.meanExposure > 0 &
signatureResults.isLagging = (signatureResults.qValue_Bonferroni < 0.05 & signatureResults.medianExposure < 0); %signatureResults.meanExposure < 0 &
signatureResults.hasStrandAsymmetry(signatureResults.isLeading | signatureResults.isLagging) = true;
for iSignature = 1:nSignatures
    if (signatureResults.isLeading(iSignature))
        signatureResults.titleText{iSignature} = sprintf('%s: leading %s', signatureNames{iSignature}, getPValueAsText(signatureResults.pValue(iSignature)));
        signatureResults.colourTitle(iSignature,:) = colours.leadingDark;
    elseif (signatureResults.isLagging(iSignature))
        signatureResults.titleText{iSignature} = sprintf('%s: lagging %s', signatureNames{iSignature}, getPValueAsText(signatureResults.pValue(iSignature)));
        signatureResults.colourTitle(iSignature,:) = colours.laggingDark;
    else
        signatureResults.titleText{iSignature} = sprintf('%s n.s.', signatureNames{iSignature});
        signatureResults.colourTitle(iSignature,:) = colours.titleNotSignif;
    end
    if (signatureResults.hasStrandAsymmetry(iSignature))
        signatureResults.colourPositiveBars(iSignature,:) = colours.leading;
        signatureResults.colourNegativeBars(iSignature,:) = colours.lagging;
        signatureResults.colourGCA(iSignature,:) = colours.titleSignificant;
    else
        signatureResults.colourPositiveBars(iSignature,:) = colours.leadingNotSignif;
        signatureResults.colourNegativeBars(iSignature,:) = colours.laggingNotSignif;
        signatureResults.colourGCA(iSignature,:) = colours.titleSignificant;%colours.titleNotSignif;
    end
end
%%
special_R23 = false;
% listSignatures = [12, 10, 17, 16, 4, 6]; %[12, 2, 16, 4, 7, 10, 13, 17, 25, 19, 20, 6, 14, 24, 29];
%     'Signature 1'     group
%     'Signature 2'     group APOBEC
%     'Signature 3'     group
%     'Signature 4'     group mutagen
%     'Signature 5'     group symmetric
%     'Signature 6'     group MSI
%     'Signature 7'     group mutagen
%     'Signature 8'     group symmetric
%     'Signature 9'     group
%     'Signature 10'     group POLE
%     'Signature 12'     group liver
%     'Signature 13'     group APOBEC
%     'Signature 14'     group POLE
%     'Signature 15'     group MSI
%     'Signature 16'     group liver
%     'Signature 17'     group mutagen
%     'Signature 18'     group POLE
%     'Signature 19'     group symmetric
%     'Signature 20'     group MSI
%     'Signature 21'     group MSI
%     'Signature 22'     group mutagen
%     'Signature 23'     group symmetric
%     'Signature 25'     group symmetric
%     'Signature 26'     group MSI
%     'Signature 28'     group POLE
%     'Signature  N1'     group
%     'Signature  N2'     group
%     'Signature  N3'     group mutagen
%     'Signature  N4'     group MSI
% listSignatures = [12, 10, 10, 16, 4, 6];
iL = 1; clear listSignatures;  % Good, 5+2+4+6+5+2+5=29
listSignatures{iL} = [2, 12];                   group_names{iL} = 'APOBEC signatures';  figName{iL} = 'Fig3'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1; 
listSignatures{iL} = [6, 10, 10, 7, 16, 5];     group_names{iL} = '';                   figName{iL} = 'Fig4'; specials_R23{iL} = true;  specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1; 
listSignatures{iL} = [6, 14, 19, 20, 24, 29];   group_names{iL} = 'MMR signatures';     figName{iL} = 'FigS2'; specials_R23{iL} = false; specific_tissues{iL} = {'MSI'}; tissues_only{iL} = true; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1; 
listSignatures{iL} = [6, 14, 19, 20, 24, 29];   group_names{iL} = 'MMR signatures';     figName{iL} = 'FigS3'; specials_R23{iL} = false; specific_tissues{iL} = {'MSI'}; tissues_only{iL} = false; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1; 
listSignatures{iL} = [10, 13, 17, 25];          group_names{iL} = 'POLE signatures';    figName{iL} = 'FigS4'; specials_R23{iL} = false; specific_tissues{iL} = {'POLE'}; tissues_only{iL} = true; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1; 
listSignatures{iL} = [10, 13, 17, 25];          group_names{iL} = 'POLE signatures';    figName{iL} = 'FigS5'; specials_R23{iL} = false; specific_tissues{iL} = {'POLE'}; tissues_only{iL} = false; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1; 
listSignatures{iL} = [4, 7, 16, 21, 28];        group_names{iL} = 'mutagen signatures'; figName{iL} = 'FigS6'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = true; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1; 
listSignatures{iL} = [4, 7, 16, 21, 28];        group_names{iL} = 'mutagen signatures'; figName{iL} = 'FigS7'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = true; iL = iL + 1; 
listSignatures{iL} = [11, 15];                  group_names{iL} = 'liver signatures';   figName{iL} = 'FigS8'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1; 
listSignatures{iL} = [1, 3, 9, 26, 27];         group_names{iL} = 'other signatures';   figName{iL} = 'FigS9'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1; 
listSignatures{iL} = [26, 27, 28, 29];          group_names{iL} = 'new signatures';     figName{iL} = 'FigS10'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1; 
listSignatures{iL} = [5, 8, 18, 22, 23];        group_names{iL} = 'no/little asymmetry';figName{iL} = 'FigS11'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1; 
% listSignatures{iL} = [6, 14, 19, 20, 24, 29];   group_names{iL} = 'MMR signatures';     figName{iL} = 'FigS'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1; 
% listSignatures{iL} = [7, 28];        group_names{iL} = 'mutagen signatures'; figName{iL} = 'FigS'; specials_R23{iL} = false; specific_tissues{iL} = {'skin'}; tissues_only{iL} = true; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1; 
% listSignatures{iL} = [7, 28];        group_names{iL} = 'mutagen signatures'; figName{iL} = 'FigS'; specials_R23{iL} = false; specific_tissues{iL} = {'skin'}; tissues_only{iL} = false; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1; 
% listSignatures{iL} = [4];        group_names{iL} = 'mutagen signatures'; figName{iL} = 'FigS'; specials_R23{iL} = false; specific_tissues{iL} = {'lung_adenocarcinoma','lung_squamous'}; tissues_only{iL} = true; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1; 
% listSignatures{iL} = [4];        group_names{iL} = 'mutagen signatures'; figName{iL} = 'FigS'; specials_R23{iL} = false; specific_tissues{iL} = {'lung_adenocarcinoma','lung_squamous'}; tissues_only{iL} = false; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1; 
% listSignatures{iL} = [21];        group_names{iL} = 'mutagen signatures'; figName{iL} = 'FigS'; specials_R23{iL} = false; specific_tissues{iL} = {'kidney_clear_cell'}; tissues_only{iL} = true; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1; 
% listSignatures{iL} = [21];        group_names{iL} = 'mutagen signatures'; figName{iL} = 'FigS'; specials_R23{iL} = false; specific_tissues{iL} = {'kidney_clear_cell'}; tissues_only{iL} = false; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1; 
% listSignatures{iL} = [16];        group_names{iL} = 'signature 17'; figName{iL} = 'FigS17'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = false; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1; 
% listSignatures{iL} = [12];        group_names{iL} = 'signature 13'; figName{iL} = 'FigS13'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = false; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1; 



for iList = 1:length(listSignatures)
    currentListSignatures = listSignatures{iList};
    group_name = group_names{iList};
    special_R23 = specials_R23{iList};
    specific_tissue = specific_tissues{iList};
    tissue_only = tissues_only{iList};
    % fig = createMaximisedFigure(1);
    nR_plus = 1;
    %     if (~isempty(group_name))
    %         nR_plus = 1;
    %     end
    
    %     nR = length(currentListSignatures); nC = 5; iS = 1; xS = 0.8; yS = 0.8; xB = 0.02; yB = 0.08*6/nR; xM = -0.03; yM = -0.01*6/nR;
    nR = length(currentListSignatures); nC = 5; iS = 1; xS = 0.8; yS = 0.8; xB = 0.02; yB = 0.081*log2(6)/max([1, log2(nR)]); xM = -0.03; yM = -0.01+(0.02/max([1, log2(nR)]));
    fig = figure(1); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 42 (nR+nR_plus)*4.4],'units','normalized','outerposition',[0 0 1 1]);
    
    maxYLimVal = 20;
    if (nR >= 4)
        legShiftHeightFactor = 2; textScaleFactor = 1.5;
    else
        legShiftHeightFactor = 4; textScaleFactor = 1.4;
    end
    plot_xLabel = false; plotAllMutationTypes = false;
    for iR = 1:nR
        iSignature = currentListSignatures(iR);
        title_text = ''; %signatureNames{iSignature};
        if (iSignature == currentListSignatures(end))
            plot_xLabel = true;
        end
        if (special_R23 && iR == 2)
            plot_quartile = false;
            tissue = 'POLE^{mut}';
            isThisT = strcmp(tableAllSamples.Tissue, 'POLE')';
        elseif (special_R23 && iR == 3)
            plot_quartile = true;
            tissue = 'POLE^{WT}';
            isThisT = ~strcmp(tableAllSamples.Tissue, 'POLE')';
            %     elseif (iR == 5)
            %         plot_quartile = false;
            %         tissue = 'lung';
            %         isThisT = ismember(tableAllSamples.Tissue, {'lung_adenocarcinoma';'lung_squamous'})';
            %     elseif (iR == 6)
            %         plot_quartile = false;
            %         tissue = 'MSI';
            %         isThisT = strcmp(tableAllSamples.Tissue, 'MSI')';
        elseif (mutagenGroupTissuesOnly{iList})
            plot_quartile = false;
            if (ismember(iSignature, [4]))
                specific_tissue = {'lung_adenocarcinoma','lung_squamous'}; tissue = 'lung';
            elseif (ismember(iSignature, [7, 28]))
                specific_tissue = {'skin'}; tissue = 'skin';
            elseif (ismember(iSignature, [21]))
                specific_tissue = {'kidney_clear_cell'}; tissue = 'kidney';
            elseif (ismember(iSignature, [16]))
                specific_tissue = {'oesophagus_adenocarcinoma', 'gastric'}; tissue = 'oesophagus, gastric';
            end
            %             tissue = speicific_tissue; %strjoin(specific_tissue);
            isThisT = ismember(tableAllSamples.Tissue, specific_tissue)';
        elseif (mutagenGroupOtherThanTissues{iList})
            plot_quartile = true;
            if (ismember(iSignature, [4]))
                specific_tissue = {'lung_adenocarcinoma','lung_squamous'}; tissue = 'not lung';
            elseif (ismember(iSignature, [7, 28]))
                specific_tissue = {'skin'}; tissue = 'not skin';
            elseif (ismember(iSignature, [21]))
                specific_tissue = {'kidney_clear_cell'}; tissue = 'not kidney';
            elseif (ismember(iSignature, [16]))
                specific_tissue = {'oesophagus_adenocarcinoma', 'gastric'}; tissue = 'not oesophagus, not gastric';
            end
            %             tissue = strcat('not_', specific_tissue);% ['not ', strjoin(specific_tissue)];
            isThisT = ~ismember(tableAllSamples.Tissue, specific_tissue)';
        elseif (~isempty(specific_tissue))
            if (tissue_only)
                plot_quartile = false;
                tissue = specific_tissue; %strjoin(specific_tissue);
                isThisT = ismember(tableAllSamples.Tissue, specific_tissue)';
            else
                plot_quartile = true;
                tissue = strcat('not_', specific_tissue);% ['not ', strjoin(specific_tissue)];
                isThisT = ~ismember(tableAllSamples.Tissue, specific_tissue)';
            end
        else
            plot_quartile = true;
            tissue = ''; %all samples
            isThisT = true(1, size(tableAllSamples, 1));
        end
        tissue = strrep(tissue, 'not_MSI', 'MSS');
        yLabel_text = signatureNames{iSignature}; %yLabel_text = [signatureShortNames{iSignature}, ' ', tissue];
        isSampleUsed = isThisT & signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure; % Only samples which are exposed to this signature are taken into account.
        for iType = 1:nC
            if (iType == 1)
                xS_minus = 0.01;
            else
                xS_minus = 0;
            end
            yB_divide= 1;
            if (special_R23 && iR == 3 && iType == 1)
                iS = iS + 1;
                continue
            elseif (special_R23 && iR == 2 && iType == 1)
                yB_divide = nR;
            end
            myGeneralSubplot(nR,nC,iS,xS-xS_minus,yS,xB,yB/yB_divide,xM,yM); iS = iS + 1; hold on; set(gca, 'Color', 'none');
            if (special_R23 && iR == 2 && iType == 1)
                gcaPos = get(gca, 'Position'); gcaPos(2) = gcaPos(2) - gcaPos(4)/2; set(gca, 'Position', gcaPos);
            end
            
            if (iType == 1)
                plot_signature([finalSignatures.splitSignaturesA(:,iSignature); finalSignatures.splitSignaturesB(:,iSignature)], subtypes.(strandSpecText), types.(strandSpecText), plot_xLabel, 'flipped', title_text, yLabel_text, colours, font_size+4, font_name, maxYLimVal, textScaleFactor);
                xVal = 100;
                if (special_R23 && iR == 2)
                    text(xVal, 18, 'POLE^{mut}', 'FontSize', font_size, 'FontName', font_name, 'FontWeight', 'bold', 'HorizontalAlignment', 'left', 'Rotation', 90);
                    text(xVal, -20, '  POLE^{WT}', 'FontSize', font_size, 'FontName', font_name, 'FontWeight', 'bold', 'HorizontalAlignment', 'center', 'Rotation', 90);
                elseif (~ismember(iList, [3, 7]))
                    text(xVal, 0, strrep(tissue, '_', ' '), 'FontSize', font_size, 'FontName', font_name, 'FontWeight', 'bold', 'HorizontalAlignment', 'center', 'Rotation', 90);%, 'Interpreter', 'latex');
                end
            elseif (iType == 2)
                plot_yLabel = true; plotMedianFilter= false;
                plot_oriplot(signatureExposuresComplex_cell_ORI_Haradhvala, iSignature, isSampleUsed, binSize_Haradhvala, nORIValues_Haradhvala, ORI_text_Haradhvala, plotMedianFilter, plot_xLabel, plot_yLabel, title_text, plotAllMutationTypes, font_size, font_name, legShiftHeightFactor);
                if (ismember(iList, [3, 7]))
                    if (iscell(tissue))
                        tissue = tissue{1};
                    end
                    ylabel({['{\bf ', strrep(tissue, '_', ' '), '}'], 'exposure'}, 'FontSize', font_size, 'FontName', font_name);
                end
            elseif (iType == 3)
                plot_yLabel = true; plotMedianFilter= false;
                plot_oriplot(signatureExposuresComplex_cell_Besnard1k, iSignature, isSampleUsed, binSize_Besnard1k, nORIValues_Besnard1k, ORI_text_Besnard, plotMedianFilter, plot_xLabel, plot_yLabel, title_text, colours, font_size, font_name, legShiftHeightFactor);
            elseif (iType == 4)
                plot_yLabel = true;
                plot_histogram(signatureExposuresSimple_matrix, iSignature, isSampleUsed, signatureResults, plot_quartile, plot_xLabel, plot_yLabel, title_text, colours, font_size, font_name)
            elseif (iType == 5)
                typeName_RT = 'double_type1_type2'; %'totalMF';
                yLabel_text = 'exposure';
                if (iSignature == currentListSignatures(end))
                    xLabel_text = 'replication timing';
                else
                    xLabel_text = '';
                end
                plot_replbar(signatureExposuresComplex_cell_RT_diff, iSignature, isSampleUsed, signatureResults, typeName_RT, nCurrentValues_RT, labelsExtremes, xLabel_text, yLabel_text, title_text, colours, font_size, font_name, legShiftHeightFactor)
                %         elseif (iType == 5)
                %             typeName_RT = 'type1Minustype2';
                %             yLabel_text = {'main - side', 'exposure'};
                %             if (iSignature == listSignatures(end))
                %                 xLabel_text = 'replication timing';
                %             else
                %                 xLabel_text = '';
                %             end
                %             plot_replbar(signatureExposuresComplex_cell_RT_diff, iSignature, isSampleUsed, signatureResults, typeName_RT, nCurrentValues_RT, labelsExtremes, xLabel_text, yLabel_text, title_text, colours, font_size)
            end
        end
    end
    % mySaveAs(fig, imagesPath, ['combined_', strandSpecText, '_', num2str(i)]);
    % suptitle(sprintf('%s: distribution of leading-lagging exposures to signatures in patients', strrep(territoriesSource, '_', ' ')));
    % mySaveAs(fig, imagesPath, ['combinedPlot_', territoriesSource, '_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);
    % suptitle(sprintf('%s mean exposure >= %d', strrep([territoriesSource], '_', ' '), minMeanExposure));
    %     if (~isempty(group_name))
    %         suptitle(group_name);
    %     end
    %     suptitle(' ');
    mySaveAs(fig, imagesPath, figName{iList}, true);
    %     mySaveAs(fig, imagesPath, ['combinedPlot_', strrep(group_name, '/', 'or'), '_', num2str(minMeanExposure), '_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);
end
%%
if (true)
    currentListSignatures = [6, 14, 19, 20, 24, 29];
    nR = 1; nC = length(currentListSignatures); iS = 1; xS = 0.8; yS = 0.8; xB = 0.05; yB = 0.08; xM = 0.1; yM = -0.01;
    fig = figure(1); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 nC*8 12],'units','normalized','outerposition',[0 0 1 1]);
    
    plot_yLabel = true; plotMedianFilter= true;
    for iSignature = currentListSignatures
        
        myGeneralSubplot(nR,nC,iS,xS,yS,xB,yB,xM,yM); iS = iS + 1; hold on; %set(gca, 'Color', 'none');
        isThisT = ~strcmp(tableAllSamples.Tissue, 'MSI')';
        tmpColours = colours;
        tmpColours.plus = [1, 0.5, 0.1];
        tmpColours.minus = [0.1, 0.5, 0.75];
        isSampleUsed = isThisT & signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure; % Only samples which are exposed to this signature are taken into account.
        hPlot_MSS = plot_oriplot(signatureExposuresComplex_cell_Besnard1k, iSignature, isSampleUsed, binSize_Besnard1k, nORIValues_Besnard1k, ORI_text_Besnard, plotMedianFilter, plot_xLabel, plot_yLabel, title_text, tmpColours, font_size+2, font_name);
        
        isThisT = strcmp(tableAllSamples.Tissue, 'MSI')';
        tmpColours = colours;
        isSampleUsed = isThisT & signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure; % Only samples which are exposed to this signature are taken into account.
        hPlot_MSI = plot_oriplot(signatureExposuresComplex_cell_Besnard1k, iSignature, isSampleUsed, binSize_Besnard1k, nORIValues_Besnard1k, ORI_text_Besnard, plotMedianFilter, plot_xLabel, plot_yLabel, title_text, tmpColours, font_size+2, font_name);
        legend(gca, 'off'); %ylim([0, 1e4]);
        title(signatureNames{iSignature}, 'FontSize', font_size+4, 'FontName', font_name);
        plot_yLabel = false;
    end
    h = legend([hPlot_MSS, hPlot_MSI], {'MSS plus', 'MSS minus', 'MSI plus', 'MSI minus'}, 'Location', 'East', 'FontSize', font_size+4, 'FontName', font_name);
    legPos = get(h, 'Position'); legPos(1) = legPos(1) + 1.5*legPos(3); set(h, 'Position', legPos);
    mySaveAs(fig, imagesPath, 'FigS13');
    %mySaveAs(fig, imagesPath, ['combinedMSIPlot_', num2str(minMeanExposure), '_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);
end
%%
% iSignature = 0; nR = 5; nC = 3;
% for i = 1%:ceil(finalSignatures.nSignatures/nR)
%     fig = createMaximisedFigure(i);
%     for j = 1:nR*nC
%         if (iSignature < finalSignatures.nSignatures)
%             iSignature = iSignature + 1;
%             subplot(nR,nC,j); %if (j == nR) plotTypes = true; end
%             title_text = finalSignatures.signaturesNames{iSignature};
%             plot_signature([finalSignatures.splitSignaturesA(:,iSignature); finalSignatures.splitSignaturesB(:,iSignature)], subtypes.(strandSpecText), types.(strandSpecText), plotTypes, 'flipped', title_text, font_size);
%         end
%     end
%     suptitle(sprintf('Final denormalised %d', i));
%     mySaveAs(fig, imagesPath, ['finalSignatures_denormalised_', strandSpecText, '_', num2str(i)]);
% end