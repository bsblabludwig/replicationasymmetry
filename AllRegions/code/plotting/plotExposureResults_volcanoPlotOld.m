% function plotExposureResults_volcanoPlot(replData, territoriesSource)


nSignatures = replData.nSignatures;
tissueNames = unique(replData.tableCancerTypes.Tissue); nTissues = length(tissueNames);
% patternNames = replData.patternNames;
% maxPattern = length(patternNames);
% structAllSamples = replData.structAllSamples;
% tableAllSamples = replData.tableAllSamples;
signatureExposuresSimple_LeadLagg = replData.signatureExposuresSimple.LeadLagg;
nSamples = size(replData.tableAllSamples, 1);

% colours.leading = [1,.3,.3];
% colours.lagging = [.3,.3,1];
% colours.leadingNotSignif = (2+colours.leading)/3;
% colours.laggingNotSignif = (2+colours.lagging)/3;
% colours.titleSignificant = 0*[1,1,1];
% colours.titleNotSignif = 0.7*[1,1,1];
% colours.patternColormap = jet(6)*0.7;
colours.tissuesColourmap = hsv(nTissues);
colours.tissuesMarkers = {'o', 's', 'p', 'd'}; nM = length(colours.tissuesMarkers);
signatureNames = replData.signatureNames;

% imagesPath = ['images/',datestr(now, 'mmdd'),'_replPrepareTables/',territoriesSource,'/']; createDir(imagesPath);
imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_scatterPlots/']; createDir(imagesPath);

% iTissue = 1;
% [~, listSamples] = sort(replData.tableAllSamples.Tissue);
% isSampleThisTissue = true(size(replData.tableAllSamples, 1), 1); %ismember(replData.tableAllSamples.Tissue, tissueNames{iTissue})';
% simpleType = 'LeadLagg';
%%
minMeanExposure = 10;%1000;
xVal_difference = signatureExposuresSimple_LeadLagg.type1Minustype2;
xVal_logRatio = log2(signatureExposuresSimple_LeadLagg.type1./signatureExposuresSimple_LeadLagg.type2);

mfTypes = {'type1', 'type2', 'totalMF'};
mfTypeNames = {'matching', 'inverse', 'total'};

nBins = length(replData.signatureExposuresComplex.ReplicationTiming_LeadLagg);
pValue = NaN*ones(nSignatures, 1);
medianDifference = NaN*ones(nSignatures, 1);
meanDifference = NaN*ones(nSignatures, 1);
medianLogRatio = NaN*ones(nSignatures, 1);
meanLogRatio = NaN*ones(nSignatures, 1);
nExposedSamples = NaN*ones(nSignatures, 1);
percentagePositiveSamples = NaN*ones(nSignatures, 1);
signatureNameShort = cell(nSignatures, 1);

tableSignatures = table(signatureNameShort, nExposedSamples, percentagePositiveSamples, pValue, medianDifference, meanDifference, medianLogRatio, meanLogRatio);
clear signatureNameShort nExposedSamples pValue medianDifference meanDifference medianLogRatio meanLogRatio

for iMFType = 1:3
    mfType = mfTypes{iMFType};
    mfTypeName = mfTypeNames{iMFType};
    tableSignatures.(['RT_pValue_', mfTypeName]) = NaN*ones(nSignatures, 1);
    tableSignatures.(['RT_slope_', mfTypeName]) = NaN*ones(nSignatures, 1);
    tableSignatures.(['RT_log2FC_', mfTypeName]) = NaN*ones(nSignatures, 1);
    tableSignatures.(['RT_percentagePositiveSlope_', mfTypeName]) = NaN*ones(nSignatures, 1);
    tableSignatures.(['RT_perSamples_pValue_', mfTypeName]) = NaN*ones(nSignatures, 1);
    tableSignatures.(['RT_perSamples_log2FCMedian_', mfTypeName]) = NaN*ones(nSignatures, 1);
    tableSignatures.(['RT_perSamples_log2FCMean_', mfTypeName]) = NaN*ones(nSignatures, 1);
    tableSignatures.(['RT_perSamples_percentagePositiveSlope_', mfTypeName]) = NaN*ones(nSignatures, 1);
end
% tableSignatures.RT_pValue_matching = NaN*ones(nSignatures, 1);
% tableSignatures.RT_slope_matching = NaN*ones(nSignatures, 1);
% tableSignatures.RT_log2FC_matching = NaN*ones(nSignatures, 1);
% tableSignatures.RT_percentagePositiveSlope_matching = NaN*ones(nSignatures, 1);
% tableSignatures.RT_pValue_inverse = NaN*ones(nSignatures, 1);
% tableSignatures.RT_slope_inverse = NaN*ones(nSignatures, 1);
% tableSignatures.RT_log2FC_inverse = NaN*ones(nSignatures, 1);
% tableSignatures.RT_percentagePositiveSlope_inverse = NaN*ones(nSignatures, 1);

fullRun = false;

for iSignature = 1:nSignatures
    fprintf('Signature %d...\n', iSignature);
    isOkSample = ~isnan(xVal_difference(iSignature,:)) & ~isinf(xVal_difference(iSignature,:)) & signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure; %~isPOLE' &
    tableSignatures.nExposedSamples(iSignature) = sum(isOkSample);
    tableSignatures.percentagePositiveSamples(iSignature) = 100*mean(xVal_difference(iSignature, isOkSample)>0);
    tableSignatures.pValue(iSignature) = signtest(xVal_difference(iSignature, isOkSample)); %signrank
    tableSignatures.medianDifference(iSignature) = nanmedian(xVal_difference(iSignature, isOkSample));
    tableSignatures.meanDifference(iSignature) = nanmean(xVal_difference(iSignature, isOkSample));
    tableSignatures.medianLogRatio(iSignature) = nanmedian(xVal_logRatio(iSignature, isOkSample));
    tableSignatures.meanLogRatio(iSignature) = nanmean(xVal_logRatio(iSignature, isOkSample));
    tableSignatures.signatureNameShort{iSignature} = strrep(signatureNames{iSignature}, 'Signature', '');
    
    if (fullRun)
        for iMFType = 1:3
            mfType = mfTypes{iMFType};
            mfTypeName = mfTypeNames{iMFType};
            replTimingExposures = NaN*ones(nBins, nSamples);
            for iBin = 1:nBins
                replTimingExposures(iBin,:) = replData.signatureExposuresComplex.ReplicationTiming_LeadLagg{iBin}.(mfType)(iSignature, :);
            end
            isPositiveSlope = replTimingExposures(1,:)<replTimingExposures(2,:) & replTimingExposures(2,:)<replTimingExposures(3,:) & replTimingExposures(3,:)<replTimingExposures(4,:);
            isNegativeSlope = replTimingExposures(1,:)>replTimingExposures(2,:) & replTimingExposures(2,:)>replTimingExposures(3,:) & replTimingExposures(3,:)>replTimingExposures(4,:);
            replTimingMeanExposures = nanmean(replTimingExposures(:,isOkSample), 2);
            if (sum(replTimingMeanExposures) == 0)
                replTimingMeanExposures = replTimingMeanExposures + 1;
            end
            linearModel = fitlm((1:nBins), replTimingMeanExposures);
            tableSignatures.(['RT_pValue_', mfTypeName])(iSignature) = coefTest(linearModel, [0,1]);
            tableSignatures.(['RT_slope_', mfTypeName])(iSignature) = linearModel.Coefficients{2,1};
            tableSignatures.(['RT_log2FC_', mfTypeName])(iSignature) = log2(max([1,linearModel.Fitted(end)])/max([1,linearModel.Fitted(1)])); %log2((linearModel.Fitted(end)+smallConstant)/(linearModel.Fitted(1)+smallConstant));
            tableSignatures.(['RT_percentagePositiveSlope_', mfTypeName])(iSignature) = 100*nanmean(isPositiveSlope(isOkSample & (isPositiveSlope | isNegativeSlope))>0);
            allSamplesLog2FC = NaN*ones(nSamples, 1);
            allSamplesPValue = NaN*ones(nSamples, 1);
            allSamplesSlope = NaN*ones(nSamples, 1);
            for iSample = find(isOkSample)
                replTimingCurrentExposures = replTimingExposures(:,iSample);
                if (sum(replTimingCurrentExposures) == 0)
                    replTimingCurrentExposures = replTimingCurrentExposures + 1;
                end
                linearModel = fitlm((1:nBins), replTimingCurrentExposures);
                allSamplesLog2FC(iSample) = log2(max([1,linearModel.Fitted(end)])/max([1,linearModel.Fitted(1)]));
                allSamplesPValue(iSample) = coefTest(linearModel, [0,1]);
                allSamplesSlope(iSample) = linearModel.Coefficients{2,1};
            end
            tableSignatures.(['RT_perSamples_pValue_', mfTypeName])(iSignature) = signtest(allSamplesLog2FC(isOkSample));
            tableSignatures.(['RT_perSamples_log2FCMedian_', mfTypeName])(iSignature) = nanmedian(allSamplesLog2FC(isOkSample));
            tableSignatures.(['RT_perSamples_log2FCMean_', mfTypeName])(iSignature) = nanmean(allSamplesLog2FC(isOkSample));
            tableSignatures.(['RT_perSamples_percentagePositiveSlope_', mfTypeName])(iSignature) = 100*nanmean(allSamplesSlope(isOkSample' & (allSamplesPValue < 0.05))>0);
        end
    end
end
tableSignatures.percentagePositiveSamplesMinus50 = tableSignatures.percentagePositiveSamples-50;
% [~, ~, ~, adj_p]=fdr_bh(tableSignatures.pValue);
% tableSignatures.qValue = adj_p;
% tableSignatures.negLogPValue = -log10(tableSignatures.pValue);
% tableSignatures.negLogQValue = -log10(tableSignatures.qValue);
% tableSignatures.isSignificant = tableSignatures.qValue < 0.05;

tableSignatures.negLogPValue = -log10(tableSignatures.pValue);
tableSignatures.negLogQValue = tableSignatures.negLogPValue;
tableSignatures.isSignificant = tableSignatures.pValue < 0.05/nSignatures;

for iMFType = 1:3
    mfTypeName = mfTypeNames{iMFType};
    tableSignatures.(['RT_negLogPValue_', mfTypeName]) = -log10(tableSignatures.(['RT_pValue_', mfTypeName]));
    tableSignatures.(['RT_negLogQValue_', mfTypeName]) = tableSignatures.(['RT_negLogPValue_', mfTypeName]);
    tableSignatures.(['RT_isSignificant_', mfTypeName]) = tableSignatures.(['RT_pValue_', mfTypeName]) < 0.05;%/nSignatures;
    tableSignatures.(['RT_percentagePositiveSamplesMinus50_', mfTypeName]) = tableSignatures.(['RT_percentagePositiveSlope_', mfTypeName])-50;
    
    tableSignatures.(['RT_perSamples_negLogPValue_', mfTypeName]) = -log10(tableSignatures.(['RT_perSamples_pValue_', mfTypeName]));
    tableSignatures.(['RT_perSamples_negLogQValue_', mfTypeName]) = tableSignatures.(['RT_perSamples_negLogPValue_', mfTypeName]);
    tableSignatures.(['RT_perSamples_isSignificant_', mfTypeName]) = tableSignatures.(['RT_perSamples_pValue_', mfTypeName]) < 0.05;%/nSignatures;
    tableSignatures.(['RT_perSamples_percentagePositiveSamplesMinus50_', mfTypeName]) = tableSignatures.(['RT_perSamples_percentagePositiveSlope_', mfTypeName])-50;
end
tableSignatures.allTrue = true(nSignatures, 1);
%%
yLabelText = '-log_{10} p-value';
for iType = 1%[2,3]%[1,4]%2:4%1:2
    if (iType == 1)
        xVar = 'medianDifference'; xLabelText = 'median asymmetry ({\bfmatching} exposure - {\bfinverse} exposure)';
        yVar = 'negLogQValue';
        insetVar = 'percentagePositiveSamplesMinus50';
        significantVar = 'isSignificant';
        plottedLabelsVar = 'isSignificant'; yLabelTextInset = {'Percentage of samples', 'with dominant matching exposure (%)'};
        saveNameFig = 'Fig2';
        positiveColour = [1,0.1,0.2]; positiveText = {' ', 'matching', 'exposure'};
        negativeColour = [0,0.5,1]; negativeText = {' ', 'inverse', 'exposure'};
    elseif (iType <= 4)
        mfTypeName = mfTypeNames{iType-1};
        xLabelText = ['log_2 fold change of replication timing: ', mfTypeName, ' exposure'];
        xVar = ['RT_log2FC_', mfTypeName]; %RT_slope_matching';
        %         xLabelText = ['slope of correlation with replication timing: ', mfTypeName, ' exposure'];
        %         xVar = ['RT_slope_', mfTypeName]; %RT_slope_matching';
        yVar = ['RT_negLogQValue_', mfTypeName]; %'RT_negLogQValue_matching';
        insetVar = ['RT_perSamples_percentagePositiveSamplesMinus50_', mfTypeName]; %'RT_percentagePositiveSamplesMinus50_matching';
        yLabelTextInset = {'Percentage of samples with positive correlation', 'with replication timing (%)'};
        significantVar = ['RT_isSignificant_', mfTypeName]; %'RT_isSignificant_matching';
        saveNameFig = ['Fig3_', mfTypeName]; %'Fig3';
        plottedLabelsVar = 'allTrue';
        positiveColour = [0.9608    0.5098    0.1451]; positiveText = {' ', 'positive', 'correlation'}; %red-ish orange[1,.3,.3], other orange [0.93,0.25,0.24], 
        negativeColour = [.1,.2,.5]; negativeText = {' ', 'negative', 'correlation'};
        if (iType == 4);
            xLabelText = 'log_2(exposure in {\bflate} / exposure in {\bfearly})';
            saveNameFig = 'Fig3';
        end
    else
        mfTypeName = mfTypeNames{iType-4};
        xLabelText = ['per sample median of log_2 fold change of replication timing: ', mfTypeName, ' exposure'];
        xVar = ['RT_perSamples_log2FCMedian_', mfTypeName]; %RT_slope_matching';
        %         xLabelText = ['slope of correlation with replication timing: ', mfTypeName, ' exposure'];
        %         xVar = ['RT_slope_', mfTypeName]; %RT_slope_matching';
        yVar = ['RT_perSamples_negLogQValue_', mfTypeName]; %'RT_negLogQValue_matching';
        insetVar = ['RT_percentagePositiveSamplesMinus50_', mfTypeName]; %'RT_percentagePositiveSamplesMinus50_matching';
        yLabelTextInset = {'Percentage of samples with positive correlation', 'with replication timing (%)'};
        significantVar = ['RT_perSamples_isSignificant_', mfTypeName]; %'RT_isSignificant_matching';
        saveNameFig = ['Fig3_perSamples_', mfTypeName]; %'Fig3';
        plottedLabelsVar = 'allTrue';
        positiveColour = [0.9608    0.5098    0.1451]; positiveText = {' ', 'positive', 'correlation'}; %red-ish orange[1,.3,.3], other orange [0.93,0.25,0.24], 
        negativeColour = [.1,.2,.5]; negativeText = {' ', 'negative', 'correlation'};
        %         if (iType == 4);
        %             xLabelText = 'log_2(exposure in {\bflate} / exposure in {\bfearly})';
        %             saveNameFig = 'Fig3';
        %         end
    end
    fig = figure(2); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition',1.3*[0 0 30 20],'units','normalized','outerposition',[0 0 1 1]);
    axes('Position', [0.07, 0.05, 0.9, 0.9]); hold on; % fig = createMaximisedFigure(1);  % cmap = hsv(nSignatures);
    nColours = 100;
    cmap_general = ([colourLinspace(negativeColour, 0.9*[1,1,1], nColours); 0.5*[1,1,1]; colourLinspace(0.9*[1,1,1], positiveColour, nColours)]);
    maxVal = (max(abs(tableSignatures.(xVar))));
    
    tmpValues = round(nColours*tableSignatures.(xVar)/maxVal) + nColours+1;
    tmpValues(~tableSignatures.(significantVar)) = nColours+1;
    cmap = cmap_general(tmpValues, :);
    font_size = 16;
    colormap(cmap_general);
    
    signatureShortNames = strrep(strrep(signatureNames, 'Signature ', ''), 'Signature', '');
    [~, listSignatures] = sort(tableSignatures.(yVar));
    hOriginalDots = zeros(nSignatures, 1);
    for iSignature = listSignatures'
        hOriginalDots(iSignature) = plot(tableSignatures.(xVar)(iSignature), tableSignatures.(yVar)(iSignature), 'o', 'MarkerFaceColor', cmap(iSignature, :), 'MarkerSize', 15, 'MarkerEdgeColor', cmap(iSignature, :)/2); %10
    end
    labelRepel(tableSignatures.(xVar)(tableSignatures.(plottedLabelsVar)), tableSignatures.(yVar)(tableSignatures.(plottedLabelsVar)), 0, 0, signatureShortNames(tableSignatures.(plottedLabelsVar)), font_size-2);
    
    delete(hOriginalDots);
    for iSignature = listSignatures'
        plot(tableSignatures.(xVar)(iSignature), tableSignatures.(yVar)(iSignature), 'o', 'MarkerFaceColor', cmap(iSignature, :), 'MarkerSize', 12, 'MarkerEdgeColor', cmap(iSignature, :)/2);
    end
    %ylim([0,4]); %TODO
    yLimVal = get(gca, 'YLim'); xLimVal = get(gca, 'XLim'); xMax = max(abs(xLimVal)); %xMax = 3; %TODO
    xLimVal = [-xMax, xMax]; xlim(xLimVal); % plot(get(gca, 'XLim'), -log10(0.05)*[1,1], '-r');
    plot(0*[1,1], yLimVal, ':k');
    text(0.95*xLimVal(1), 0, negativeText, 'Color', negativeColour, 'HorizontalAlignment', 'center', 'FontSize', font_size, 'VerticalAlignment', 'top');
    text(0.95*xLimVal(2), 0, positiveText, 'Color', positiveColour, 'HorizontalAlignment', 'center', 'FontSize', font_size, 'VerticalAlignment', 'top');
    %TODO!!!! colorbar('SouthOutside', 'Ticks', []); %[0,1], 'TickLabels', {'negativeText', 'positiveText'});
    
    xlabel(xLabelText, 'FontSize', font_size); type = 'medianDifference';
    ylabel(yLabelText, 'FontSize', font_size);
    set(gca, 'FontSize', font_size); % title(sprintf('%s mean exposure >= %d', strrep(territoriesSource, '_', '\_'), minMeanExposure));
    %mySaveAs(fig, imagesPath, ['volcanoPlot_', type, '_', num2str(minMeanExposure), '_', territoriesSource]);%, '_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')]);
    
    %
    if (iType == 1)
        axes('Position', [0.13, 0.5, 0.36, 0.4]); hold on; % fig = createMaximisedFigure(3); hold on;
        [~, listSignatures] = sort(tableSignatures.(insetVar), 'descend');
        isPositive = tableSignatures.(insetVar)(listSignatures)>0;
        
        font_size = 12;
        nColours = 100;
        cmap_general = ([colourLinspace(negativeColour, 0.9*[1,1,1], nColours); colourLinspace(0.9*[1,1,1], positiveColour, nColours)]);
        maxVal = (max(abs(tableSignatures.(insetVar))));
        cmap = cmap_general(round(nColours*tableSignatures.(insetVar)/maxVal) + nColours, :);
        
        cmap(~tableSignatures.(significantVar), 1) = 0.5;
        cmap(~tableSignatures.(significantVar), 2) = 0.5;
        cmap(~tableSignatures.(significantVar), 3) = 0.5;
        
        xValues = 1:nSignatures;
        for iCol = 1:nSignatures
            bar(iCol, tableSignatures.(insetVar)(listSignatures(iCol)), 'FaceColor',  cmap(listSignatures(iCol), :));
        end
        
        text(xValues(isPositive), tableSignatures.(insetVar)(listSignatures(isPositive)), signatureShortNames(listSignatures(isPositive)), 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', 'FontSize', font_size-3);
        text(xValues(~isPositive), tableSignatures.(insetVar)(listSignatures(~isPositive)), signatureShortNames(listSignatures(~isPositive)), 'HorizontalAlignment', 'center', 'VerticalAlignment', 'top', 'FontSize', font_size-3);
        
        % text(xValues(isPositive), tableSignatures.percentagePositiveSamples(listSignatures(isPositive))-50, signatureShortNames(listSignatures(isPositive)), 'Rotation', 45, 'HorizontalAlignment', 'left', 'VerticalAlignment', 'bottom', 'FontSize', font_size-3);
        % text(xValues(~isPositive), tableSignatures.percentagePositiveSamples(listSignatures(~isPositive))-50, signatureShortNames(listSignatures(~isPositive)), 'Rotation', 45, 'HorizontalAlignment', 'right', 'VerticalAlignment', 'top', 'FontSize', font_size-3);
        set(gca, 'XTick', [], 'XTickLabel', []);
        ylim([-50, 50]); % ylim([-25, 40]);
        yTickVal = get(gca, 'YTick');
        set(gca, 'YTick', yTickVal, 'YTickLabel', yTickVal+50, 'FontSize', font_size, 'XColor', 1*[1,1,1]);
        ylabel(yLabelTextInset, 'FontSize', font_size); % title(sprintf('%s mean exposure >= %d', strrep(territoriesSource, '_', '\_'), minMeanExposure));
    end
    %     mySaveAs(fig, imagesPath, ['percentageBarPlot_', type, '_', num2str(minMeanExposure), '_', territoriesSource]);
    mySaveAs(fig, imagesPath, saveNameFig);
    
end

% save('workspaces\workspace_2017_02_04.mat', '-v7.3');
% save('workspaces\workspace_2017_02_09.mat', '-v7.3');