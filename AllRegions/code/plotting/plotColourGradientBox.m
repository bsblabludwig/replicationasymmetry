function plotColourGradientBox(xMin, xMax, yMin, yMax, cMin, cMax, direction, positiveColours, zeroColour, negativeColours, scalingFactor, nLines)

% cmap = colourLinspace(colour1, colour2, nLines);

nPositive = size(positiveColours, 1);
nNegative = size(negativeColours, 1);

if (~exist('nLines', 'var'))
    nLines = round(scalingFactor*(cMax - cMin));
end
    

if (strcmp(direction, 'x'))
    cValues = linspace(cMin, cMax, nLines);
    xValues = linspace(xMin, xMax, nLines);
    for iLine = 1:nLines
        cValue = cValues(iLine);
        xValue = xValues(iLine);
        xValueRound = round(scalingFactor*cValue);
        if (xValueRound > 0)
            col = positiveColours(min([xValueRound, nPositive]),:);
        elseif (xValueRound < 0)
            col = negativeColours(min([abs(xValueRound), nNegative]),:);
        else
            col = zeroColour;
        end
        plot(xValue*[1,1], [yMin, yMax], 'Color', col);%cmap(iLine,:));
    end
else
    cValues = linspace(cMin, cMax, nLines);
    yValues = linspace(yMin, yMax, nLines);
    for iLine = 1:nLines
        cValue = cValues(iLine);
        yValue = yValues(iLine);
        yValueRound = round(scalingFactor*cValue);
        if (yValueRound > 0)
            col = positiveColours(min([yValueRound, nPositive]),:);
        elseif (yValueRound < 0)
            col = negativeColours(min([abs(yValueRound), nNegative]),:);
        else
            col = zeroColour;
        end
        plot([xMin, xMax], yValue*[1,1], 'Color', col);%cmap(iLine,:));
    end
end