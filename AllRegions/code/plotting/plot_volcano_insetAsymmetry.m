function plot_volcano_insetAsymmetry(tableSignatures, signatureShortNames, significantVar, positiveColour, negativeColour, insetVar, yLabelTextInset)

nSignatures = length(signatureShortNames);
[~, listSignatures] = sort(tableSignatures.(insetVar), 'descend');
isPositive = tableSignatures.(insetVar)(listSignatures)>0;

font_size = 12;
nColours = 100;
cmap_general = ([colourLinspace(negativeColour, 0.9*[1,1,1], nColours); colourLinspace(0.9*[1,1,1], positiveColour, nColours)]);
maxVal = (max(abs(tableSignatures.(insetVar))));
cmap = cmap_general(round(nColours*tableSignatures.(insetVar)/maxVal) + nColours, :);

cmap(~tableSignatures.(significantVar), 1) = 0.5;
cmap(~tableSignatures.(significantVar), 2) = 0.5;
cmap(~tableSignatures.(significantVar), 3) = 0.5;

xValues = 1:nSignatures;
for iCol = 1:nSignatures
    bar(iCol, tableSignatures.(insetVar)(listSignatures(iCol)), 'FaceColor',  cmap(listSignatures(iCol), :));
end

text(xValues(isPositive), tableSignatures.(insetVar)(listSignatures(isPositive)), signatureShortNames(listSignatures(isPositive)), 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', 'FontSize', font_size-3);
text(xValues(~isPositive), tableSignatures.(insetVar)(listSignatures(~isPositive)), signatureShortNames(listSignatures(~isPositive)), 'HorizontalAlignment', 'center', 'VerticalAlignment', 'top', 'FontSize', font_size-3);

% text(xValues(isPositive), tableSignatures.percentagePositiveSamples(listSignatures(isPositive))-50, signatureShortNames(listSignatures(isPositive)), 'Rotation', 45, 'HorizontalAlignment', 'left', 'VerticalAlignment', 'bottom', 'FontSize', font_size-3);
% text(xValues(~isPositive), tableSignatures.percentagePositiveSamples(listSignatures(~isPositive))-50, signatureShortNames(listSignatures(~isPositive)), 'Rotation', 45, 'HorizontalAlignment', 'right', 'VerticalAlignment', 'top', 'FontSize', font_size-3);
set(gca, 'XTick', [], 'XTickLabel', []);
ylim([-50, 50]); % ylim([-25, 40]);
yTickVal = -50:25:50; %get(gca, 'YTick');
set(gca, 'YTick', yTickVal, 'YTickLabel', yTickVal+50, 'FontSize', font_size, 'XColor', 'none');
ylabel(yLabelTextInset, 'FontSize', font_size+1); % title(sprintf('%s mean exposure >= %d', strrep(territoriesSource, '_', '\_'), minMeanExposure));