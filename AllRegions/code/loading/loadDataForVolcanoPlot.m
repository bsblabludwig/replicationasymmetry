function tableSignatures = loadDataForVolcanoPlot(alwaysCompute, savePathBasic, replData, territoriesSource)

pathAndName = [savePathBasic, 'dataForVolcanoPlot_', territoriesSource];

%%
if (~alwaysCompute && exist([pathAndName, '.mat'],'file'))
    load(pathAndName, 'tableSignatures');
else
    tic;
    fullRun = true;
    minMeanExposure = 10;%1000;
    signatureExposuresSimple_LeadLagg = replData.signatureExposuresSimple.LeadLagg;
    nSignatures = replData.nSignatures;
    signatureNames = replData.signatureNames;
    nSamples = size(replData.tableAllSamples, 1);
    
    xVal_difference = signatureExposuresSimple_LeadLagg.type1Minustype2;
    xVal_logRatio = log2(signatureExposuresSimple_LeadLagg.type1./signatureExposuresSimple_LeadLagg.type2);
    
    mfTypes = {'type1', 'type2', 'totalMF', 'average'}; nMFTypes = length(mfTypes);
    mfTypeNames = {'matching', 'inverse', 'total', 'average'};
    
    nBins = length(replData.signatureExposuresComplex.ReplicationTiming_LeadLagg);
    pValue = NaN*ones(nSignatures, 1);
    medianDifference = NaN*ones(nSignatures, 1);
    meanDifference = NaN*ones(nSignatures, 1);
    medianLogRatio = NaN*ones(nSignatures, 1);
    meanLogRatio = NaN*ones(nSignatures, 1);
    nExposedSamples = NaN*ones(nSignatures, 1);
    percentagePositiveSamples = NaN*ones(nSignatures, 1);
    signatureNameShort = cell(nSignatures, 1);
    
    tableSignatures = table(signatureNameShort, nExposedSamples, percentagePositiveSamples, pValue, medianDifference, meanDifference, medianLogRatio, meanLogRatio);
    clear signatureNameShort nExposedSamples pValue medianDifference meanDifference medianLogRatio meanLogRatio
    
    for iMFType = 1:nMFTypes
        mfTypeName = mfTypeNames{iMFType};
        tableSignatures.(['RT_pValue_', mfTypeName]) = NaN*ones(nSignatures, 1);
        tableSignatures.(['RT_slope_', mfTypeName]) = NaN*ones(nSignatures, 1);
        tableSignatures.(['RT_log2FC_', mfTypeName]) = NaN*ones(nSignatures, 1);
        tableSignatures.(['RT_percentagePositiveSlope_', mfTypeName]) = NaN*ones(nSignatures, 1);
        tableSignatures.(['RT_perSamples_pValue_', mfTypeName]) = NaN*ones(nSignatures, 1);
        tableSignatures.(['RT_perSamples_log2FCMedian_', mfTypeName]) = NaN*ones(nSignatures, 1);
        tableSignatures.(['RT_perSamples_log2FCMean_', mfTypeName]) = NaN*ones(nSignatures, 1);
        tableSignatures.(['RT_perSamples_percentagePositiveSlope_', mfTypeName]) = NaN*ones(nSignatures, 1);
        tableSignatures.(['RT_perSamples_percentageNegativeSlope_', mfTypeName]) = NaN*ones(nSignatures, 1);
    end
    % tableSignatures.RT_pValue_matching = NaN*ones(nSignatures, 1);
    % tableSignatures.RT_slope_matching = NaN*ones(nSignatures, 1);
    % tableSignatures.RT_log2FC_matching = NaN*ones(nSignatures, 1);
    % tableSignatures.RT_percentagePositiveSlope_matching = NaN*ones(nSignatures, 1);
    % tableSignatures.RT_pValue_inverse = NaN*ones(nSignatures, 1);
    % tableSignatures.RT_slope_inverse = NaN*ones(nSignatures, 1);
    % tableSignatures.RT_log2FC_inverse = NaN*ones(nSignatures, 1);
    % tableSignatures.RT_percentagePositiveSlope_inverse = NaN*ones(nSignatures, 1);
    
    for iBin = 1:nBins
        replData.signatureExposuresComplex.ReplicationTiming_LeadLagg{iBin}.average = (replData.signatureExposuresComplex.ReplicationTiming_LeadLagg{iBin}.type1 + replData.signatureExposuresComplex.ReplicationTiming_LeadLagg{iBin}.type2)/2;
    end
    
    for iSignature = 1:nSignatures
        fprintf('Signature %d...\n', iSignature);
        isOkSample = ~isnan(xVal_difference(iSignature,:)) & ~isinf(xVal_difference(iSignature,:)) & signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure; %~isPOLE' &
        tableSignatures.nExposedSamples(iSignature) = sum(isOkSample);
        tableSignatures.percentagePositiveSamples(iSignature) = 100*mean(xVal_difference(iSignature, isOkSample)>0);
        tableSignatures.pValue(iSignature) = signtest(xVal_difference(iSignature, isOkSample)); %signrank
        tableSignatures.medianDifference(iSignature) = nanmedian(xVal_difference(iSignature, isOkSample));
        tableSignatures.meanDifference(iSignature) = nanmean(xVal_difference(iSignature, isOkSample));
        tableSignatures.medianLogRatio(iSignature) = nanmedian(xVal_logRatio(iSignature, isOkSample));
        tableSignatures.meanLogRatio(iSignature) = nanmean(xVal_logRatio(iSignature, isOkSample));
        tableSignatures.signatureNameShort{iSignature} = strrep(signatureNames{iSignature}, 'Signature', '');
        
        if (fullRun)
            for iMFType = 1:nMFTypes
                mfType = mfTypes{iMFType};
                mfTypeName = mfTypeNames{iMFType};
                replTimingExposures = NaN*ones(nBins, nSamples);
                for iBin = 1:nBins
                    replTimingExposures(iBin,:) = replData.signatureExposuresComplex.ReplicationTiming_LeadLagg{iBin}.(mfType)(iSignature, :);
                end
                isPositiveSlope = replTimingExposures(1,:)<replTimingExposures(2,:) & replTimingExposures(2,:)<replTimingExposures(3,:) & replTimingExposures(3,:)<replTimingExposures(4,:);
                isNegativeSlope = replTimingExposures(1,:)>replTimingExposures(2,:) & replTimingExposures(2,:)>replTimingExposures(3,:) & replTimingExposures(3,:)>replTimingExposures(4,:);
                replTimingMeanExposures = nanmean(replTimingExposures(:,isOkSample), 2);
                if (sum(replTimingMeanExposures) == 0)
                    replTimingMeanExposures = replTimingMeanExposures + 1;
                end
                linearModel = fitlm((1:nBins), replTimingMeanExposures);
                tableSignatures.(['RT_pValue_', mfTypeName])(iSignature) = coefTest(linearModel, [0,1]);
                tableSignatures.(['RT_slope_', mfTypeName])(iSignature) = linearModel.Coefficients{2,1};
                tableSignatures.(['RT_log2FC_', mfTypeName])(iSignature) = log2(max([1,linearModel.Fitted(end)])/max([1,linearModel.Fitted(1)])); %log2((linearModel.Fitted(end)+smallConstant)/(linearModel.Fitted(1)+smallConstant));
                tableSignatures.(['RT_percentagePositiveSlope_', mfTypeName])(iSignature) = 100*nanmean(isPositiveSlope(isOkSample & (isPositiveSlope | isNegativeSlope))>0);
                allSamplesLog2FC = NaN*ones(nSamples, 1);
                allSamplesPValue = NaN*ones(nSamples, 1);
                allSamplesSlope = NaN*ones(nSamples, 1);
                for iSample = find(isOkSample)
                    replTimingCurrentExposures = replTimingExposures(:,iSample);
                    if (sum(replTimingCurrentExposures) == 0)
                        replTimingCurrentExposures = replTimingCurrentExposures + 1;
                    end
                    linearModel = fitlm((1:nBins), replTimingCurrentExposures);
                    allSamplesLog2FC(iSample) = log2(max([1,linearModel.Fitted(end)])/max([1,linearModel.Fitted(1)]));
                    allSamplesPValue(iSample) = coefTest(linearModel, [0,1]);
                    allSamplesSlope(iSample) = linearModel.Coefficients{2,1};
                end
                tableSignatures.(['RT_perSamples_pValue_', mfTypeName])(iSignature) = signtest(allSamplesLog2FC(isOkSample));
                tableSignatures.(['RT_perSamples_log2FCMedian_', mfTypeName])(iSignature) = nanmedian(allSamplesLog2FC(isOkSample));
                tableSignatures.(['RT_perSamples_log2FCMean_', mfTypeName])(iSignature) = nanmean(allSamplesLog2FC(isOkSample));
                tableSignatures.(['RT_perSamples_percentagePositiveSlope_', mfTypeName])(iSignature) = 100*nanmean((allSamplesSlope(isOkSample) > 0) & (allSamplesPValue(isOkSample) < 0.05));
                tableSignatures.(['RT_perSamples_percentageNegativeSlope_', mfTypeName])(iSignature) = 100*nanmean((allSamplesSlope(isOkSample) < 0) & (allSamplesPValue(isOkSample) < 0.05));
            end
        end
    end
    tableSignatures.percentagePositiveSamplesMinus50 = tableSignatures.percentagePositiveSamples-50;
    % [~, ~, ~, adj_p]=fdr_bh(tableSignatures.pValue);
    % tableSignatures.qValue = adj_p;
    % tableSignatures.negLogPValue = -log10(tableSignatures.pValue);
    % tableSignatures.negLogQValue = -log10(tableSignatures.qValue);
    % tableSignatures.isSignificant = tableSignatures.qValue < 0.05;
    
    tableSignatures.negLogPValue = -log10(tableSignatures.pValue);
    tableSignatures.negLogQValue = tableSignatures.negLogPValue;
    tableSignatures.isSignificant = tableSignatures.pValue < 0.05/nSignatures;
    
    for iMFType = 1:nMFTypes
        mfTypeName = mfTypeNames{iMFType};
        tableSignatures.(['RT_negLogPValue_', mfTypeName]) = -log10(tableSignatures.(['RT_pValue_', mfTypeName]));
        tableSignatures.(['RT_negLogQValue_', mfTypeName]) = tableSignatures.(['RT_negLogPValue_', mfTypeName]);
        tableSignatures.(['RT_isSignificant_', mfTypeName]) = tableSignatures.(['RT_pValue_', mfTypeName]) < 0.05;%/nSignatures;
        tableSignatures.(['RT_percentagePositiveSamplesMinus50_', mfTypeName]) = tableSignatures.(['RT_percentagePositiveSlope_', mfTypeName])-50;
        
        tableSignatures.(['RT_perSamples_negLogPValue_', mfTypeName]) = -log10(tableSignatures.(['RT_perSamples_pValue_', mfTypeName]));
        tableSignatures.(['RT_perSamples_negLogQValue_', mfTypeName]) = tableSignatures.(['RT_perSamples_negLogPValue_', mfTypeName]);
        tableSignatures.(['RT_perSamples_isSignificant_', mfTypeName]) = tableSignatures.(['RT_perSamples_pValue_', mfTypeName]) < 0.05;%/nSignatures;
        tableSignatures.(['RT_perSamples_percentagePositiveSamplesMinus50_', mfTypeName]) = tableSignatures.(['RT_perSamples_percentagePositiveSlope_', mfTypeName])-50;
        tableSignatures.(['RT_perSamples_percentageNegativeSamplesMinus50_', mfTypeName]) = tableSignatures.(['RT_perSamples_percentageNegativeSlope_', mfTypeName])-50;
    end
    tableSignatures.allTrue = true(nSignatures, 1);
    
    %%
    minMeanExposure = 10;%1000;
    mfTypes = {'type1', 'type2', 'totalMF', 'average'}; nMFTypes = length(mfTypes);
    mfTypeNames = {'matching', 'inverse', 'total', 'average'};
    nBins = length(replData.signatureExposuresComplex.ReplicationTiming_LeadLagg);
    tic
    
    for iBin = 1:nBins
        replData.signatureExposuresComplex.ReplicationTiming_LeadLagg{iBin}.average = (replData.signatureExposuresComplex.ReplicationTiming_LeadLagg{iBin}.type1 + replData.signatureExposuresComplex.ReplicationTiming_LeadLagg{iBin}.type2)/2;
    end
    for iMFType = nMFTypes%1:nMFTypes
        mfType = mfTypes{iMFType};
        mfTypeName = mfTypeNames{iMFType};
        tableSignatures.(['RT_perSamples_signSlopeFC_pValue_', mfTypeName]) = NaN*ones(nSignatures, 1);
        for iSignature = 1:nSignatures
            fprintf('Signature %d...\n', iSignature);
            isOkSample = ~isnan(signatureExposuresSimple_LeadLagg.type1Minustype2(iSignature,:)) & ~isinf(signatureExposuresSimple_LeadLagg.type1Minustype2(iSignature,:)) & signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure; %~isPOLE' &
            replTimingExposures = NaN*ones(nBins, nSamples);
            for iBin = 1:nBins
                replTimingExposures(iBin,:) = replData.signatureExposuresComplex.ReplicationTiming_LeadLagg{iBin}.(mfType)(iSignature, :);
            end
            allSamplesLog2FC = NaN*ones(nSamples, 1);
            allSamplesPValue = NaN*ones(nSamples, 1);
            %allSamplesSlope = NaN*ones(nSamples, 1);
            for iSample = find(isOkSample)
                replTimingCurrentExposures = replTimingExposures(:,iSample);
                if (sum(replTimingCurrentExposures) == 0)
                    replTimingCurrentExposures = replTimingCurrentExposures + 1;
                end
                linearModel = fitlm((1:nBins), replTimingCurrentExposures);
                allSamplesLog2FC(iSample) = log2(max([1,linearModel.Fitted(end)])/max([1,linearModel.Fitted(1)]));
                allSamplesPValue(iSample) = coefTest(linearModel, [0,1]);
                %allSamplesSlope(iSample) = linearModel.Coefficients{2,1};
            end
            allSamplesLog2FC(allSamplesPValue >= 0.05) = 0;
            tableSignatures.(['RT_perSamples_signSlopeFC_pValue_', mfTypeName])(iSignature) = signtest(allSamplesLog2FC(isOkSample));
        end
        tableSignatures.(['RT_perSamples_signSlopeFC_negLogPValue_', mfTypeName]) = -log10(tableSignatures.(['RT_perSamples_signSlopeFC_pValue_', mfTypeName]));
        tableSignatures.(['RT_perSamples_signSlopeFC_negLogQValue_', mfTypeName]) = tableSignatures.(['RT_perSamples_signSlopeFC_negLogPValue_', mfTypeName]);
        tableSignatures.(['RT_perSamples_signSlopeFC_isSignificant_', mfTypeName]) = tableSignatures.(['RT_perSamples_signSlopeFC_pValue_', mfTypeName]) < 0.05/nSignatures;
    end
    toc
    %%
    for iMFType = 1:nMFTypes
        mfTypeName = mfTypeNames{iMFType};
        tableSignatures.(['RT_perSamples_signSlope_pValue_', mfTypeName]) = NaN*ones(nSignatures, 1);
        for iSignature = 1:nSignatures
            nExposed = tableSignatures.nExposedSamples(iSignature);
            nPositive = round(nExposed*tableSignatures.(['RT_perSamples_percentagePositiveSlope_', mfTypeName])(iSignature)/100);
            nNegative = round(nExposed*tableSignatures.(['RT_perSamples_percentageNegativeSlope_', mfTypeName])(iSignature)/100);
            tableSignatures.(['RT_perSamples_signSlope_pValue_', mfTypeName])(iSignature) = signtest([-1*ones(nNegative, 1); 0*ones(nExposed-nPositive-nNegative, 1); 1*ones(nPositive, 1)]);
        end
        tableSignatures.(['RT_perSamples_signSlope_negLogPValue_', mfTypeName]) = -log10(tableSignatures.(['RT_perSamples_signSlope_pValue_', mfTypeName]));
        tableSignatures.(['RT_perSamples_signSlope_negLogQValue_', mfTypeName]) = tableSignatures.(['RT_perSamples_signSlope_negLogPValue_', mfTypeName]);
        tableSignatures.(['RT_perSamples_signSlope_isSignificant_', mfTypeName]) = tableSignatures.(['RT_perSamples_signSlope_pValue_', mfTypeName]) < 0.05/nSignatures;
    end% SAVE
    save(pathAndName, 'tableSignatures');
    toc
end