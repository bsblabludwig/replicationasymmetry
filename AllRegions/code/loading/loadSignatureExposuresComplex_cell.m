function [signatureExposuresComplex_cell, signatureExposuresSimple_LeadLagg_cell, tableAllSamples] = loadSignatureExposuresComplex_cell(alwaysCompute, savePathBasic, dirDataCT, nameDataCT, tableTerritoriesFileName, territoriesSource, decomposedSignatures)

% Testing:
% clear;
% dirMutations = 'data/mutationsWGS';
% dirBackground = 'data/contextInTerritories';
% dirDataCT = 'save/replPrepareTables_withoutBlacklist0816/'; nameDataCT = 'withoutGenesAndBlacklisted';
% savePathBasic = 'save/'; createDir(savePathBasic);
% tableTerritoriesFileName = [savePathBasic, 'tableTerritories'];               tableTerritories = [];  load(tableTerritoriesFileName, 'tableTerritories'); nameTerritories = 'tableTerritoriesHaradhvala'; % created in script_60726_replPrepareTables
% nTerritories = size(tableTerritories, 1);
% maxPattern = 96;
% alwaysCompute = false;

pathAndNameReplData = [savePathBasic, territoriesSource, '/signatureExposuresComplexAndSimple_cell_', nameDataCT, '_', territoriesSource, 'without_N1_N2'];

%%
if (~alwaysCompute && exist([pathAndNameReplData, '.mat'],'file'))
    fprintf('Loading from %s...\n', pathAndNameReplData);
    load(pathAndNameReplData, 'signatureExposuresComplex_cell', 'signatureExposuresSimple_LeadLagg_cell', 'tableAllSamples');
else
    tic;
    % dirDataCT = 'save/Besnard1k_territories/replPrepareTables1216/'; nameDataCT = 'replPrepareTables1216'; territoriesSource = 'Besnard1k_territories';
    % tableTerritoriesFileName = [savePathBasic, 'table.', territoriesSource, '.mat'];  nameTerritories = territoriesSource;
    replData = loadReplicationData(alwaysCompute, savePathBasic, dirDataCT, nameDataCT, tableTerritoriesFileName, territoriesSource, decomposedSignatures);
    signatureExposuresComplex_cell = replData.signatureExposuresComplex.DistanceFromORI_LeadLagg;
    signatureExposuresSimple_LeadLagg_cell = replData.signatureExposuresSimple.LeadLagg;
    tableAllSamples = replData.tableAllSamples;
    % SAVE
    fprintf('Saving signatureExposuresComplex_cell and signatureExposuresSimple_LeadLagg_cell and tableAllSamples...\n');
    save(pathAndNameReplData, 'signatureExposuresComplex_cell', 'signatureExposuresSimple_LeadLagg_cell', 'tableAllSamples', '-v7.3');
    toc
end