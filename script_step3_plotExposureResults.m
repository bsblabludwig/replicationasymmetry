%% script_step3_plotExposureResults.m
clear; addpath(genpath('code/'));
savePathDiary = 'diaries/'; createDir(savePathDiary); scriptName = 'script_step1_prepareCancerTissues'; compName = 'HOME';
diaryFile = [savePathDiary, 'diaryLog_',scriptName,'_',datestr(now, 'dd-mm-yyyy_HH-MM-SS'),'.txt'];
diary(diaryFile); diary on; sendingEmails = false;
fprintf('%s %s %s START\n', datestr(now, 'dd-mm-yyyy HH-MM-SS'), compName, scriptName);
%%
% prepareOfficialSignatures; % Prepares files with the signatures using a file downloaded from http://cancer.sanger.ac.uk/cancergenome/assets/signatures_probabilities.txt.
tissueNames = {'MSI_19', 'POLE_14', 'blood_lymphoid_274', 'blood_myeloid_56', 'bone_98', 'brain_237', 'breast_560', 'colorectum_35', 'gastric_90', 'kidney_clear_cell_95', 'liver_303', 'lung_adenocarcinoma_24', ...
    'lung_squamous_36', 'oesophagus_adenocarcinoma_219', 'oral_25', 'ovary_93', 'pancreas_406', 'prostate_294', 'skin_183'}; 
territoriesSource = 'Haradhvala_territories';
nMaxSignatures = 7; 
nRuns = 100;
minStability = 0.8;
alwaysComputeDecomposition = false;
alwaysComputeClustering = false;
savePathBasic = 'save/';
evaluationOfDecomposedSignatures = loadEvaluationOfDecomposedSignatures(alwaysComputeDecomposition, savePathBasic, tissueNames, nMaxSignatures, nRuns, minStability, territoriesSource);
decomposedSignatures = loadDecomposedSignatures(alwaysComputeClustering, savePathBasic, tissueNames, evaluationOfDecomposedSignatures, territoriesSource);
alwaysComputeReplication = false;
%%
if (true)
    dirDataCT_Besnard1k = 'save/Besnard1k_territories/replPrepareTables1216/'; nameDataCT_Besnard1k = 'replPrepareTables1216'; territoriesSource_Besnard1k = 'Besnard1k_territories'; binSize_Besnard1k = 1000;
    tableTerritoriesFileName_Besnard1k = [savePathBasic, 'table.', territoriesSource_Besnard1k, '.mat'];  nameTerritories_Besnard1k = territoriesSource_Besnard1k; 
    tableTerritoriesFileName = tableTerritoriesFileName_Besnard1k; nameTerritories = nameTerritories_Besnard1k;
    %replData_Besnard1k = loadReplicationData(alwaysComputeReplication, savePathBasic, dirDataCT_Besnard1k, nameDataCT_Besnard1k, tableTerritoriesFileName_Besnard1k, nameTerritories_Besnard1k, decomposedSignatures);
    tableSignatures_Besnard1k = loadDataForVolcanoPlot(false, savePathBasic, territoriesSource_Besnard1k, nameDataCT_Besnard1k);%, replData_Besnard1k);
    [signatureExposuresComplex_cell_Besnard1k, signatureExposuresSimple_LeadLagg_cell_Besnard1k, tableAllSamples_Besnard1k] = loadSignatureExposuresComplex_cell(alwaysComputeReplication, savePathBasic, dirDataCT_Besnard1k, nameDataCT_Besnard1k, tableTerritoriesFileName, nameTerritories, decomposedSignatures);
    %signatureExposuresSimple_LeadLagg_cell_Besnard1k = loadSignatureExposuresSimple_LeadLagg_cell(alwaysComputeReplication, savePathBasic, dirDataCT_Besnard1k, nameDataCT_Besnard1k, tableTerritoriesFileName, nameTerritories, decomposedSignatures);
end
%%
% dirDataCT_PetrykGM_rep1 = 'save/PetrykGM_rep1_territories/replPrepareTables_OutsideGenesAndBlacklisted/'; nameDataCT_PetrykGM_rep1 = 'replPrepareTables_OutsideGenesAndBlacklisted'; territoriesSource_PetrykGM_rep1 = 'PetrykGM_rep1_territories'; binSize_PetrykGM_rep1 = 1000;
% tableTerritoriesFileName_PetrykGM_rep1 = [savePathBasic, 'table.', territoriesSource_PetrykGM_rep1, '.mat'];  nameTerritories_PetrykGM_rep1 = territoriesSource_PetrykGM_rep1; 
% replData_PetrykGM_rep1 = loadReplicationData(alwaysComputeReplication, savePathBasic, dirDataCT_PetrykGM_rep1, nameDataCT_PetrykGM_rep1, tableTerritoriesFileName_PetrykGM_rep1, nameTerritories_PetrykGM_rep1, decomposedSignatures);
% tableSignatures_PetrykGM_rep1 = loadDataForVolcanoPlot(false, savePathBasic, territoriesSource_PetrykGM_rep1, nameDataCT_PetrykGM_rep1);%, replData_PetrykGM_rep1);
% [signatureExposuresComplex_cell_PetrykGM_rep1, signatureExposuresSimple_LeadLagg_cell_PetrykGM_rep1, tableAllSamples_PetrykGM_rep1] = loadSignatureExposuresComplex_cell(alwaysComputeReplication, savePathBasic, dirDataCT_PetrykGM_rep1, nameDataCT_PetrykGM_rep1, tableTerritoriesFileName_PetrykGM_rep1, nameTerritories_PetrykGM_rep1, decomposedSignatures);
%%
nameTerritories = territoriesSource; tableTerritoriesFileName = [savePathBasic, 'table.', territoriesSource, '.mat']; % created in script_replPrepareTerritories (originally script_60726_replPrepareTables)
if (false)
savePathBasic_OAGAB = 'OutsideAllGenesAndBlacklisted/save/'; dirDataCT_OAGAB = 'OutsideAllGenesAndBlacklisted/save/Haradhvala_territories/replPrepareTables_OutsideAllGenesAndBlacklisted/'; nameDataCT_OAGAB = 'replPrepareTables_OutsideAllGenesAndBlacklisted'; %territoriesSource = 'Haradhvala_territories'; %binSize = 20000; % tableTerritoriesFileName = [savePathBasic, 'table.', territoriesSource, '.mat'];   % created in script_replPrepareTerritories (originally script_60726_replPrepareTables)
replData_OAGAB = loadReplicationData(false, savePathBasic_OAGAB, dirDataCT_OAGAB, nameDataCT_OAGAB, tableTerritoriesFileName, nameTerritories, decomposedSignatures);
tableSignatures_OAGAB = loadDataForVolcanoPlot(false, savePathBasic_OAGAB, territoriesSource, nameDataCT_OAGAB, replData_OAGAB);
savePathBasic_AR = 'AllRegions/save/'; dirDataCT_AR = 'AllRegions/save/Haradhvala_territories/replPrepareTables_AllRegions/'; nameDataCT_AR = 'replPrepareTables_AllRegions'; %territoriesSource = 'Haradhvala_territories'; %binSize = 20000; % tableTerritoriesFileName = [savePathBasic, 'table.', territoriesSource, '.mat'];  nameTerritories = territoriesSource; % created in script_replPrepareTerritories (originally script_60726_replPrepareTables)
replData_AR = loadReplicationData(false, savePathBasic_AR, dirDataCT_AR, nameDataCT_AR, tableTerritoriesFileName, nameTerritories, decomposedSignatures);
tableSignatures_AR = loadDataForVolcanoPlot(false, savePathBasic_AR, territoriesSource, nameDataCT_AR, replData_AR);
% dirDataCT = 'save/Haradhvala_territories/replPrepareTables1119_70114/'; nameDataCT = 'replPrepareTables1119_70114'; territoriesSource = 'Haradhvala_territories'; binSize = 20000;
% replData = loadReplicationData(alwaysComputeReplication, savePathBasic, dirDataCT, nameDataCT, tableTerritoriesFileName, nameTerritories, decomposedSignatures);
% tableSignatures = loadDataForVolcanoPlot(false, savePathBasic, territoriesSource, nameDataCT, replData);
end
%%
dirDataCT = 'save/Haradhvala_territories/replPrepareTables_OutsideGenesAndBlacklisted/'; nameDataCT = 'replPrepareTables_OutsideGenesAndBlacklisted'; territoriesSource = 'Haradhvala_territories'; binSize = 20000;
replData = loadReplicationData(alwaysComputeReplication, savePathBasic, dirDataCT, nameDataCT, tableTerritoriesFileName, nameTerritories, decomposedSignatures);
tableSignatures = loadDataForVolcanoPlot(false, savePathBasic, territoriesSource, nameDataCT, replData);
%
imagesPath = ['images/',datestr(now, 'mmdd'),'_finalFigures/']; createDir(imagesPath); %territoriesSource,'/',
nameVar = 'RT_log2FC_average'; % RT_log2FC_average RT_perSamples_log2FCMean_average RT_perSamples_log2FCMedian_average
% fig = createMaximisedFigure(1); plot(tableSignatures.(nameVar), tableSignatures_AR.(nameVar), 'o', 'MarkerFaceColor', 'c'); text(tableSignatures.(nameVar), tableSignatures_AR.(nameVar), tableSignatures.signatureNameShort, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom'); xlabel('method 1 (outside protein coding genes and low mappability regions)'); ylabel('method 2 (all regions)'); title('mean replication timing (log fold-change)'); mySaveAs(fig, imagesPath, 'FigS20_replicationTiming_basic_AllRegions');
% fig = createMaximisedFigure(2); plot(tableSignatures.(nameVar), tableSignatures_OAGAB.(nameVar), 'o', 'MarkerFaceColor', 'c'); text(tableSignatures.(nameVar), tableSignatures_OAGAB.(nameVar), tableSignatures.signatureNameShort, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom'); xlabel('method 1 (outside protein coding genes and low mappability regions)'); ylabel('method 2 (outside all genes and low mappability regions)'); title('mean replication timing (log fold-change)'); mySaveAs(fig, imagesPath, 'FigS20_replicationTiming_basic_OutsideAllGenesAndBlacklisted');
% fig = createMaximisedFigure(2); plot(tableSignatures.(nameVar), tableSignatures_PetrykGM_rep1.(nameVar), 'o', 'MarkerFaceColor', 'c'); text(tableSignatures.(nameVar), tableSignatures_PetrykGM_rep1.(nameVar), tableSignatures.signatureNameShort, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom'); xlabel('method 1 (outside protein coding genes and low mappability regions)'); ylabel('method 2 (OK-seq)'); title('mean replication timing (log fold-change)'); mySaveAs(fig, imagesPath, 'FigS20_replicationTiming_basic_OutsideAllGenesAndBlacklisted');
% fig = createMaximisedFigure(3); plot(tableSignatures_OAGAB.(nameVar), tableSignatures_AR.(nameVar), 'o', 'MarkerFaceColor', 'c'); text(tableSignatures_OAGAB.(nameVar), tableSignatures_AR.(nameVar), tableSignatures.signatureNameShort, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom');
% fig = createMaximisedFigure(3); plot(tableSignatures_AR.(nameVar), tableSignatures_OAGAB.(nameVar), 'o', 'MarkerFaceColor', 'c'); text(tableSignatures_AR.(nameVar), tableSignatures_OAGAB.(nameVar), tableSignatures_AR.signatureNameShort, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom');
%
plotExposureResults_combined
plotExposureResults_heatmapSummary
%%
plotFigures = false;
if (plotFigures)
    plotExposureResults_volcanoPlot(replData, territoriesSource, savePathBasic, nameDataCT);
    plotExposureResults_combined(replData, territoriesSource, decomposedSignatures, signatureExposuresComplex_cell_Besnard1k, signatureExposuresSimple_LeadLagg_cell_Besnard1k, tableAllSamples_Besnard1k);
    plotExposureResults_heatmapSummary(replData, territoriesSource, decomposedSignatures);
    plotExposureResults_validatePatterns(replData, territoriesSource);
    plotExposureResults_directionalSignatures(replData, territoriesSource, decomposedSignatures);
    plotExposureResults_methodsComparison(replData, territoriesSource, signatureExposuresSimple_LeadLagg_cell_Besnard1k, replData_OAGAB, replData_AR);
    plotExposureResults_aroundORIplots
    compareMSIallSignatures
    writetable(replData.tableAllSamples, 'images\tableAllSamples.xlsx');
    %         plotExposureResults_methodsComparison2
    %         compareMSI(replData, territoriesSource, decomposedSignatures)
    %         plotExposureResults_scatterPlot(replData, territoriesSource);
    %         plotExposureResults_heatmapTrinucleotides(replData, territoriesSource);
    %         plotExposureResults_heatmapSignatures(replData, territoriesSource);
    %         [signatureResults] = plotExposureResults_histogramSignatures(replData, territoriesSource);
    %         plotExposureResults_barplotTiming(replData, territoriesSource, signatureResults);
    %         plotExposureResults_oriplotsTrinucleotides(replData, territoriesSource);
    %         plotExposureResults_oriplotsSignatures(replData, territoriesSource, binSize);
    %         plotExposureResults_clustering(replData, territoriesSource, decomposedSignatures, signatureExposuresComplex_cell_Besnard1k);
end
%%
if (false)
    dirDataCT_RandomORI = 'save/RandomORI_territories/replPrepareTables_OutsideGenesAndBlacklisted/'; nameDataCT_RandomORI = 'replPrepareTables_OutsideGenesAndBlacklisted'; territoriesSource_RandomORI = 'RandomORI_territories'; binSize_RandomORI = 1000;
    tableTerritoriesFileName_RandomORI = [savePathBasic, 'table.', territoriesSource_RandomORI, '.mat'];  nameTerritories_RandomORI = territoriesSource_RandomORI;
    replData_RandomORI = loadReplicationData(alwaysComputeReplication, savePathBasic, dirDataCT_RandomORI, nameDataCT_RandomORI, tableTerritoriesFileName_RandomORI, nameTerritories_RandomORI, decomposedSignatures);
    tableSignatures_RandomORI = loadDataForVolcanoPlot(false, savePathBasic, territoriesSource_RandomORI, nameDataCT_RandomORI, replData_RandomORI);
    [signatureExposuresComplex_cell_RandomORI, signatureExposuresSimple_LeadLagg_cell_RandomORI, tableAllSamples_RandomORI] = loadSignatureExposuresComplex_cell(alwaysComputeReplication, savePathBasic, dirDataCT_RandomORI, nameDataCT_RandomORI, tableTerritoriesFileName_RandomORI, nameTerritories_RandomORI, decomposedSignatures);
end
%%
% plotExposureResults_combined(replData_RandomORI, territoriesSource_RandomORI, decomposedSignatures, signatureExposuresComplex_cell_Besnard1k, signatureExposuresSimple_LeadLagg_cell_Besnard1k, tableAllSamples_Besnard1k);
%%
fprintf('%s %s %s END\n', datestr(now, 'dd-mm-yyyy HH-MM-SS'), compName, scriptName);
diary off;
if (sendingEmails)
    sendEmail('popelovam@seznam.cz', sprintf('hmC: %s %s', compName, scriptName), 'successfully finished', diaryFile);
end