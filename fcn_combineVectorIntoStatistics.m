function result = fcn_combineVectorIntoStatistics(numeratorType1, numeratorType2, denominatorType1, denominatorType2)

if (sum(numeratorType1 > denominatorType1)>0 || sum(numeratorType2 > denominatorType2)>0)
    fprintf('numeratorType1:\n')
    numeratorType1
    fprintf('denominatorType1:\n')
    denominatorType1
    fprintf('numeratorType2:\n')
    numeratorType2
    fprintf('denominatorType2:\n')
    denominatorType2    
    error('ERROR: more mutations than locations where they could occur.\n');
end

ratioType1 = numeratorType1./denominatorType1;
ratioType2 = numeratorType2./denominatorType2;
result.mutType1 = sum(numeratorType1);
result.mutType2 = sum(numeratorType2);
result.mfType1 = sum(numeratorType1)/sum(denominatorType1);
result.mfType2 = sum(numeratorType2)/sum(denominatorType2);
result.asymmetryDifference = nanmean(ratioType1 - ratioType2);
result.asymmetryPValue = signtest(...
    ratioType1((~isnan(ratioType1) & ~isnan(ratioType2))), ...
    ratioType2((~isnan(ratioType1) & ~isnan(ratioType2))));
