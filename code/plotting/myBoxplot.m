function myBoxplot(xValue, values, width, faceColour, edgeColour)


val25 = quantile(values, 0.25);
val50 = quantile(values, 0.50);
val75 = quantile(values, 0.75);
rectangle('Position', [xValue-width/2 val25 width (val75-val25)], 'FaceColor', [faceColour, 0.5], 'EdgeColor', edgeColour)
plot(xValue+width/2*[-1,1], val50*[1,1], '-', 'Color', edgeColour);
