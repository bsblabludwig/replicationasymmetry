% function plotExposureResults_methodsComparison2(territoriesSource, tableSignatures, tableSignatures_OAGAB

imagesPath = ['images/',datestr(now, 'mmdd'),'_finalFigures/']; createDir(imagesPath);
nameVar = 'RT_log2FC_average'; % RT_log2FC_average RT_perSamples_log2FCMean_average RT_perSamples_log2FCMedian_average


fig = createMaximisedFigure(1); hold on;
xValues = tableSignatures.(nameVar);        xLabelText = 'method 1 (outside protein coding genes and low mappability regions)';
yValues = tableSignatures_OAGAB.(nameVar);  yLabelText = 'method 2 (outside all genes and low mappability regions)'; figName = 'outsideAllGenes';
% yValues = tableSignatures_AR.(nameVar);  yLabelText = 'method 2 (all regions)'; figName = 'allRegions';
% yValues = tableSignatures_Besnard1k.(nameVar); yLabelText = 'method 2 (SNS-seq)'; figName = 'Besnard1k';
labels = tableSignatures.signatureNameShort;
fontSize = 14;
ignoreDist = 0.05; maxForce = 0.01; shiftDist = 0.015; scaleBorderForce= 1;
hOriginalDots = plot(xValues, yValues, 'o');
[xValuesText, yValuesText] = labelRepelSimple(xValues, yValues, labels, fontSize-4, ignoreDist, maxForce, shiftDist, scaleBorderForce);
delete(hOriginalDots);
plot(xValues, yValues, 'o', 'MarkerFaceColor', [1,.5,0], 'MarkerEdgeColor', [.8,.3,.2]);
text(xValuesText, yValuesText, labels, 'HorizontalAlignment', 'center', 'FontSize', fontSize-4, 'Color', 0.5*[1,1,1]);
% xlabel(sprintf('method 1 (from %s)', territoryNamesLabel{iTerritoryName}), 'FontSize', fontSize);
% ylabel(sprintf('method 2 (from %s)', territoryNamesLabel{jTerritoryName}), 'FontSize', fontSize);
xlabel(xLabelText, 'FontSize', fontSize);
ylabel(yLabelText, 'FontSize', fontSize);
title('mean replication timing (log fold-change)', 'FontSize', fontSize);
xLimVal = get(gca, 'XLim'); yLimVal = get(gca, 'YLim');
text(xLimVal(1), yLimVal(2), {' ', sprintf('  r = %.4f', corr(xValues, yValues))}, 'FontSize', fontSize, 'FontAngle', 'italic');
mySaveAs(fig, imagesPath, ['FigS23_', figName]);





% plot(xValues, yValues, 'o', 'MarkerFaceColor', 'c'); 
% text(tableSignatures.(nameVar), tableSignatures_OAGAB.(nameVar), tableSignatures.signatureNameShort, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom'); 
% xlabel('method 1 (outside protein coding genes and low mappability regions)'); 
% ylabel('method 2 (outside all genes and low mappability regions)'); 
% title('mean replication timing (log fold-change)'); 
% mySaveAs(fig, imagesPath, 'FigS20_replicationTiming_basic_OutsideAllGenesAndBlacklisted');


%% OLD %%
% fig = createMaximisedFigure(1); plot(tableSignatures.(nameVar), tableSignatures_AR.(nameVar), 'o', 'MarkerFaceColor', 'c'); text(tableSignatures.(nameVar), tableSignatures_AR.(nameVar), tableSignatures.signatureNameShort, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom'); xlabel('method 1 (outside protein coding genes and low mappability regions)'); ylabel('method 2 (all regions)'); title('mean replication timing (log fold-change)'); mySaveAs(fig, imagesPath, 'FigS20_replicationTiming_basic_AllRegions');
% fig = createMaximisedFigure(2); plot(tableSignatures.(nameVar), tableSignatures_OAGAB.(nameVar), 'o', 'MarkerFaceColor', 'c'); text(tableSignatures.(nameVar), tableSignatures_OAGAB.(nameVar), tableSignatures.signatureNameShort, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom'); xlabel('method 1 (outside protein coding genes and low mappability regions)'); ylabel('method 2 (outside all genes and low mappability regions)'); title('mean replication timing (log fold-change)'); mySaveAs(fig, imagesPath, 'FigS20_replicationTiming_basic_OutsideAllGenesAndBlacklisted');
% fig = createMaximisedFigure(2); plot(tableSignatures.(nameVar), tableSignatures_PetrykGM_rep1.(nameVar), 'o', 'MarkerFaceColor', 'c'); text(tableSignatures.(nameVar), tableSignatures_PetrykGM_rep1.(nameVar), tableSignatures.signatureNameShort, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom'); xlabel('method 1 (outside protein coding genes and low mappability regions)'); ylabel('method 2 (OK-seq)'); title('mean replication timing (log fold-change)'); mySaveAs(fig, imagesPath, 'FigS20_replicationTiming_basic_OutsideAllGenesAndBlacklisted');
% fig = createMaximisedFigure(3); plot(tableSignatures_OAGAB.(nameVar), tableSignatures_AR.(nameVar), 'o', 'MarkerFaceColor', 'c'); text(tableSignatures_OAGAB.(nameVar), tableSignatures_AR.(nameVar), tableSignatures.signatureNameShort, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom');
% fig = createMaximisedFigure(3); plot(tableSignatures_AR.(nameVar), tableSignatures_OAGAB.(nameVar), 'o', 'MarkerFaceColor', 'c'); text(tableSignatures_AR.(nameVar), tableSignatures_OAGAB.(nameVar), tableSignatures_AR.signatureNameShort, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom');
