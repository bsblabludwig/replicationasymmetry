% function plotExposureResults_volcanoPlot(replData, territoriesSource, savePathBasic, nameDataCT)
fontName = 'Trebuchet MS';
set(0,'defaultAxesFontName',fontName);
set(0,'defaultTextFontName',fontName);
imagesPath = ['images/',datestr(now, 'mmdd'),'_finalFigures/']; createDir(imagesPath);
%%

nSignatures = replData.nSignatures;
tissueNames = unique(replData.tableCancerTypes.Tissue); nTissues = length(tissueNames);
% patternNames = replData.patternNames;
% maxPattern = length(patternNames);
% structAllSamples = replData.structAllSamples;
% tableAllSamples = replData.tableAllSamples;
signatureExposuresSimple_LeadLagg = replData.signatureExposuresSimple.LeadLagg;
nSamples = size(replData.tableAllSamples, 1);

% colours.leading = [1,.3,.3];
% colours.lagging = [.3,.3,1];
% colours.leadingNotSignif = (2+colours.leading)/3;
% colours.laggingNotSignif = (2+colours.lagging)/3;
% colours.titleSignificant = 0*[1,1,1];
% colours.titleNotSignif = 0.7*[1,1,1];
% colours.patternColormap = jet(6)*0.7;
colours.tissuesColourmap = hsv(nTissues);
colours.tissuesMarkers = {'o', 's', 'p', 'd'}; nM = length(colours.tissuesMarkers);
signatureNames = replData.signatureNames;
signatureShortNames = strrep(strrep(signatureNames, 'Signature ', ''), 'Signature', '');
mfTypeNames = {'matching', 'inverse', 'total', 'average'};

% imagesPath = ['images/',datestr(now, 'mmdd'),'_replPrepareTables/',territoriesSource,'/']; createDir(imagesPath);
% imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_scatterPlots/']; createDir(imagesPath);
% imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_finalFigures/']; createDir(imagesPath);

% iTissue = 1;
% [~, listSamples] = sort(replData.tableAllSamples.Tissue);
% isSampleThisTissue = true(size(replData.tableAllSamples, 1), 1); %ismember(replData.tableAllSamples.Tissue, tissueNames{iTissue})';
% simpleType = 'LeadLagg';
alwaysCompute = false;
tableSignatures = loadDataForVolcanoPlot(alwaysCompute, savePathBasic, territoriesSource, nameDataCT, replData);
%%
for iSignature = 1:nSignatures
    fprintf('%s: %f\n', tableSignatures.signatureNameShort{iSignature}, tableSignatures.RT_log2FC_average(iSignature));
end
%%
fig = figure(3); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition',1.3*[0 0 30 30],'units','normalized','outerposition',[0 0 1 1]);

positionsMain = NaN*ones(3,4); positionsInset = NaN*ones(3,4);
positionsMain(1,:) = [0.06, 0.5, 0.92, 0.46]; %x: 0.06-0.98, y: 0.52-0.96
positionsInset(1,:) = [0.13, 0.7, 0.37, 0.22]; %x: 0.13-0.50, y: 0.80-0.97

% positionsMain(2,:) = [0.55, 0.24, 0.43, 0.32]; %x: 0.55-0.98, y: 0.24-0.56
% positionsInset(2,:) = [0.55, 0.02, 0.43, 0.20]; %x: 0.55-0.98, y: 0.02-0.22
% 
% positionsMain(3,:) = [0.06, 0.24, 0.43, 0.32]; %x: 0.06-0.49, y: 0.24-0.56
% positionsInset(3,:) = [0.06, 0.02, 0.43, 0.20]; %x: 0.06-0.49, y: 0.02-0.22

positionsMain(5,:) = [0.06, 0.02, 0.92, 0.46]; %x: 0.06-0.98, y: 0.02-0.48
positionsInset(5,:) = [0.13, 0.19, 0.37, 0.22]; %


posTitle.xShift = 0.04;
posTitle.yShift = 0.01;
posTitle.width = 0.02;
posTitle.height = 0.02;
posTitle.fontSize = 18;
posTitle.letters = {'A', 'B', 'C', 'D', 'E', 'F'}; %{'a', 'b', 'c', 'd', 'e', 'f'};

iS = 1;
for iType = [1,5]
    if (iType == 1)
        titleText = 'replication strand asymmetry';
        xVar = 'medianDifference'; xLabelText = 'median asymmetry ({\bfmatching} exposure - {\bfinverse} exposure)';
        yVar = 'negLogQValue';
        significantVar = 'isSignificant';
        insetVar = 'percentagePositiveSamplesMinus50';
        yLabelTextInset = 'Samples with dominant matching exposure (%)  '; %{'Samples with dominant', 'matching exposure (%)'}; %{'Percentage of samples with', 'dominant matching exposure (%)'}
        plottedLabelsVar = 'isSignificant'; %'isSignificant';
        positiveColour = [1,0.1,0.2]; positiveText = {' ' , ' ' , 'matching exposure'}; %{' ', 'matching', 'exposure'};
        negativeColour = [0,0.5,1]; negativeText = {' ' , ' ' , 'inverse exposure'}; %{' ', 'inverse', 'exposure'};
        
        for iSignature = 1:nSignatures
            fprintf('%s: median asymmetry %f, Q value %.1e.\n', tableSignatures.signatureNameShort{iSignature}, tableSignatures.(xVar)(iSignature), 10^(-tableSignatures.(yVar)(iSignature)));
        end
    else 
        mfTypeName = mfTypeNames{iType-1};
        titleText = 'replication timing'; %[mfTypeName, ' exposure'];
        xLabelText = 'log_2 fold change from {\bfearly} to {\bflate}';% ({\bflate}/{\bfearly})'; %['log_2 fold change of replication timing: ', mfTypeName, ' exposure']; replication timing
        xVar = ['RT_log2FC_', mfTypeName];
        yVar = ['RT_perSamples_signSlopeFC_negLogQValue_', mfTypeName]; %RT_perSamples_signSlopeFC_negLogQValue_ RT_perSamples_negLogQValue_
        significantVar = ['RT_perSamples_signSlopeFC_isSignificant_', mfTypeName]; %RT_perSamples_signSlopeFC_isSignificant_ RT_perSamples_isSignificant_
        %tableSignatures.(['RT_perSamples_isSignificant_', mfTypeName]) = tableSignatures.(['RT_perSamples_pValue_', mfTypeName]) < 0.05/nSignatures;
        %insetVar = ['RT_perSamples_percentagePositiveSlope_', mfTypeName]; 
        yLabelTextInset = 'Samples correlated with replication timing'; %{'Percentage of samples with positive correlation', 'with replication timing (%)'}; {'Samples correlated with', 'replication timing'}
        plottedLabelsVar = 'allTrue';
        positiveColour = [0.9608    0.5098    0.1451]; positiveText = {' ' , ' ' , 'positive slope'}; %{' ', 'positive', 'slope'}; %red-ish orange[1,.3,.3], other orange [0.93,0.25,0.24],
        negativeColour = [.1,.2,.5]; negativeText = {' ' , ' ' , 'negative slope'}; %{' ', 'negative', 'slope'};
        for iSignature = 1:nSignatures
            fprintf('%s: RT_log2FC %f, Q value %.1e.\n', tableSignatures.signatureNameShort{iSignature}, tableSignatures.(xVar)(iSignature), 10^(-tableSignatures.(yVar)(iSignature)));
        end
    end
    
    positionVector = positionsMain(iType, :); clear pos
    axes('Position', positionVector); hold on;  pos.x = positionVector(1); pos.y = positionVector(2); pos.width = positionVector(3); pos.height = positionVector(4);  
    plot_volcano_main(tableSignatures, signatureShortNames, iType, xVar, xLabelText, yVar, significantVar, plottedLabelsVar, positiveColour, positiveText, negativeColour, negativeText, titleText);
    
    axes('Position', [pos.x - posTitle.xShift, pos.y + pos.height + posTitle.yShift, posTitle.width, posTitle.height]); axis off;
    text(.5,.5,posTitle.letters{iS},'FontWeight', 'bold', 'FontSize', posTitle.fontSize+2, 'HorizontalAlignment', 'center'); iS = iS + 1;
    
    positionVector = positionsInset(iType, :);
    axes('Position', positionVector); set(gca, 'color', 'none'); hold on;   pos.x = positionVector(1); pos.y = positionVector(2); pos.width = positionVector(3); pos.height = positionVector(4);  
    if (iType == 1)
        plot_volcano_insetAsymmetry(tableSignatures, signatureShortNames, significantVar, positiveColour, negativeColour, insetVar, yLabelTextInset)
    else
        plot_volcano_insertTiming(tableSignatures, signatureShortNames, significantVar, positiveColour, negativeColour, mfTypeName, yLabelTextInset)
    end
    
    axes('Position', [pos.x - posTitle.xShift, pos.y + pos.height + posTitle.yShift, posTitle.width, posTitle.height]); axis off;
    text(.5,.5,posTitle.letters{iS},'FontWeight', 'bold', 'FontSize', posTitle.fontSize+2, 'HorizontalAlignment', 'center'); iS = iS + 1;
end
mySaveAs(fig, imagesPath, 'Fig2', true);
%     mySaveAs(fig, imagesPath, ['percentageBarPlot_', type, '_', num2str(minMeanExposure), '_', territoriesSource]);
    
% save('workspaces\workspace_2017_02_04.mat', '-v7.3');
% save('workspaces\workspace_2017_02_09.mat', '-v7.3');
% save('workspaces\workspace_2017_02_15.mat', '-v7.3');

%%
% 1.3*[0 0 30 28]
%     if (iType == 1)
%         axes('Position', [0.06, 0.51, 0.92, 0.46]); hold on; %x: 0.06-0.98, y: 0.52-0.97
%     elseif (iType == 3)
%         axes('Position', [0.06, 0.02, 0.43, 0.45]); hold on; %x: 0.06-0.49, y: 0.02-0.47
%     elseif (iType == 2)
%         axes('Position', [0.55, 0.02, 0.43, 0.45]); hold on; %x: 0.55-0.98, y: 0.02-0.47
%     end
%     
%     plot_volcano_main(tableSignatures, signatureShortNames, iType, xVar, xLabelText, yVar, significantVar, plottedLabelsVar, positiveColour, positiveText, negativeColour, negativeText, titleText);
%     
%     if (iType == 1)
%         positionValue = [0.15, 0.73, 0.35, 0.25]; %annotation('rectangle', positionValue, 'FaceColor','blue','FaceAlpha',.2)
%         axes('Position', positionValue); set(gca, 'color', 'none'); hold on;  %x: 0.12-0.20, y: 0.85-0.97
%         plot_volcano_insetAsymmetry(tableSignatures, signatureShortNames, significantVar, positiveColour, negativeColour, insetVar, yLabelTextInset)
%     elseif (iType == 3)
%         axes('Position', [0.1, 0.2, 0.18, 0.25]); set(gca, 'color', 'none'); hold on; 
%         plot_volcano_insertTiming(tableSignatures, signatureShortNames, significantVar, positiveColour, negativeColour, mfTypeName)
%     elseif (iType == 2)
%         axes('Position', [0.58, 0.2, 0.18, 0.25]); set(gca, 'color', 'none'); hold on; 
%         plot_volcano_insertTiming(tableSignatures, signatureShortNames, significantVar, positiveColour, negativeColour, mfTypeName)
%     end