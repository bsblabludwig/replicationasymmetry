% function plotExposureResults_combined(replData, territoriesSource, decomposedSignatures, signatureExposuresComplex_cell_Besnard1k, signatureExposuresSimple_LeadLagg_cell_Besnard1k, tableAllSamples_Besnard1k)

%%
fontName = 'Trebuchet MS';
set(0,'defaultAxesFontName',fontName);
set(0,'defaultTextFontName',fontName);
%%
% Here we will plot signature plot, H ori plot, B ori plot, repl timing barplot, histogram, etc.
nSignatures = replData.nSignatures;
signatureNames = replData.signatureNames;
signatureShortNames = strrep(strrep(signatureNames, 'Signature ', ''), 'Signature', 'S');
signatureNames = strrep(signatureNames, 'Signature', 'Signature ');
tissueNames = unique(replData.tableCancerTypes.Tissue); nTissues = length(tissueNames);
tableAllSamples = replData.tableAllSamples;
signatureExposuresSimple_LeadLagg = replData.signatureExposuresSimple.LeadLagg;
signatureExposuresSimple_matrix = replData.signatureExposuresSimple.LeadLagg.type1Minustype2;
signatureExposuresComplex_cell_ORI_Haradhvala = replData.signatureExposuresComplex.DistanceFromORI_LeadLagg;

binSize_Haradhvala = 20000;
binSize_Besnard1k = 1000;
nORIValues_Haradhvala = length(signatureExposuresComplex_cell_ORI_Haradhvala); %replData.dataFields.nValues.DistanceFromORI_LeadLagg;
nORIValues_Besnard1k = length(signatureExposuresComplex_cell_Besnard1k);
ORI_text_Haradhvala = 'left/right transition';
ORI_text_Besnard = 'ORI';

finalSignatures = decomposedSignatures.finalSignatures;
finalSignatures.nSignatures = length(finalSignatures.signaturesNames);
types = decomposedSignatures.types;
subtypes = decomposedSignatures.subtypes;
strandSpecText = 'strandSpec';
%%
tissueNamesShort = strrep(tissueNames, '_', ' ');
tissueNamesShort{strcmp(tissueNamesShort, 'blood lymphoid')} = 'blood l.';
tissueNamesShort{strcmp(tissueNamesShort, 'blood myeloid')} = 'blood m.';
tissueNamesShort{strcmp(tissueNamesShort, 'kidney clear cell')} = 'kidney';
tissueNamesShort{strcmp(tissueNamesShort, 'lung adenocarcinoma')} = 'lung ad.';
tissueNamesShort{strcmp(tissueNamesShort, 'lung squamous')} = 'lung sq.';
tissueNamesShort{strcmp(tissueNamesShort, 'oesophagus adenocarcinoma')} = 'oeso.';
tissueNamesMiddle = strrep(tissueNames, '_', ' ');
tissueNamesMiddle{strcmp(tissueNamesMiddle, 'lung adenocarcinoma')} = 'lung adeno.';
tissueNamesMiddle{strcmp(tissueNamesMiddle, 'oesophagus adenocarcinoma')} = 'oesophagus adeno.';
%%
complexType = 'ReplicationTiming_LeadLagg';     nCurrentValues_RT = replData.dataFields.nValues.ReplicationTiming_LeadLagg;    labelsExtremes = {'early', 'late'};
% complexType = 'NOS_LeadLagg';     nCurrentValues = nValues.NOS_LeadLagg;    labelsExtremes = {'low', 'high'};
% typeNames = {'type1Minustype2', 'totalMF', 'type1', 'type2'};
% printNames = {'leading - lagging', 'total exposure', 'leading', 'lagging'};
signatureExposuresComplex_cell_RT_diff = replData.signatureExposuresComplex.(complexType);
%%
% imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_combined/']; createDir(imagesPath);
imagesPath = ['images/',datestr(now, 'mmdd'),'_finalFigures/']; createDir(imagesPath); %territoriesSource,'/', %RandomORI
minMeanExposure = 10;
font_size = 12;
% fontName = 'Helvetica';% 'Lucida Sans'; % Console
colours.leadingDark = [1,0,0];
colours.laggingDark = [0,0,1];
colours.leading = [1,.3,.3];
colours.lagging = [.3,.3,1];
colours.leadingNotSignif = (2+colours.leading)/3;
colours.laggingNotSignif = (2+colours.lagging)/3;
colours.titleSignificant = 0*[1,1,1];
colours.titleNotSignif = 0.7*[1,1,1];
colours.patternColormap = jet(6)*0.7;
colours.plus = [.75,.1,1];
colours.minus = [.1,.75,.5];
colours.tissuesColourmap = hsv(nTissues);
%% clear signatureResults
pValue = NaN*ones(nSignatures, 1);
signatureResults = table(pValue);
signatureResults.meanExposure = NaN*ones(nSignatures, 1);
signatureResults.medianExposure = NaN*ones(nSignatures, 1);
signatureResults.hasStrandAsymmetry = false(nSignatures, 1);
signatureResults.colourTitle = zeros(nSignatures, 3);
signatureResults.colourPositiveBars = zeros(nSignatures, 3);
signatureResults.colourNegativeBars = zeros(nSignatures, 3);
signatureResults.colourGCA = zeros(nSignatures, 3);
signatureResults.titleText = cell(nSignatures, 3);
for iSignature = 1:nSignatures
    isSampleUsed = signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure; % Only samples which are exposed to this signature are taken into account.
    exposuresDifference = signatureExposuresSimple_matrix(iSignature, isSampleUsed);
    signatureResults.pValue(iSignature) = signtest(exposuresDifference); %signrank
    signatureResults.meanExposure(iSignature) = mean(exposuresDifference);
    signatureResults.medianExposure(iSignature) = median(exposuresDifference);
end
[~, ~, ~, qValues]=fdr_bh(signatureResults.pValue);
signatureResults.qValue_BenjaminiHochberg = qValues;
signatureResults.qValue_Bonferroni = signatureResults.pValue*nSignatures;
signatureResults.isLeading = (signatureResults.qValue_Bonferroni < 0.05 & signatureResults.medianExposure > 0); %signatureResults.meanExposure > 0 &
signatureResults.isLagging = (signatureResults.qValue_Bonferroni < 0.05 & signatureResults.medianExposure < 0); %signatureResults.meanExposure < 0 &
signatureResults.hasStrandAsymmetry(signatureResults.isLeading | signatureResults.isLagging) = true;
for iSignature = 1:nSignatures
    if (signatureResults.isLeading(iSignature))
        signatureResults.titleText{iSignature} = sprintf('%s: leading %s', signatureNames{iSignature}, getPValueAsText(signatureResults.pValue(iSignature)));
        signatureResults.colourTitle(iSignature,:) = colours.leadingDark;
    elseif (signatureResults.isLagging(iSignature))
        signatureResults.titleText{iSignature} = sprintf('%s: lagging %s', signatureNames{iSignature}, getPValueAsText(signatureResults.pValue(iSignature)));
        signatureResults.colourTitle(iSignature,:) = colours.laggingDark;
    else
        signatureResults.titleText{iSignature} = sprintf('%s n.s.', signatureNames{iSignature});
        signatureResults.colourTitle(iSignature,:) = colours.titleNotSignif;
    end
    if (signatureResults.hasStrandAsymmetry(iSignature))
        signatureResults.colourPositiveBars(iSignature,:) = colours.leading;
        signatureResults.colourNegativeBars(iSignature,:) = colours.lagging;
        signatureResults.colourGCA(iSignature,:) = colours.titleSignificant;
    else
        signatureResults.colourPositiveBars(iSignature,:) = colours.leadingNotSignif;
        signatureResults.colourNegativeBars(iSignature,:) = colours.laggingNotSignif;
        signatureResults.colourGCA(iSignature,:) = colours.titleSignificant;%colours.titleNotSignif;
    end
end
%%
special_R23 = false;
% listSignatures = [12, 10, 17, 16, 4, 6]; %[12, 2, 16, 4, 7, 10, 13, 17, 25, 19, 20, 6, 14, 24, 29];
%     'Signature 1'     group
%     'Signature 2'     group APOBEC
%     'Signature 3'     group
%     'Signature 4'     group mutagen
%     'Signature 5'     group symmetric
%     'Signature 6'     group MSI
%     'Signature 7'     group mutagen
%     'Signature 8'     group symmetric
%     'Signature 9'     group
%     'Signature 10'     group POLE
%     'Signature 12'     group liver
%     'Signature 13'     group APOBEC
%     'Signature 14'     group POLE
%     'Signature 15'     group MSI
%     'Signature 16'     group liver
%     'Signature 17'     group mutagen
%     'Signature 18'     group POLE
%     'Signature 19'     group symmetric
%     'Signature 20'     group MSI
%     'Signature 21'     group MSI
%     'Signature 22'     group mutagen
%     'Signature 23'     group symmetric
%     'Signature 25'     group symmetric
%     'Signature 26'     group MSI
%     'Signature 28'     group POLE
%     'Signature  N1'     group
%     'Signature  N2'     group
%     'Signature  N3'     group mutagen
%     'Signature  N4'     group MSI
% listSignatures = [12, 10, 10, 16, 4, 6];
iL = 1; clear listSignatures;  % Good, 5+2+4+6+5+2+5=29
% listSignatures{iL} = [2, 12];                   group_names{iL} = 'APOBEC signatures';  figName{iL} = 'Fig3_selected';  specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = true; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% listSignatures{iL} = [6, 10, 10, 7, 16, 5];     group_names{iL} = '';                   figName{iL} = 'Fig4_selected';  specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = true; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% listSignatures{iL} = [6, 14, 19, 20, 24, 29];   group_names{iL} = 'MMR signatures';     figName{iL} = 'FigS2_selected'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = true; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% listSignatures{iL} = [10, 13, 17, 25];          group_names{iL} = 'POLE signatures';    figName{iL} = 'FigS4_selected'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = true; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% listSignatures{iL} = [4, 7, 16, 21, 28];        group_names{iL} = 'mutagen signatures'; figName{iL} = 'FigS6_selected'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = true; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% listSignatures{iL} = [11, 15];                  group_names{iL} = 'liver signatures';   figName{iL} = 'FigS8_selected'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = true; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% listSignatures{iL} = [1, 3, 9, 26, 27];         group_names{iL} = 'other signatures';   figName{iL} = 'FigS9_selected'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = true; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% listSignatures{iL} = [26, 27, 28, 29];          group_names{iL} = 'new signatures';     figName{iL} = 'FigS10_selected'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = true; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% listSignatures{iL} = [5, 8, 18, 22, 23];        group_names{iL} = 'no/little asymmetry';figName{iL} = 'FigS11_selected'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = true; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
%
% listSignatures{iL} = [2, 12];                   group_names{iL} = 'APOBEC signatures';  figName{iL} = 'Fig3_other';  specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = true; iL = iL + 1;
% listSignatures{iL} = [6, 10, 10, 7, 16, 5];     group_names{iL} = '';                   figName{iL} = 'Fig4_other';  specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = true; iL = iL + 1;
% listSignatures{iL} = [6, 14, 19, 20, 24, 29];   group_names{iL} = 'MMR signatures';     figName{iL} = 'FigS2_other'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = true; iL = iL + 1;
% listSignatures{iL} = [10, 13, 17, 25];          group_names{iL} = 'POLE signatures';    figName{iL} = 'FigS4_other'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = true; iL = iL + 1;
% listSignatures{iL} = [4, 7, 16, 21, 28];        group_names{iL} = 'mutagen signatures'; figName{iL} = 'FigS6_other'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = true; iL = iL + 1;
% listSignatures{iL} = [11, 15];                  group_names{iL} = 'liver signatures';   figName{iL} = 'FigS8_other'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = true; iL = iL + 1;
% listSignatures{iL} = [1, 3, 9, 26, 27];         group_names{iL} = 'other signatures';   figName{iL} = 'FigS9_other'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = true; iL = iL + 1;
% listSignatures{iL} = [26, 27, 28, 29];          group_names{iL} = 'new signatures';     figName{iL} = 'FigS10_other'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = true; iL = iL + 1;
% listSignatures{iL} = [5, 8, 18, 22, 23];        group_names{iL} = 'no/little asymmetry';figName{iL} = 'FigS11_other'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = true; iL = iL + 1;


% listSignatures{iL} = [2, 12];                   group_names{iL} = 'APOBEC signatures';  figName{iL} = 'Fig3_skin'; specials_R23{iL} = false; specific_tissues{iL} = {'skin'}; tissues_only{iL} = true; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;

% listSignatures{iL} = [2, 12];                   group_names{iL} = 'APOBEC signatures';  figName{iL} = 'Fig3_all'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
listSignatures{iL} = [2, 12];                   group_names{iL} = 'APOBEC signatures';  figName{iL} = 'Fig3'; specials_R23{iL} = false; specific_tissues{iL} = {'breast'}; tissues_only{iL} = true; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
listSignatures{iL} = [2, 12];                   group_names{iL} = 'APOBEC signatures';  figName{iL} = 'Fig3_esad'; specials_R23{iL} = false; specific_tissues{iL} = {'oesophagus_adenocarcinoma'}; tissues_only{iL} = true; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
listSignatures{iL} = [2, 12];                   group_names{iL} = 'APOBEC signatures';  figName{iL} = 'Fig3_lung'; specials_R23{iL} = false; specific_tissues{iL} = {'lung_squamous'}; tissues_only{iL} = true; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
listSignatures{iL} = [2, 12];                   group_names{iL} = 'APOBEC signatures';  figName{iL} = 'Fig3_pancreas'; specials_R23{iL} = false; specific_tissues{iL} = {'pancreas'}; tissues_only{iL} = true; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
listSignatures{iL} = [6, 10, 10, 7, 16, 5];     group_names{iL} = '';                   figName{iL} = 'Fig4'; specials_R23{iL} = true;  specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
listSignatures{iL} = [6, 14, 19, 20, 24, 29];   group_names{iL} = 'MMR signatures';     figName{iL} = 'Fig4supplement1'; specials_R23{iL} = false; specific_tissues{iL} = {'MSI'}; tissues_only{iL} = true; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
listSignatures{iL} = [6, 14, 19, 20, 24, 29];   group_names{iL} = 'MMR signatures';     figName{iL} = 'Fig4supplement2'; specials_R23{iL} = false; specific_tissues{iL} = {'MSI'}; tissues_only{iL} = false; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
listSignatures{iL} = [10, 13, 17, 25];          group_names{iL} = 'POLE signatures';    figName{iL} = 'Fig4supplement3'; specials_R23{iL} = false; specific_tissues{iL} = {'POLE'}; tissues_only{iL} = true; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
listSignatures{iL} = [10, 13, 17, 25];          group_names{iL} = 'POLE signatures';    figName{iL} = 'Fig4supplement4'; specials_R23{iL} = false; specific_tissues{iL} = {'POLE'}; tissues_only{iL} = false; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
listSignatures{iL} = [4, 7, 16, 21, 28];        group_names{iL} = 'mutagen signatures'; figName{iL} = 'Fig4supplement5'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = true; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
listSignatures{iL} = [4, 7, 16, 21, 28];        group_names{iL} = 'mutagen signatures'; figName{iL} = 'Fig4supplement6'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = true; iL = iL + 1;
listSignatures{iL} = [11, 15];                  group_names{iL} = 'liver signatures';   figName{iL} = 'Fig4supplement7'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
listSignatures{iL} = [1, 3, 9, 26, 27];         group_names{iL} = 'other signatures';   figName{iL} = 'Fig4supplement8'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
listSignatures{iL} = [26, 27, 28, 29];          group_names{iL} = 'new signatures';     figName{iL} = 'Fig4supplement9'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
listSignatures{iL} = [5, 8, 18, 22, 23];        group_names{iL} = 'no/little asymmetry';figName{iL} = 'Fig4supplement10'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;

% % listSignatures{iL} = [6, 14, 19, 20, 24, 29];   group_names{iL} = 'MMR signatures';     figName{iL} = 'FigS'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% % listSignatures{iL} = [7, 28];        group_names{iL} = 'mutagen signatures'; figName{iL} = 'FigS'; specials_R23{iL} = false; specific_tissues{iL} = {'skin'}; tissues_only{iL} = true; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% % listSignatures{iL} = [7, 28];        group_names{iL} = 'mutagen signatures'; figName{iL} = 'FigS'; specials_R23{iL} = false; specific_tissues{iL} = {'skin'}; tissues_only{iL} = false; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% % listSignatures{iL} = [4];        group_names{iL} = 'mutagen signatures'; figName{iL} = 'FigS'; specials_R23{iL} = false; specific_tissues{iL} = {'lung_adenocarcinoma','lung_squamous'}; tissues_only{iL} = true; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% % listSignatures{iL} = [4];        group_names{iL} = 'mutagen signatures'; figName{iL} = 'FigS'; specials_R23{iL} = false; specific_tissues{iL} = {'lung_adenocarcinoma','lung_squamous'}; tissues_only{iL} = false; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% % listSignatures{iL} = [21];        group_names{iL} = 'mutagen signatures'; figName{iL} = 'FigS'; specials_R23{iL} = false; specific_tissues{iL} = {'kidney_clear_cell'}; tissues_only{iL} = true; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% % listSignatures{iL} = [21];        group_names{iL} = 'mutagen signatures'; figName{iL} = 'FigS'; specials_R23{iL} = false; specific_tissues{iL} = {'kidney_clear_cell'}; tissues_only{iL} = false; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% % listSignatures{iL} = [16];        group_names{iL} = 'signature 17'; figName{iL} = 'FigS17'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = false; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% % listSignatures{iL} = [12];        group_names{iL} = 'signature 13'; figName{iL} = 'FigS13'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = false; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;

% listSignatures{iL} = [4, 7, 21, 28];        group_names{iL} = 'mutagen signatures'; figName{iL} = 'Fig_mutagens'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = true; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% listSignatures{iL} = [4, 7, 21, 28];        group_names{iL} = 'mutagen signatures'; figName{iL} = 'Fig_mutagens_other'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = true; iL = iL + 1;
% listSignatures{iL} = [16, 16];        group_names{iL} = 'signature 17'; figName{iL} = 'Fig_signature17'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; mutagenGroupTissuesOnly{iL} = true; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;

% % % listSignatures{iL} = [16];        group_names{iL} = 'signature 17'; figName{iL} = 'Fig_signature17_otherSamples'; specials_R23{iL} = false; specific_tissues{iL} = []; tissues_only{iL} = []; %mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = true; iL = iL + 1;
% % listSignatures{iL} = [1:6];   group_names{iL} = 'MMR: signatures 1-6';     figName{iL} = 'MMR_1_6_MSI'; specials_R23{iL} = false; specific_tissues{iL} = {'MSI'}; tissues_only{iL} = true; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% % listSignatures{iL} = [1:6];   group_names{iL} = 'MMR: signatures 1-6';     figName{iL} = 'MMR_1_6_MSS'; specials_R23{iL} = false; specific_tissues{iL} = {'MSI'}; tissues_only{iL} = false; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% % listSignatures{iL} = [7:12];   group_names{iL} = 'MMR: signatures 7-12';     figName{iL} = 'MMR_7_12_MSI'; specials_R23{iL} = false; specific_tissues{iL} = {'MSI'}; tissues_only{iL} = true; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% % listSignatures{iL} = [7:12];   group_names{iL} = 'MMR: signatures 7-12';     figName{iL} = 'MMR_7_12_MSS'; specials_R23{iL} = false; specific_tissues{iL} = {'MSI'}; tissues_only{iL} = false; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% % listSignatures{iL} = [13:18];   group_names{iL} = 'MMR: signatures 13-18';     figName{iL} = 'MMR_13_18_MSI'; specials_R23{iL} = false; specific_tissues{iL} = {'MSI'}; tissues_only{iL} = true; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% % listSignatures{iL} = [13:18];   group_names{iL} = 'MMR: signatures 13-18';     figName{iL} = 'MMR_13_18_MSS'; specials_R23{iL} = false; specific_tissues{iL} = {'MSI'}; tissues_only{iL} = false; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% % listSignatures{iL} = [19:24];   group_names{iL} = 'MMR: signatures 19-24';     figName{iL} = 'MMR_19_24_MSI'; specials_R23{iL} = false; specific_tissues{iL} = {'MSI'}; tissues_only{iL} = true; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% % listSignatures{iL} = [19:24];   group_names{iL} = 'MMR: signatures 19-24';     figName{iL} = 'MMR_19_24_MSS'; specials_R23{iL} = false; specific_tissues{iL} = {'MSI'}; tissues_only{iL} = false; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% % listSignatures{iL} = [25:29];   group_names{iL} = 'MMR: signatures 25-29';     figName{iL} = 'MMR_25_29_MSI'; specials_R23{iL} = false; specific_tissues{iL} = {'MSI'}; tissues_only{iL} = true; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;
% % listSignatures{iL} = [25:29];   group_names{iL} = 'MMR: signatures 25-29';     figName{iL} = 'MMR_25_29_MSS'; specials_R23{iL} = false; specific_tissues{iL} = {'MSI'}; tissues_only{iL} = false; mutagenGroupTissuesOnly{iL} = false; mutagenGroupOtherThanTissues{iL} = false; iL = iL + 1;

exponentValue = 3;
cmapPerTissue = (hsv(nTissues)+1)/2;

for iList = 1:length(listSignatures) %:18%1:9%[1,3:6]%:2 %length(listSignatures)+(-9:-2) %length(listSignatures)-2%+[-2,-1,0]%5:length(listSignatures)
    currentListSignatures = listSignatures{iList};
    group_name = group_names{iList};
    special_R23 = specials_R23{iList};
    specific_tissue = specific_tissues{iList};
    tissue_only = tissues_only{iList};
    nR_plus = 1;
    nR = length(currentListSignatures); nC = 5; iS = 1; xS = 0.8; yS = 0.73; xB = 0.02; yB2 = 0.03; yB = yB2+0.081*log2(6)/max([1, log2(nR)]); xM = -0.03; yM = -0.01+(0.02/max([1, log2(nR)]));
    fig = figure(1); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 42 (nR+nR_plus)*4.8],'units','normalized','outerposition',[0 0 1 1]); colormap([(hsv(nTissues)+1)/2;0.8*[1,1,1]]); %[0 0 42 (nR+nR_plus)*4.4]
    piePlotted = false;
    maxYLimVal = 20;
    if (nR >= 4)
        legShiftHeightFactor = 2; textScaleFactor = 1.5;
    else
        legShiftHeightFactor = 4; textScaleFactor = 1.4;
    end
    plot_xLabel = false; plotAllMutationTypes = false;
    for iR = 1:nR
        iSignature = currentListSignatures(iR);
        title_text = ''; %signatureNames{iSignature};
        if (iR == nR) %(iSignature == currentListSignatures(end))
            plot_xLabel = true;
        end
        if (strcmp(figName{iList}, 'Fig_signature17') && iR == 2) % little hack
            mutagenGroupTissuesOnly{iList} = false; mutagenGroupOtherThanTissues{iList} = true;
        end
        tissue = '';
        if (special_R23 && iR == 2)
            tissue = 'POLE^{mut}';
            isThisT = strcmp(tableAllSamples.Tissue, 'POLE')';
            isThisT_Besnard1k = strcmp(tableAllSamples_Besnard1k.Tissue, 'POLE')';
        elseif (special_R23 && iR == 3)
            tissue = 'POLE^{WT}';
            isThisT = ~strcmp(tableAllSamples.Tissue, 'POLE')';
            isThisT_Besnard1k = ~strcmp(tableAllSamples_Besnard1k.Tissue, 'POLE')';
        elseif (special_R23 && iR == 4)
            tissue = 'skin';
            isThisT = strcmp(tableAllSamples.Tissue, 'skin')';
            isThisT_Besnard1k = strcmp(tableAllSamples_Besnard1k.Tissue, 'skin')';
        elseif (special_R23 && iR == 1)
            tissue = 'MSI';
            isThisT = strcmp(tableAllSamples.Tissue, 'MSI')';
            isThisT_Besnard1k = strcmp(tableAllSamples_Besnard1k.Tissue, 'MSI')';
        elseif (mutagenGroupTissuesOnly{iList})
            if (ismember(iSignature, [4]))
                specific_tissue = {'lung_adenocarcinoma','lung_squamous'}; tissue = 'lung';
            elseif (ismember(iSignature, [7, 28]))
                specific_tissue = {'skin'}; tissue = 'skin';
            elseif (ismember(iSignature, [21]))
                specific_tissue = {'kidney_clear_cell'}; tissue = 'kidney';
            elseif (ismember(iSignature, [16]))
                specific_tissue = {'oesophagus_adenocarcinoma', 'gastric'}; tissue = 'oesophagus, gastric';
            elseif (ismember(iSignature, [2, 12]))
                specific_tissue = {'breast'}; tissue = 'breast';
            elseif (ismember(iSignature, [6, 14, 19, 20, 24, 29, 10]))
                specific_tissue = {'MSI', 'POLE'}; tissue = 'MSI/POLE^{MUT}';
            elseif (ismember(iSignature, [5, 11, 15, 18, 22, 23]))
                specific_tissue = {'liver'}; tissue = 'liver';
            elseif (ismember(iSignature, [1]))
                specific_tissue = {'pancreas'}; tissue = 'pancreas';
            elseif (ismember(iSignature, [3]))
                specific_tissue = {'breast'}; tissue = 'breast';
            elseif (ismember(iSignature, [8, 9]))
                specific_tissue = {'oesophagus_adenocarcinoma'}; tissue = 'oesophagus';
            elseif (ismember(iSignature, [26]))
                specific_tissue = {'POLE'}; tissue = 'POLE^{MUT}';
            elseif (ismember(iSignature, [27]))
                specific_tissue = {'blood_lymphoid'}; tissue = 'blood l.';
            else
                specific_tissue = {'POLE'}; tissue = 'POLE^{MUT}';
            end
            isThisT = ismember(tableAllSamples.Tissue, specific_tissue)';
            isThisT_Besnard1k = ismember(tableAllSamples_Besnard1k.Tissue, specific_tissue)';
        elseif (mutagenGroupOtherThanTissues{iList})
            if (ismember(iSignature, [4]))
                specific_tissue = {'lung_adenocarcinoma','lung_squamous'}; tissue = 'not lung';
            elseif (ismember(iSignature, [7, 28]))
                specific_tissue = {'skin'}; tissue = 'not skin';
            elseif (ismember(iSignature, [21]))
                specific_tissue = {'kidney_clear_cell'}; tissue = 'not kidney';
            elseif (ismember(iSignature, [16]))
                specific_tissue = {'oesophagus_adenocarcinoma', 'gastric'}; tissue = 'not oesophagus, not gastric';
            elseif (ismember(iSignature, [2, 12]))
                specific_tissue = {'breast', 'skin'}; tissue = 'not breast, skin';
            elseif (ismember(iSignature, [6, 14, 19, 20, 24, 29, 10]))
                specific_tissue = {'MSI', 'POLE'}; tissue = 'not MSI/POLE^{MUT}';
            elseif (ismember(iSignature, [5, 11, 15, 18, 22, 23]))
                specific_tissue = {'liver'}; tissue = 'not liver';
            elseif (ismember(iSignature, [1]))
                specific_tissue = {'pancreas'}; tissue = 'not pancreas';
            elseif (ismember(iSignature, [3]))
                specific_tissue = {'breast'}; tissue = 'not breast';
            elseif (ismember(iSignature, [8, 9]))
                specific_tissue = {'oesophagus_adenocarcinoma'}; tissue = 'not oesophagus';
            elseif (ismember(iSignature, [26]))
                specific_tissue = {'POLE'}; tissue = 'not POLE^{MUT}';
            elseif (ismember(iSignature, [27]))
                specific_tissue = {'blood_lymphoid'}; tissue = 'not blood l.';
            else
                specific_tissue = {'POLE'}; tissue = 'not POLE^{MUT}';
            end
            %             tissue = strcat('not_', specific_tissue);% ['not ', strjoin(specific_tissue)];
            isThisT = ~ismember(tableAllSamples.Tissue, specific_tissue)';
            isThisT_Besnard1k = ~ismember(tableAllSamples_Besnard1k.Tissue, specific_tissue)';
        elseif (~isempty(specific_tissue))
            if (tissue_only)
                tissue = strrep(specific_tissue, 'adenocarcinoma', ''); %strjoin(specific_tissue);
                isThisT = ismember(tableAllSamples.Tissue, specific_tissue)';
                isThisT_Besnard1k = ismember(tableAllSamples_Besnard1k.Tissue, specific_tissue)';
            else
                tissue = strcat('not_', specific_tissue);% ['not ', strjoin(specific_tissue)];
                isThisT = ~ismember(tableAllSamples.Tissue, specific_tissue)';
                isThisT_Besnard1k = ~ismember(tableAllSamples_Besnard1k.Tissue, specific_tissue)';
            end
        else
            tissue = ''; %all samples
            isThisT = true(1, size(tableAllSamples, 1));
            isThisT_Besnard1k = true(1, size(tableAllSamples_Besnard1k, 1));
        end
        tissue = strrep(tissue, 'not_MSI', 'MSS');
        yLabel_text = signatureNames{iSignature};
        isSampleUsed = isThisT & signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure; % Only samples which are exposed to this signature are taken into account.
        isSampleUsed_Besnard1k = isThisT_Besnard1k & signatureExposuresSimple_LeadLagg_cell_Besnard1k.meanType1Type2(iSignature, :)>minMeanExposure; % Only samples which are exposed to this signature are taken into account.
        
        
        %         tmp1 = signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, isSampleUsed' & tableAllSamples.iCancerType == 19);
        %         tmp2 = signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, isSampleUsed' & tableAllSamples.iCancerType ~= 19);
        %         fig = createMaximisedFigure(5);
        %         histogram(tmp1); hold on; histogram(tmp2);
        %
        %         tmp1 = signatureExposuresSimple_matrix(iSignature, isSampleUsed' & tableAllSamples.iCancerType == 19);
        %         tmp2 = signatureExposuresSimple_matrix(iSignature, isSampleUsed' & tableAllSamples.iCancerType ~= 19);
        %         fig = createMaximisedFigure(6);
        %         histogram(tmp1); hold on; histogram(tmp2);
        
        exposuresDifference = signatureExposuresSimple_matrix(iSignature, :);
        exposuresMean = signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :);
        quantile75mean = quantile(exposuresMean(isSampleUsed), .75);
        quantile25mean = quantile(exposuresMean(isSampleUsed), .25);
        k = 2;
        minValue = quantile25mean - k*(quantile75mean-quantile25mean);
        maxValue = quantile75mean + k*(quantile75mean-quantile25mean);
        isSampleUsedOriginal = isSampleUsed;
        isSampleUsed = isSampleUsed & exposuresMean>=minValue & exposuresMean<=maxValue;
        nOutliersNegativeDiff = sum(exposuresDifference(isSampleUsedOriginal & ~isSampleUsed)<0);
        nOutliersPositiveDiff = sum(exposuresDifference(isSampleUsedOriginal & ~isSampleUsed)>0);
        plot_quartile = false;
        
        %             exposuresMean = signatureExposuresSimple_LeadLagg_cell_Besnard1k.meanType1Type2(iSignature, :);
        %             quantile75mean = quantile(exposuresMean(isSampleUsed_Besnard1k), .75);
        %             quantile25mean = quantile(exposuresMean(isSampleUsed_Besnard1k), .25);
        %             k = 3;
        %             minValue = quantile25mean - k*(quantile75mean-quantile25mean);
        %             maxValue = quantile75mean + k*(quantile75mean-quantile25mean);
        %             isSampleUsed_Besnard1kOriginal = isSampleUsed_Besnard1k;
        %             isSampleUsed_Besnard1k = isSampleUsed_Besnard1k & exposuresMean>=minValue & exposuresMean<=maxValue;
        
        for iType = 1:nC
            if (iType == 1)
                xS_minus = 0.01;
            else
                xS_minus = 0;
            end
            yB_divide= 1;
            if (special_R23 && iR == 3 && iType == 1)
                iS = iS + 1;
                continue
            elseif (special_R23 && iR == 2 && iType == 1)
                yB_divide = nR;
            end
            myGeneralSubplot(nR,nC,iS,xS-xS_minus,yS,xB,yB/yB_divide,xM,yM); iS = iS + 1; hold on; set(gca, 'Color', 'none');
            if (special_R23 && iR == 2 && iType == 1)
                gcaPos = get(gca, 'Position'); gcaPos(2) = gcaPos(2) - gcaPos(4)/2; set(gca, 'Position', gcaPos);
            end
            
            if (iType == 1)
                plot_signature([finalSignatures.splitSignaturesA(:,iSignature); finalSignatures.splitSignaturesB(:,iSignature)], subtypes.(strandSpecText), types.(strandSpecText), plot_xLabel, 'flipped', title_text, yLabel_text, plotAllMutationTypes, font_size+4, fontName, maxYLimVal, textScaleFactor);
                xVal = 100;
                ax = gca;
            elseif (iType == 2)
                plot_yLabel = true; plotMedianFilter= false;
                plot_oriplot(signatureExposuresComplex_cell_ORI_Haradhvala, iSignature, isSampleUsed, binSize_Haradhvala, nORIValues_Haradhvala, ORI_text_Haradhvala, plotMedianFilter, plot_xLabel, plot_yLabel, title_text, colours, font_size, fontName, legShiftHeightFactor);
                ax = gca; ax.YAxis.Exponent = exponentValue;
                if (iscell(tissue))
                    tissue = tissue{1};
                end
                ylabel({['{\bf ', strrep(tissue, '_', ' '), '}'], 'average exposure'}, 'FontSize', font_size, 'FontName', fontName);
                
            elseif (iType == 3)
                plot_yLabel = true; plotMedianFilter= false;
                plot_oriplot(signatureExposuresComplex_cell_Besnard1k, iSignature, isSampleUsed_Besnard1k, binSize_Besnard1k, nORIValues_Besnard1k, ORI_text_Besnard, plotMedianFilter, plot_xLabel, plot_yLabel, title_text, colours, font_size, fontName, legShiftHeightFactor);
                ax = gca; ax.YAxis.Exponent = exponentValue;
            elseif (iType == 4)
                plot_yLabel = true;
                plot_histogram(signatureExposuresSimple_matrix, iSignature, isSampleUsedOriginal, isSampleUsed, signatureResults, plot_quartile, plot_xLabel, plot_yLabel, title_text, colours, font_size, fontName)
                xLimVal = get(gca, 'XLim'); yLimVal = get(gca, 'YLim');
                if (nOutliersPositiveDiff > 0)
                    text(0.95*xLimVal(2), (1*yLimVal(2)+5*yLimVal(1))/6, sprintf('%d', nOutliersPositiveDiff), 'Color', colours.leading, 'HorizontalAlignment', 'right', 'FontSize', font_size-2, 'FontName', fontName, 'FontWeight', 'bold');
                end
                if (nOutliersNegativeDiff > 0)
                    text(0.95*xLimVal(1), (1*yLimVal(2)+5*yLimVal(1))/6, sprintf('%d', nOutliersNegativeDiff), 'Color', colours.lagging, 'HorizontalAlignment', 'left', 'FontSize', font_size-2, 'FontName', fontName, 'FontWeight', 'bold');
                end
                
                ax = gca;
                
                tmp1 = signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, isSampleUsed)';
                tmp2 = tableAllSamples.Tissue(isSampleUsed);
                pos = get(gca, 'Position'); pos(1) = pos(1) + pos(3)*0.5; pos(2) = pos(2) + pos(4)/3; pos(3) = pos(3)/2; pos(4) = pos(4)/2; axes('Position', pos); axis off;
                if (length(unique(tmp2))>1)
                    piePlotted = true;
                    tissues = [tissueNamesShort; 'other'];
                    tmpTable = table(tissues);
                    tmpTable.sumExposures = NaN*ones(nTissues+1,1);
                    tmpTable.nPatients = NaN*ones(nTissues+1,1);
                    for iTissue = 1:nTissues
                        tmpTable.sumExposures(iTissue) = sum(tmp1(strcmp(tmp2, tissueNames{iTissue}) & tmp1>0));
                        tmpTable.nPatients(iTissue) = sum(strcmp(tmp2, tissueNames{iTissue}));
                        %fprintf('%s: %.1f\n', tissueNames{iTissue}, tmpTable.sumExposures(iTissue));
                    end
                    tmpTable.sumExposuresPercentage = 100*tmpTable.sumExposures./nansum(tmpTable.sumExposures);
                    tmpTable.isTooLow = tmpTable.sumExposuresPercentage<2;
                    tmpTable.sumExposuresPercentage(end) = sum(tmpTable.sumExposuresPercentage(tmpTable.isTooLow));
                    tmpTable.sumExposuresPercentage(tmpTable.isTooLow) = 0;
                    
                    tmpTable.tissuesPrint = tmpTable.tissues;
                    tmpTable.tissuesPrint(tmpTable.sumExposuresPercentage<10) = {''};
                    if (nansum(tmpTable.nPatients)>0)
                        hPie = pie(tmpTable.sumExposuresPercentage(tmpTable.sumExposuresPercentage>0), tmpTable.tissuesPrint(tmpTable.sumExposuresPercentage>0));
                        hPiePatches = hPie(1:2:end); iSlice = 1;
                        for iTissue = find(tmpTable.sumExposuresPercentage(1:end-1)>0)'
                            hPiePatches(iSlice).FaceColor = cmapPerTissue(iTissue,:);
                            hPiePatches(iSlice).EdgeColor = cmapPerTissue(iTissue,:)*2-1;
                            iSlice = iSlice+1;
                        end
                    end
                end
                title(sprintf('n = %d', sum(isSampleUsedOriginal)));
                
            elseif (iType == 5)
                typeName_RT = 'double_type1_type2';
                yLabel_text = 'average exposure';
                if (iR == nR)
                    xLabel_text = 'replication timing';
                else
                    xLabel_text = '';
                end
                plot_replbar(signatureExposuresComplex_cell_RT_diff, iSignature, isSampleUsed, signatureResults, typeName_RT, nCurrentValues_RT, labelsExtremes, xLabel_text, yLabel_text, title_text, colours, font_size, fontName, legShiftHeightFactor)
                ax = gca; ax.YAxis.Exponent = exponentValue;
            end
            if (iR == 1)
                title(ax, {num2str(iType), ' '}, 'FontSize', font_size+4);
            end
        end
    end
    if (piePlotted)
        yB3 = 0.01; axes('Position', [xB, yB3, 1-2*xB, yB2-yB3]); hold on; tissueNamesLonger = [tissueNamesMiddle; {'other'}]; cmapPerTissueLonger = [cmapPerTissue; 0.8*[1,1,1]];
        charsPerTissue = zeros(nTissues+1,1);
        for iTissue = 1:nTissues+1
            charsPerTissue(iTissue) = max([6, length(tissueNamesLonger{iTissue})]);
        end
        for iTissue = 1:nTissues+1
            xVal = sum(charsPerTissue(1:iTissue-1))/sum(charsPerTissue);
            plot(xVal, 1, 's', 'MarkerFaceColor', cmapPerTissueLonger(iTissue,:), 'MarkerEdgeColor', cmapPerTissueLonger(iTissue,:)*2-1);
            text(xVal+0.005, 1, tissueNamesLonger{iTissue});
        end
        set(gca, 'XTick', [], 'YTick', [], 'XColor', 'none', 'YColor', 'none');
    end
    mySaveAs(fig, imagesPath, figName{iList}, false);
end