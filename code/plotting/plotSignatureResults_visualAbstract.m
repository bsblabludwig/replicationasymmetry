% function plotSignatureResults_visualAbstract(replData, territoriesSource, decomposedSignatures)


%%
% Here we will plot signature plots.
nSignatures = replData.nSignatures;
signatureNames = replData.signatureNames;
% signatureShortNames = strrep(strrep(signatureNames, 'Signature ', ''), 'Signature', 'S');
% signatureNames = strrep(signatureNames, 'Signature', 'Signature ');
tissueNames = unique(replData.tableCancerTypes.Tissue); nTissues = length(tissueNames);

structClusters = decomposedSignatures.structClusters;
finalSignatures = decomposedSignatures.finalSignatures;
finalSignatures.nSignatures = length(finalSignatures.signaturesNames);
types = decomposedSignatures.types;
subtypes = decomposedSignatures.subtypes;
strandSpecText = 'strandSpec';
%%
imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_signatures/']; createDir(imagesPath);
font_size = 10;
font_name = 'Helvetica';% 'Lucida Sans'; % Console
colours.leadingDark = [1,0,0];
colours.laggingDark = [0,0,1];
colours.leading = [1,.3,.3];
colours.lagging = [.3,.3,1];
colours.leadingNotSignif = (2+colours.leading)/3;
colours.laggingNotSignif = (2+colours.lagging)/3;
colours.titleSignificant = 0*[1,1,1];
colours.titleNotSignif = 0.7*[1,1,1];
colours.patternColormap = jet(6)*0.7;
colours.plus = [.75,.1,1];
colours.minus = [.1,.75,.5];
colours.tissuesColourmap = hsv(nTissues);
%%
currentListSignatures = 19;%[18, 19, 20];


nR_plus = 1;
%     nR = length(currentListSignatures); nC = 5; iS = 1; xS = 0.8; yS = 0.8; xB = 0.02; yB = 0.08*6/nR; xM = -0.03; yM = -0.01*6/nR;
nR = length(currentListSignatures); nC = 2; iS = 1; xS = 0.8; yS = 0.8; xB = 0.02; yB = 0.08*log2(6)/max([1, log2(nR)]); xM = -0.03; yM = -0.01;
fig = figure(1); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 42 (nR+nR_plus)*4.4],'units','normalized','outerposition',[0 0 1 1]);

plot_xLabel = false;
for iR = 1:nR
    iSignature = currentListSignatures(iR);
    title_text = ''; %signatureNames{iSignature};
%     if (iSignature == currentListSignatures(end))
        plot_xLabel = true;
%     end
    yLabel_text = signatureNames{iSignature};
    for iType = 1:nC
        myGeneralSubplot(nR,nC,iS,xS,yS,xB,yB,xM,yM); iS = iS + 1; hold on; set(gca, 'Color', 'none');
        
        if (iType == 1)
            signatureToPlot = [finalSignatures.splitSignaturesA(:,iSignature); finalSignatures.splitSignaturesB(:,iSignature)]; maxVal = 105*max(abs(signatureToPlot));
            plot_signature(signatureToPlot, subtypes.(strandSpecText), types.(strandSpecText), plot_xLabel, 'flipped', title_text, yLabel_text, colours, font_size+4, font_name, maxVal);
            ylim([-maxVal,maxVal]);
        else
            iCluster = finalSignatures.clusterIndex(iSignature);
            lstSignatureNames = unique(structClusters.strandSpec.allSignatureCTs(structClusters.strandSpec.finalClusters == iCluster & ~structClusters.strandSpec.allSignatureIsOfficial));
            %title_text = strrep(strjoin(lstSignatureNames), '_', '\_');
            signatureToPlot = structClusters.strandSpec.finalCentroidsSignatures(:,iCluster); maxVal = 100*max(abs(signatureToPlot));
            plot_signature(signatureToPlot, subtypes.(strandSpecText), types.(strandSpecText), plot_xLabel, 'normal', title_text, yLabel_text, colours, font_size+4, font_name);
            ylim([0, maxVal]); set(gca, 'YTick', []);
        end
        
    end
end
mySaveAs(fig, imagesPath, ['signaturesPlot_', strjoin(signatureNames(currentListSignatures)), '_', datestr(now, 'dd-mm-yyyy_HH-MM-SS')], true);

plot2svg('myfile.svg', fig) 
%%
