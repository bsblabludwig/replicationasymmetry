function plot_signature(signature, subtypes, types, plotTypes, strandSpecPlotType, title_text, yLabel_text, plotAllMutationTypes, font_size, font_name, maxYLimVal, textScaleFactor)
% font_name = font_name;

if (~exist('strandSpecPlotType', 'var'))
    strandSpecPlotType = 'normal';
end
if (~exist('maxYLimVal', 'var'))
    maxYLimVal = 20; %        maxYLimVal = max([20; abs(signatureValues)+2]);
end
if (~exist('textScaleFactor', 'var'))
    textScaleFactor = 1.2;
end

hold on;
cmap = [.2 0.7 1; 0.3 0.3 0.3; 1 .1 .1; 0.5 0.5 0.5; .4 .9 .3; 1 0.5 0.8];
if (length(signature) == 96)
    iColour = 1;
    for iBar = 1:16:96
        bar(iBar+(0:15), 100*signature(iBar+(0:15)), 'FaceColor', cmap(iColour,:), 'EdgeColor', 'none');
        for jBar = iBar+(0:15)
            text(jBar, 0, [subtypes{jBar}, ' '], 'Color', cmap(iColour,:), 'Rotation', 90, 'HorizontalAlignment', 'right', 'FontName', font_name);
        end
        iColour = iColour + 1;
    end
elseif (length(signature) == 2*96)
    if strcmp(strandSpecPlotType, 'flipped')
        leadingPart = 100*signature(1:96);
        laggingPart = -100*signature(97:end);
        signatureValues = laggingPart;
        signatureValues(signatureValues == 0) = leadingPart(signatureValues == 0);
        colourPerBar = cmap(sort(repmat(1:6, 1, 16)),:);
        
        yStar = [-23, 21]; %[-22, 20];
        isToPlotStar = false(96, 1);
        for iBar = 1:96
            bar(iBar, signatureValues(iBar), 'FaceColor', colourPerBar(iBar,:), 'EdgeColor', 'none');
            if (abs(signatureValues(iBar)) > maxYLimVal)
                if (plotAllMutationTypes)
                    isToPlotStar(iBar) = true;
                else
                    text(iBar, yStar(1+(signatureValues(iBar)>0)), '*', 'Color', colourPerBar(iBar,:), 'FontSize', 14, 'HorizontalAlignment', 'center', 'VerticalAlignment', 'middle');
                end
            end
        end
        %         iColour = 1;
        %         for iBar = 1:16:96
        %             bar(iBar+(0:15)-.2, signatureValues(iBar+(0:15)), 0.3, 'FaceColor', cmap(iColour,:), 'EdgeColor', 'none');
        %             for jBar = iBar+(0:15)
        %                 %                 if (signature(96+jBar) == 0)
        %                 %                     text(jBar, 0, [subtypes{jBar}, ' '], 'Color', cmap(iColour,:), 'Rotation', 90, 'HorizontalAlignment', 'right', 'FontName', font_name);
        %                 %                 else
        %                 %                     text(jBar, 0, [subtypes{jBar}, ' '], 'Color', cmap(iColour,:), 'Rotation', 90, 'HorizontalAlignment', 'left', 'FontName', font_name);
        %                 %                 end
        %             end
        %             iColour = iColour + 1;
        %     end
        set(gca, 'XColor', 'none');
        yLimVal = [-maxYLimVal, maxYLimVal]; ylim(yLimVal);
        plusYLim = maxYLimVal; minusYLim = -maxYLimVal;
        if (max(signatureValues(1:96/2)) > maxYLimVal)
            plusYLim = maxYLimVal*textScaleFactor;
        end
        if (min(signatureValues(1:96/2)) < -maxYLimVal)
            minusYLim = -maxYLimVal*textScaleFactor;
        end
        
        if (plotTypes)
            for iBar = 8:16:96 %listBars = 8:16:96;
                if (plotAllMutationTypes)
                    text(iBar, minusYLim, types{iBar}, 'Color', colourPerBar(iBar,:), 'HorizontalAlignment', 'center', 'VerticalAlignment', 'top', 'FontName', font_name, 'FontSize', font_size, 'FontWeight', 'bold');
                else
                    text(iBar, minusYLim, types{iBar}, 'Color', colourPerBar(iBar,:), 'Rotation', 45, 'HorizontalAlignment', 'right', 'VerticalAlignment', 'top', 'FontName', font_name, 'FontSize', font_size-2, 'FontWeight', 'bold');
                end
            end
        end
        if (plotAllMutationTypes)
            iColour = 1;
            signatureValues(signatureValues>maxYLimVal) = maxYLimVal;
            signatureValues(signatureValues<-maxYLimVal) = -maxYLimVal;
            for iBar = 1:16:96
                for jBar = iBar+(0:15)
                    if (signatureValues(jBar)~=0)
                        if (signatureValues(jBar)>0)
                            horizontalAlignmentValue = 'left';
                        else
                            horizontalAlignmentValue = 'right';
                        end
                        if (isToPlotStar(jBar))
                            if (signatureValues(jBar)>0)
                                labelText = [' ', subtypes{jBar}, ' {\bf*} '];
                            else
                                labelText = [' {\bf*} ', subtypes{jBar}, ' '];
                            end
                        else
                            labelText = [' ', subtypes{jBar}, ' '];
                        end
                        text(jBar, signatureValues(jBar), labelText, 'Color', cmap(iColour,:), 'Rotation', 90, 'HorizontalAlignment', horizontalAlignmentValue, 'FontSize', 6); % 'FontName', 'Monospaced', Segoe UI Monospaced
                    end
                end
                iColour = iColour + 1;
            end
            set(gca, 'YTick', [-maxYLimVal, 0, maxYLimVal], 'YTickLabel', [maxYLimVal, 0, maxYLimVal]);
        else
            set(gca, 'YTick', []);
        end
        %         text(2, maxYLimVal, 'main', 'Color', (1+colours.leading)/2, 'FontSize', font_size-4, 'FontName', font_name, 'VerticalAlignment', 'top');
        %         text(2, -maxYLimVal, 'side', 'Color', (1+colours.lagging)/2, 'FontSize', font_size-4, 'FontName', font_name, 'VerticalAlignment', 'bottom');
        %         text(2, maxYLimVal, 'main', 'Color', colours.leading, 'FontSize', font_size-4, 'FontName', font_name, 'VerticalAlignment', 'top');
        %         text(2, -maxYLimVal, 'side', 'Color', colours.lagging, 'FontSize', font_size-4, 'FontName', font_name, 'VerticalAlignment', 'bottom');
        text(2, plusYLim, 'leading template', 'Color', 0.5*[1,1,1], 'FontSize', font_size-4, 'FontName', font_name, 'VerticalAlignment', 'top');
        text(2, minusYLim, 'lagging template', 'Color', 0.5*[1,1,1], 'FontSize', font_size-4, 'FontName', font_name, 'VerticalAlignment', 'bottom');
    else
        iColour = 1;
        for iBar = 1:16:96
            bar(iBar+(0:15)-.2, 100*signature(iBar+(0:15)), 0.3, 'FaceColor', cmap(iColour,:), 'EdgeColor', 'none');
            bar(iBar+(0:15)+.2, 100*signature(96+iBar+(0:15)), 0.3, 'FaceColor', (cmap(iColour,:))/2, 'EdgeColor', 'none');
            for jBar = iBar+(0:15)
                text(jBar, 0, [subtypes{jBar}, ' '], 'Color', cmap(iColour,:), 'Rotation', 90, 'HorizontalAlignment', 'right', 'FontName', font_name, 'FontSize', 8); % 'Monospaced' Segoe UI Monospaced
            end
            iColour = iColour + 1;
        end
    end
end

set(gca, 'XTick', []);

if (~strcmp(strandSpecPlotType, 'flipped') && plotTypes)
    iType = 1;
    for iBar = 8:16:96
        text(iBar, 0, {' ', ' ', ' ', ' ', ' ', types{iBar}}, 'Color', cmap(iType,:), 'HorizontalAlignment', 'center', 'FontName', font_name, 'FontSize', font_size, 'FontWeight', 'bold');
        iType = iType + 1;
    end
end

set(gca, 'FontName', font_name);

if (~isempty(title_text))
    title(title_text, 'FontSize', font_size, 'FontName', font_name);
end
if (~isempty(yLabel_text))
    ylabel(yLabel_text, 'FontSize', font_size, 'FontName', font_name, 'FontWeight', 'bold');
end

% yLimVal = get(gca, 'YLim');
% if (~exist('maxYLimVal', 'var'))
%     maxYLimVal = max([20, abs(yLimVal)]);
% end
% yLimVal = [-maxYLimVal, maxYLimVal]; ylim(yLimVal);
% % yLimVal(1) = min([yLimVal(1), -20]); yLimVal(2) = max([yLimVal(2), 20]); ylim(yLimVal);
drawnow;