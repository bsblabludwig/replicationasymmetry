function plotSignature(signature, subtypes, types, plotTypes)

hold on;
cmap = [.2 0.7 1; 0.3 0.3 0.3; 1 .1 .1; 0.5 0.5 0.5; .4 .9 .3; 1 0.5 0.8];
if (length(signature) == 96)
    iColour = 1;
    for iBar = 1:16:96
        bar(iBar+(0:15), 100*signature(iBar+(0:15)), 'FaceColor', cmap(iColour,:), 'EdgeColor', 'none');
        for jBar = iBar+(0:15)
            text(jBar, 0, [subtypes{jBar}, ' '], 'Color', cmap(iColour,:), 'Rotation', 90, 'HorizontalAlignment', 'right', 'FontName', 'Lucida Console');
        end
        iColour = iColour + 1;
    end
elseif (length(signature) == 2*96)
    iColour = 1;
    for iBar = 1:16:96
        bar(iBar+(0:15)-.2, 100*signature(iBar+(0:15)), 0.3, 'FaceColor', cmap(iColour,:), 'EdgeColor', 'none');
        bar(iBar+(0:15)+.2, 100*signature(96+iBar+(0:15)), 0.3, 'FaceColor', (cmap(iColour,:))/2, 'EdgeColor', 'none');
        for jBar = iBar+(0:15)
            text(jBar, 0, [subtypes{jBar}, ' '], 'Color', cmap(iColour,:), 'Rotation', 90, 'HorizontalAlignment', 'right', 'FontName', 'Lucida Console');
        end
        iColour = iColour + 1;
    end
end

set(gca, 'XTick', []);
if (plotTypes)
    iType = 1;
    for iBar = 8:16:96
        text(iBar, 0, {' ', ' ', ' ', ' ', ' ', types{iBar}}, 'Color', cmap(iType,:), 'HorizontalAlignment', 'center', 'FontName', 'Lucida Console', 'FontSize', 12, 'FontWeight', 'bold');
        iType = iType + 1;
    end
end
set(gca, 'FontName', 'Lucida Console');