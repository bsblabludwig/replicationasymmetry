function patternString = getPatternStringFromNumber(number)

[nuclLeft, nuclRight, nuclFrom, nuclTo] = getPatternFromNumber(number);
patternString = [nuclLeft, nuclFrom, nuclRight, 'to', nuclTo];