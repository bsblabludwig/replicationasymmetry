function fig = createMaximisedFigure(figNumber)
% createMaximisedFigure(figNumber)

fig = figure(figNumber); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 30 20],'units','normalized','outerposition',[0 0 1 1]);
% set(gcf, 'Position', get(0,'Screensize'));