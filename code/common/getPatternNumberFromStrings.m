function [patternNumber, nuclLeftNew, nuclRightNew] = getPatternNumberFromStrings(contextString, nuclFromChar, nuclToChar)

bases = {'A', 'C', 'G', 'T'};
nuclLeft = find(strcmp(bases, contextString(1)));
nuclRight = find(strcmp(bases, contextString(end)));
nuclFrom = find(strcmp(bases, nuclFromChar));
nuclTo = find(strcmp(bases, nuclToChar));

if (isempty(nuclLeft) || isempty(nuclRight) || isempty(nuclFrom) || isempty(nuclTo))
    fprintf('WARNING: unsupported base within %s>%s.\n', contextString, nuclToChar);
    patternNumber = -1;
    nuclLeftNew = -1;
    nuclRightNew = -1;
else
    [patternNumber, nuclLeftNew, nuclRightNew] = getPatternNumber(nuclLeft, nuclRight, nuclFrom, nuclTo);
end