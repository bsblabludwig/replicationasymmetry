function result = num2sepNumStr(inputNumber)

result = sprintf(',%c%c%c',fliplr(num2str(inputNumber)));
result = fliplr(result(2:end));