function evaluationOfDecomposedSignatures = loadEvaluationOfDecomposedSignatures(alwaysCompute, savePathBasic, tissueNames, nMaxSignatures, nRuns, minStability, territoriesSource)
% This function finds the best K number of mutational signatures for each cancer type.
% It uses the following data:
% - data/signatures/hg19.chrom.sizes.backgroundTableTriNucl96.txt
% - data/signatures/backgroundTrinuclFrequency_isLeftOrRightNotTx.txt
% - data/signatures/signaturesOfficial30.txt
% - files in the following name format: 
%     data/Haradhvala_territories/evaluationOfDecomposedSignatures/res_blood_lymphoid_274_2k_strandSpec_100runs.reconstructionError.txt 
%     data/Haradhvala_territories/res_blood_lymphoid_274_2k_strandSpec_100runs.stability.txt
% 
% Testing:
% clear
% tissueNames = {'POLEmut_14', 'blood_myeloid_56', 'blood_lymphoid_53', 'colon_14', 'breast_119', 'oesophagus_135', 'pancreas_421', 'kidney_95', 'skin_66', 'liver_88', 'lung_24', 'brain_MDB_100', 'brain_PA_101', 'gastric_100'}; %, 'colon_MSI_9'
% imagesPath = ['images/',datestr(now, 'mmdd'),'_evaluationOfDecomposedSignatures/']; createDir(imagesPath);
% nMaxSignatures = 7; %15
% nRuns = 100;
% minStability = 0.8;
% alwaysCompute = true;
% savePathBasic = 'save/';


pathAndName = [savePathBasic, territoriesSource, '/evaluationOfDecomposedSignatures_', territoriesSource];

%%
if (~alwaysCompute && exist([pathAndName, '.mat'],'file'))
    load(pathAndName, 'evaluationOfDecomposedSignatures');
else
    tic;
    plotFigures = false;
    nTissues = length(tissueNames);
    strandSpecTexts = {'strandUnspec', 'strandSpec'};
    tableGenomeTrinucleotides = readtable('data/signatures/hg19.chrom.sizes.backgroundTableTriNucl96.txt', 'delimiter', '\t', 'ReadVariableNames', true);
    backgroundTrinuclFrequency = readtable('data/signatures/backgroundTrinuclFrequency_isLeftOrRightNotTx.txt'); %backgroundTrinuclFrequency
    tableSignaturesOfficial = readtable('data/signatures/signaturesOfficial30.txt', 'delimiter', '\t', 'ReadVariableNames', true);
    matrixSignaturesOfficial = table2array(tableSignaturesOfficial(:,4:(4+29))); signaturesOfficialNames = tableSignaturesOfficial.Properties.VariableNames(4:(4+29));
    evaluationOfDecomposedSignatures.normalisingVector.strandUnspec = tableGenomeTrinucleotides.genomeComputed./backgroundTrinuclFrequency.leadingPlusLagging;
    evaluationOfDecomposedSignatures.normalisingVector.strandSpec = [tableGenomeTrinucleotides.genomeComputed; tableGenomeTrinucleotides.genomeComputed]./[backgroundTrinuclFrequency.leading; backgroundTrinuclFrequency.lagging];
    evaluationOfDecomposedSignatures.signaturesOfficialNames = signaturesOfficialNames;
    evaluationOfDecomposedSignatures.matrixSignaturesOfficial = matrixSignaturesOfficial;
    evaluationOfDecomposedSignatures.nPatterns.strandUnspec = 96;
    evaluationOfDecomposedSignatures.nPatterns.strandSpec = 2*96;
    evaluationOfDecomposedSignatures.nRuns = nRuns;
    evaluationOfDecomposedSignatures.strandSpecTexts = strandSpecTexts;
    imagesPath = ['images/',territoriesSource,'/',datestr(now, 'mmdd'),'_decomposedSignatures/']; createDir(imagesPath);
    %%
    for iStrandSpec = 1:2
        strandSpecText = strandSpecTexts{iStrandSpec};
        evaluationOfDecomposedSignatures.stability.(strandSpecText) = NaN*ones(nTissues, nMaxSignatures);
        evaluationOfDecomposedSignatures.reconstructionError.(strandSpecText) = NaN*ones(nTissues, nMaxSignatures);
        evaluationOfDecomposedSignatures.minErrorNSignatures.(strandSpecText) = NaN*ones(nTissues, 1);
        evaluationOfDecomposedSignatures.bestNSignatures.(strandSpecText) = NaN*ones(nTissues, 1);
        for iTissue = 1:nTissues
            tissueName = tissueNames{iTissue};
            for nSignatures = 2:nMaxSignatures
                fileNamePrefix = ['data/', territoriesSource, '/evaluationOfDecomposedSignatures/res_', tissueName, '_', num2str(nSignatures), 'k_', strandSpecText, '_', num2str(nRuns), 'runs'];
                try
                    evaluationOfDecomposedSignatures.stability.(strandSpecText)(iTissue, nSignatures) = dlmread([fileNamePrefix, '.stability.txt']);
                    evaluationOfDecomposedSignatures.reconstructionError.(strandSpecText)(iTissue, nSignatures) = dlmread([fileNamePrefix, '.reconstructionError.txt']);
                catch
                    fprintf('WARNING: problem with file %s.\n', fileNamePrefix);
                end
            end
            [~, iMin] = nanmin(evaluationOfDecomposedSignatures.reconstructionError.(strandSpecText)(iTissue, :));
            evaluationOfDecomposedSignatures.minErrorNSignatures.(strandSpecText)(iTissue) = iMin;
            meanEvaluation = evaluationOfDecomposedSignatures.reconstructionError.(strandSpecText)(iTissue, :)/max(evaluationOfDecomposedSignatures.reconstructionError.(strandSpecText)(iTissue, :)) + (1-evaluationOfDecomposedSignatures.stability.(strandSpecText)(iTissue, :));
            meanEvaluation(evaluationOfDecomposedSignatures.stability.(strandSpecText)(iTissue, :) < minStability) = max(meanEvaluation);
            [~, iMin] = nanmin(meanEvaluation);
            evaluationOfDecomposedSignatures.bestNSignatures.(strandSpecText)(iTissue) = iMin;
        end
    end
    %%
    if (plotFigures)
        for iStrandSpec = 1:2
            strandSpecText = strandSpecTexts{iStrandSpec};
            fig = createMaximisedFigure(iStrandSpec);
            nR = 4; nC = ceil(nTissues/nR);
            for iTissue = 1:nTissues
                tissueName = tissueNames{iTissue};
                subplot(nR,nC,iTissue);
                yyaxis left
                yValues = evaluationOfDecomposedSignatures.stability.(strandSpecText)(iTissue, 2:nMaxSignatures);
                plot(2:nMaxSignatures, yValues, '.-');
                if (mod(iTissue-1, nC) == 0) ylabel('Stability'); end
                yStep = (max(yValues)-min(yValues))/10; ylim([min(yValues)-yStep, max(yValues)+yStep]);
                
                yyaxis right
                yValues = evaluationOfDecomposedSignatures.reconstructionError.(strandSpecText)(iTissue, 2:nMaxSignatures);
                plot(2:nMaxSignatures, yValues, '.-'); hold on;
                yStep = (max(yValues)-min(yValues))/10; ylim([min(yValues)-yStep, max(yValues)+yStep]);
                xlim([1.8,nMaxSignatures+0.2]); yLimVal = get(gca, 'YLim');
                if (mod(iTissue-1, nC) == nC-1) ylabel('Reconstruction error'); end
                iMin = evaluationOfDecomposedSignatures.minErrorNSignatures.(strandSpecText)(iTissue);
                plot(iMin, evaluationOfDecomposedSignatures.reconstructionError.(strandSpecText)(iTissue, iMin), 'o');
                iMin = evaluationOfDecomposedSignatures.bestNSignatures.(strandSpecText)(iTissue);
                plot(iMin*[1,1], yLimVal, '-', 'LineWidth', 2);
                
                title(tissueName, 'interpret', 'none');
                if (iTissue > (nR-1)*nC) xlabel('Number of signatures'); end
            end
            suptitle(sprintf('Best number of signatures in each cancer type for %s', strandSpecText));
            mySaveAs(fig, imagesPath, ['nSignaturesB_', strandSpecText]);
        end
    end
    % SAVE
    save(pathAndName, 'evaluationOfDecomposedSignatures');
    toc
end
%% Prepareation of signatures:

% tableSignaturesOfficialOld = readtable('data/signatures/signaturesOfficial.txt', 'delimiter', '\t', 'ReadVariableNames', true);
% tableSignaturesOfficialNew = readtable('data/signatures/signatures_probabilities.txt', 'delimiter', '\t', 'ReadVariableNames', true);
% permNewToOld = NaN*ones(96, 1);
% for iPattern = 1:96
%     permNewToOld(iPattern) = find(strcmp(tableSignaturesOfficialNew.SomaticMutationType, tableSignaturesOfficialOld.SomaticMutationType{iPattern}));
% end
% tableSignaturesOfficialNewCorrectOrder = tableSignaturesOfficialNew(permNewToOld, 1:33);
% writetable(tableSignaturesOfficialNewCorrectOrder, 'data/signatures/signaturesOfficial30.txt', 'delimiter', '\t', 'WriteVariableNames', true);

% matrixSignaturesOfficialNormalised = matrixSignaturesOfficial;
% for iSignature = 1:nSignaturesOfficial
%     tmpSignatureNormalised = matrixSignaturesOfficial(:,iSignature);
%     tmpSignatureNormalised = tmpSignatureNormalised./tableGenomeTrinucleotides.genomeComputed;
%     matrixSignaturesOfficialNormalised(:,iSignature) = tmpSignatureNormalised/sum(tmpSignatureNormalised);
% end
