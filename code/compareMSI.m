% function compareMSI(replData)
fontName = 'Trebuchet MS';
set(0,'defaultAxesFontName',fontName);
set(0,'defaultTextFontName',fontName);
imagesPath = ['images/',datestr(now, 'mmdd'),'_compareMSI/']; createDir(imagesPath);

colours.MSS = [0.1250    0.5781    0.5430];
colours.MSI = [1    0.6    0.2];

%
tableAllSamples = replData.tableAllSamples;
signatureExposuresSimple_LeadLagg = replData.signatureExposuresSimple.LeadLagg;
signatureExposuresComplex_cell = replData.signatureExposuresComplex.ReplicationTiming_LeadLagg;
nCurrentValues_RT = replData.dataFields.nValues.ReplicationTiming_LeadLagg;

clear lstSamples.MSI
lstCTs.MSI = [35]; % 31={'TCGA_MSI_Strelka'}
lstCTs.MSS = [36,12];%,12];%,4,12]; % 10 (COCA-CA and pancrease are not clear enough)
lstSamples.MSI = {'DO219117'}; %{'DO219117', 'DO227620'};
isSample.MSI = ismember(tableAllSamples.iCancerType, lstCTs.MSI)' | ismember(tableAllSamples.sampleName, lstSamples.MSI)'; sum(isSample.MSI)
isSample.MSS = ismember(tableAllSamples.iCancerType, lstCTs.MSS)' & ~ismember(tableAllSamples.sampleName, lstSamples.MSI)'; sum(isSample.MSS)


iSignature = 16; %replData.signatureNames{iSignature}
minMeanExposure = 10;

typesMS = {'MSS', 'MSI'}; clear exposures; clear isSampleUsed;
for iTypeMS = 1:2
    typeMS = typesMS{iTypeMS};
    exposures.together.(typeMS) = signatureExposuresSimple_LeadLagg.type1Minustype2(iSignature, isSample.(typeMS));
    %     exposures.together.(typeMS) = signatureExposuresSimple_LeadLagg.total(iSignature, isSample.(typeMS))./sum(signatureExposuresSimple_LeadLagg.total(:, isSample.(typeMS)));
    %     exposures.together.(typeMS) = (replData.structAllSamples.LeadLagg.mfTotal(isSample.(typeMS), 88)./sum(replData.structAllSamples.LeadLagg.mfTotal(isSample.(typeMS), 81:96), 2))';
    
    selectedTypes = {'all', 'above10'};
    for iSelectedType = 1:2
        selectedType = selectedTypes{iSelectedType};
        if (iSelectedType == 1)
            isSampleUsed.(selectedType).(typeMS) = isSample.(typeMS);
        else
            isSampleUsed.(selectedType).(typeMS) = isSample.(typeMS) & signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure;
        end
        exposures.(selectedType).matching.(typeMS) = NaN*ones(sum(isSampleUsed.(selectedType).(typeMS)), nCurrentValues_RT);
        exposures.(selectedType).inverse.(typeMS) = NaN*ones(sum(isSampleUsed.(selectedType).(typeMS)), nCurrentValues_RT);
        exposures.(selectedType).total.(typeMS) = NaN*ones(sum(isSampleUsed.(selectedType).(typeMS)), nCurrentValues_RT);
        for iValue = 1:nCurrentValues_RT
            exposures.(selectedType).matching.(typeMS)(:,iValue) = signatureExposuresComplex_cell{iValue}.type1(iSignature, isSampleUsed.(selectedType).(typeMS))';
            exposures.(selectedType).inverse.(typeMS)(:,iValue) = signatureExposuresComplex_cell{iValue}.type2(iSignature, isSampleUsed.(selectedType).(typeMS))';
            exposures.(selectedType).total.(typeMS)(:,iValue) = signatureExposuresComplex_cell{iValue}.totalMF(iSignature, isSampleUsed.(selectedType).(typeMS))';
        end
    end
end
%%
fig = figure(2); clf(fig);  set(gcf,'PaperUnits','centimeters','PaperPosition',[0 0 35 15],'units','normalized','outerposition',[0 0 1 1]);
colormap([colours.MSI; colours.MSS]); fontSize = 14;
nR=2; nC=2; iS=1; xS=.8; yS=.75; xB=.06; yB=.06; xM=-.1; yM=-.04;
posTitle.xShift = 0.04;
posTitle.yShift = 0.01;
posTitle.width = 0.02;
posTitle.height = 0.02;
posTitle.fontSize = 18;
posTitle.letters = {'A', 'B', 'C', 'D'}; jS = 1;

positionVector = myGeneralSubplot(nR,nC,3,xS,1+yS,xB,yB,xM,yM); iS = iS + 1; hold on; pos.x = positionVector(1); pos.y = positionVector(2); pos.width = positionVector(3); pos.height = positionVector(4);
myBoxplot(1, exposures.together.MSI, .3, colours.MSI, colours.MSI/2);
myBoxplot(2, exposures.together.MSS, .3, colours.MSS, colours.MSS/2);
% h = boxplot([exposures.together.MSI, exposures.together.MSS], [0*exposures.together.MSI, 1+0*exposures.together.MSS], 'Color', 'k', 'Symbol', ''); set(h,'LineWidth',3);

handles = plotSpread({exposures.together.MSI, exposures.together.MSS}, 'xNames', {'MSI', 'MSS'}, 'distributionMarkers', {'o', 'o'});
set(handles{1}(1), 'MarkerFaceColor', colours.MSI, 'MarkerEdgeColor', colours.MSI/2, 'LineWidth', 1, 'MarkerSize', 5); %[.5,.5,.8]
set(handles{1}(2), 'MarkerFaceColor', colours.MSS, 'MarkerEdgeColor', colours.MSS/2, 'LineWidth', 1, 'MarkerSize', 4);

h = boxplot([exposures.together.MSI, exposures.together.MSS], [0*exposures.together.MSI, 1+0*exposures.together.MSS], 'Color', 'k', 'Symbol', ''); set(h,'LineWidth',3);

yLimVal = get(gca, 'YLim'); yLimVal(1) = 0; ylim(yLimVal);
% plot([1.2,1.8], 1*yLimVal(2)*[1,1], '-k', 'LineWidth', 4);
% plot(1.21*[1,1], [0.98,1]*yLimVal(2), '-k', 'LineWidth', 4);
% plot(1.79*[1,1], [0.98,1]*yLimVal(2), '-k', 'LineWidth', 4);
text(1.5, .8*yLimVal(2), getPValueAsText(ranksum(exposures.together.MSI, exposures.together.MSS)), 'HorizontalAlignment', 'center', 'VerticalAlignment', 'bottom', 'FontSize', fontSize);
ylabel({'Exposure to signature 17', ' '});
set(gca, 'FontSize', fontSize, 'XTickLabel', {'MSI', 'MSS'});
xlim([0.5,2.5]); title('All samples');
axes('Position', [pos.x - posTitle.xShift, pos.y + pos.height + posTitle.yShift, posTitle.width, posTitle.height]); axis off; drawnow;
text(.5,.5,posTitle.letters{jS},'FontWeight', 'bold', 'FontSize', posTitle.fontSize+2, 'HorizontalAlignment', 'center'); jS = jS + 1;

for iSelectedType = 1:2
    selectedType = selectedTypes{iSelectedType};
    positionVector = myGeneralSubplot(nR,nC,iS,xS,yS,xB,yB,xM,yM); iS = iS + 2; hold on; pos.x = positionVector(1); pos.y = positionVector(2); pos.width = positionVector(3); pos.height = positionVector(4);
    bar([nanmean(exposures.(selectedType).total.MSI); nanmean(exposures.(selectedType).total.MSS)]', 'EdgeColor', 'none'); hold on;
    
    x1=0.14;
    width = 0.1;
    edgeColour = 0*[1,1,1];
    for iValue = 1:nCurrentValues_RT
        myStderrorBar(iValue-x1, exposures.(selectedType).total.MSI(:,iValue), width, colours.MSI/2);
        myStderrorBar(iValue+x1, exposures.(selectedType).total.MSS(:,iValue), width, colours.MSS/2);
        pValue = ranksum(exposures.(selectedType).total.MSI(:,iValue), exposures.(selectedType).total.MSS(:,iValue));
        tmp = exposures.(selectedType).total.MSS(:,iValue); maxVal = mean([tmp])+std(tmp)/length(tmp) + 1.5e3;
        text(iValue, maxVal, getPValueAsText(pValue), 'HorizontalAlignment', 'center', 'FontSize', fontSize-4);
    end
    yLimVal = get(gca, 'YLim'); yLimVal(2)=yLimVal(2)*1.05; yLimVal(1) = 0; ylim(yLimVal);
    ylabel('Signature 17 exposure', 'FontSize', fontSize);
    set(gca, 'FontSize', fontSize, 'XTick', [1,nCurrentValues_RT], 'XTickLabel', {'early', 'late'});
    legend({'MSI', 'MSS'}, 'FontSize', fontSize, 'Location', 'NorthWest'); legend boxoff;
    if (iSelectedType == 1)
        %         text(2.5, yLimVal(2), 'All samples', 'HorizontalAlignment', 'center', 'FontSize', fontSize);
        title('All samples');
    else
        %         text(2.5, yLimVal(2), 'Signature 17 exposure > 10', 'HorizontalAlignment', 'center', 'FontSize', fontSize);
        title('Signature 17 exposure > 10');
    end
    axes('Position', [pos.x - posTitle.xShift, pos.y + pos.height + posTitle.yShift, posTitle.width, posTitle.height]); axis off; drawnow;
    text(.5,.5,posTitle.letters{jS},'FontWeight', 'bold', 'FontSize', posTitle.fontSize+2, 'HorizontalAlignment', 'center'); jS = jS + 1;
end
mySaveAs(fig, imagesPath, 'MSI_comparison');
%%
% fig = createMaximisedFigure(1);
% subplot(1,2,1); plot(exposures.total.MSI');
% subplot(1,2,2); plot(exposures.total.MSS');
% subplot(1,2,1); bar(nanmean(exposures.total.MSI));
% subplot(1,2,2); bar(nanmean(exposures.total.MSS));
%%
% tmp = tableAllSamples(isSampleUsed.MSS,:);
% %% 10 COCA-CN, 22 PACA-AU (several could be, but are not), 23 PACA-CA (one could be, but is not), 24 PAEN-AU (one), 25 PAEN-IT (none)
% % isSampleUsed.ESAD = ismember(tableAllSamples.iCancerType, 12)' & signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure;
% isSampleUsedTmp = ismember(tableAllSamples.iCancerType, 23)' & signatureExposuresSimple_LeadLagg.meanType1Type2(6, :)>1000;    %signatureExposuresSimple_LeadLagg.meanType1Type2(iSignature, :)>minMeanExposure &
% sum(isSampleUsedTmp)
% fig = createMaximisedFigure(1);
% imagesc(signatureExposuresSimple_LeadLagg.meanType1Type2(:,isSampleUsedTmp)); colorbar;
% set(gca, 'YTick', 1:replData.nSignatures, 'YTickLabel', replData.signatureNames, 'XTick', 1:sum(isSampleUsedTmp), 'XTickLabel', tableAllSamples.sampleName(isSampleUsedTmp));
% %%
% % ESAD-UK: {'DO219117'}; The other one is unclear: 'DO227620'
% % PACA-AU: {'DO33091'}
% isTmp = ismember(replData.tableAllSamples.sampleName, {'DO219117', 'DO227620'}); sum(isTmp) % , 'DO49079', 'DO49172', 'DO49195', 'DO33105', 'DO46688', 'DO49178' {'DO219117', 'DO227620', 'DO227511', 'DO227679'} %isSampleUsed.ESAD %, 'DO50450', 'DO219111' % , 'DO50326', 'DO50332', 'DO50362', 'DO50388', 'DO10840', 'DO227466', 'DO227523', 'DO227672', 'DO50309', 'DO50323', 'DO50374', 'DO50440', 'DO227814'
% tmpNames = replData.tableAllSamples(isTmp,:);
%
% fig = createMaximisedFigure(1);
% imagesc(signatureExposuresSimple_LeadLagg.meanType1Type2(:,isTmp)); colorbar;
% set(gca, 'YTick', 1:replData.nSignatures, 'YTickLabel', replData.signatureNames, 'XTick', 1:sum(isTmp), 'XTickLabel', tmpNames.sampleName);
%
% tmp = signatureExposuresSimple_LeadLagg.meanType1Type2([6,14,19,20,24],isTmp);
% tmp2 = max(tmp); %fig = createMaximisedFigure(2); histogram(tmp2);
% tmpNames.sampleName(tmp2>2500)
% tmp(:,tmp2>2500)
%%
% cd ../mutationFrequency/data/Mutations/ICGC
% grep deletion simple_somatic_mutation.open.ESAD-UK.tsv | sort -k1,1 -k2,2 -k4,4 -u > deletions.simple_somatic_mutation.open.ESAD-UK.tsv
% cut -f 2,4 deletions.simple_somatic_mutation.open.ESAD-UK.tsv | sort | uniq -c > nDeletions.simple_somatic_mutation.open.ESAD-UK.tsv
% grep insertion simple_somatic_mutation.open.ESAD-UK.tsv | sort -k1,1 -k2,2 -k4,4 -u > insertions.simple_somatic_mutation.open.ESAD-UK.tsv
% cut -f 2,4 insertions.simple_somatic_mutation.open.ESAD-UK.tsv | sort | uniq -c > nInsertions.simple_somatic_mutation.open.ESAD-UK.tsv
% tail -n +2 ncomms15180-s15.txt | cut -f 1-3 | sort -k1,1 -k2,2n > locations_MSI_WGS_CortesCiriano2017.bed
% tail -n +2 ncomms15180-s14.txt | cut -f 1-3 | sort -k1,1 -k2,2n > locations_MSI_WES_CortesCiriano2017.bed
% cat locations_MSI_WGS_CortesCiriano2017.bed locations_MSI_WES_CortesCiriano2017.bed | sort -k1,1 -k2,2n > locations_MSI_all_CortesCiriano2017.bed

% icgc_mutation_id
% icgc_donor_id
% project_code
% icgc_specimen_id
% icgc_sample_id
% matched_icgc_sample_id
% submitted_sample_id
% submitted_matched_sample_id
% chromosome
% chromosome_start
% chromosome_end
% chromosome_strand
% assembly_version	mutation_type	reference_genome_allele	mutated_from_allele	mutated_to_allele	quality_score	probability	total_read_count	mutant_allele_read_count	verification_status	verification_platform	biological_validation_status	biological_validation_platform	consequence_type	aa_mutation	cds_mutation	gene_affected	transcript_affected	gene_build_version	platform	experimental_protocol	sequencing_strategy	base_calling_algorithm	alignment_algorithm	variation_calling_algorithm	other_analysis_algorithm	seq_coverage	raw_data_repository	raw_data_accession	initial_data_release_date

% awk '{chromosome=$9; chromosome_start=$10; chromosome_end=$11; icgc_donor_id=$2; icgc_specimen_id=$4; printf "chr%s\t%d\t%d\t%s\t%s\n", chromosome, chromosome_start, chromosome_end, icgc_donor_id, icgc_specimen_id}' deletions.simple_somatic_mutation.open.ESAD-UK.tsv | sort -k1,1 -k2,2n > deletions.ESAD-UK.bed
% bedtools intersect -a deletions.ESAD-UK.bed -b locations_MSI_all_CortesCiriano2017.bed -sorted > deletions.inLocationsMSI.ESAD-UK.bed
% cut -f 4,5 deletions.inLocationsMSI.ESAD-UK.bed | sort | uniq -c > nDeletions.inLocationsMSI.simple_somatic_mutation.open.ESAD-UK.tsv
%      15 DO219117	SP119779
%       6 DO227620	SP192313
%       1 DO227814	SP192578
%       1 DO50326	SP111001
%       1 DO50332	SP111006
%       1 DO50362	SP111101
%       1 DO50388	SP111038
%
% awk '{chromosome=$9; chromosome_start=$10; chromosome_end=$11; icgc_donor_id=$2; icgc_specimen_id=$4; printf "chr%s\t%d\t%d\t%s\t%s\n", chromosome, chromosome_start, chromosome_end, icgc_donor_id, icgc_specimen_id}' insertions.simple_somatic_mutation.open.ESAD-UK.tsv | sort -k1,1 -k2,2n > insertions.ESAD-UK.bed
% bedtools intersect -a insertions.ESAD-UK.bed -b locations_MSI_all_CortesCiriano2017.bed -sorted > insertions.inLocationsMSI.ESAD-UK.bed
% cut -f 4,5 insertions.inLocationsMSI.ESAD-UK.bed | sort | uniq -c > nInsertions.inLocationsMSI.simple_somatic_mutation.open.ESAD-UK.tsv
%       1 DO10840	SP23498
%       3 DO219117	SP119779
%       1 DO227466	SP192162
%       1 DO227523	SP192604
%       1 DO227672	SP192260
%       1 DO227814	SP192578
%       1 DO50309	SP110846
%       1 DO50323	SP110865
%       1 DO50374	SP111031
%       1 DO50440	SP111017

% length(unique({'DO219117', 'DO227620', 'DO227814', 'DO50326', 'DO50332', 'DO50362', 'DO50388', 'DO10840', 'DO227466', 'DO227523', 'DO227672', 'DO50309', 'DO50323', 'DO50374', 'DO50440'}))
%%
% CT=PAEN-IT
% grep deletion ../other/simple_somatic_mutation.open.${CT}.tsv | sort -k1,1 -k2,2 -k4,4 -u > deletions.simple_somatic_mutation.open.${CT}.tsv
% cut -f 2,4 deletions.simple_somatic_mutation.open.${CT}.tsv | sort | uniq -c > nDeletions.simple_somatic_mutation.open.${CT}.tsv
% grep insertion ../other/simple_somatic_mutation.open.${CT}.tsv | sort -k1,1 -k2,2 -k4,4 -u > insertions.simple_somatic_mutation.open.${CT}.tsv
% cut -f 2,4 insertions.simple_somatic_mutation.open.${CT}.tsv | sort | uniq -c > nInsertions.simple_somatic_mutation.open.${CT}.tsv
%
% awk '{chromosome=$9; chromosome_start=$10; chromosome_end=$11; icgc_donor_id=$2; icgc_specimen_id=$4; printf "chr%s\t%d\t%d\t%s\t%s\n", chromosome, chromosome_start, chromosome_end, icgc_donor_id, icgc_specimen_id}' deletions.simple_somatic_mutation.open.${CT}.tsv | sort -k1,1 -k2,2n > deletions.${CT}.bed
% bedtools intersect -a deletions.${CT}.bed -b locations_MSI_all_CortesCiriano2017.bed -sorted > deletions.inLocationsMSI.${CT}.bed
% cut -f 4,5 deletions.inLocationsMSI.${CT}.bed | sort | uniq -c > nDeletions.inLocationsMSI.simple_somatic_mutation.open.${CT}.tsv
%
%
% awk '{chromosome=$9; chromosome_start=$10; chromosome_end=$11; icgc_donor_id=$2; icgc_specimen_id=$4; printf "chr%s\t%d\t%d\t%s\t%s\n", chromosome, chromosome_start, chromosome_end, icgc_donor_id, icgc_specimen_id}' insertions.simple_somatic_mutation.open.${CT}.tsv | sort -k1,1 -k2,2n > insertions.${CT}.bed
% bedtools intersect -a insertions.${CT}.bed -b locations_MSI_all_CortesCiriano2017.bed -sorted > insertions.inLocationsMSI.${CT}.bed
% cut -f 4,5 insertions.inLocationsMSI.${CT}.bed | sort | uniq -c > nInsertions.inLocationsMSI.simple_somatic_mutation.open.${CT}.tsv
