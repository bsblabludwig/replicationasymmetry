% function validationPerturbations(replData)

imagesPath = ['images/',datestr(now, 'mmdd'),'_validationPerturbations/']; createDir(imagesPath);
%%
structAllSamples = replData.structAllSamples;
dataFields = replData.dataFields;
nTotalSamples = size(replData.tableAllSamples, 1);
nSignatures = replData.nSignatures;

tableGenomeTrinucleotides = readtable('data/signatures/hg19.chrom.sizes.backgroundTableTriNucl96.txt', 'delimiter', '\t', 'ReadVariableNames', true);

matrixSignatures = decomposedSignatures.finalSignatures.signatures; % 96x21
signatureNames = decomposedSignatures.finalSignatures.signaturesNames; % 1x21
nSignatures = length(signatureNames);

signatureNamesTwice = [strrep(signatureNames, 'Signature', 'S'), strrep(signatureNames, 'Signature', 'S')];
% signatureNamesTwice = [strcat(strrep(signatureNames, 'Signature', 'S'), ' LD'), strcat(strrep(signatureNames, 'Signature', 'S'), ' LG')];

matrixForRegression = [decomposedSignatures.finalSignatures.splitSignaturesA, decomposedSignatures.finalSignatures.splitSignaturesB;
    decomposedSignatures.finalSignatures.splitSignaturesB, decomposedSignatures.finalSignatures.splitSignaturesA];
%%
scalingThresholdErrorIncrease = 1.005;
maxPattern = 96;
fprintf('Computing signature exposures in %s...\n', strjoin(dataFields.simpleTypes));
printInfo = false;
for simpleType = dataFields.simpleTypes(1)
    simpleType = simpleType{1};
    if (isfield(structAllSamples, simpleType) && ~isempty(structAllSamples.(simpleType)))
        signatureExposures.type1 = NaN*ones(nSignatures, nTotalSamples);
        signatureExposures.type2 = NaN*ones(nSignatures, nTotalSamples);
        for iSample = 1798%1753%1692%:nTotalSamples % 1680        1683        1692        1749        1753        1794        1798
            mfVectorForRegression = [...
                full(structAllSamples.(simpleType).mfType1(iSample,:)').*tableGenomeTrinucleotides.genomeComputed; ...
                full(structAllSamples.(simpleType).mfType2(iSample,:)').*tableGenomeTrinucleotides.genomeComputed];
            %currentExposures = lsqnonneg(matrixForRegression, mfVectorForRegression); %,residual,exitflag,output,lambda
            currentExposures = runRegressionToExposures(matrixForRegression, mfVectorForRegression, nSignatures, scalingThresholdErrorIncrease, printInfo, iSample);
            
            signatureExposures.type1(:, iSample) = currentExposures(1:nSignatures);
            signatureExposures.type2(:, iSample) = currentExposures(nSignatures+(1:nSignatures));
            fig = createMaximisedFigure(7); subplot(2,1,1); 
            plot_signature(mfVectorForRegression/100, decomposedSignatures.subtypes.strandSpec, decomposedSignatures.types.strandSpec, true, 'normal', ...
                strrep(sprintf('%d: %s | %s', iSample, replData.tableAllSamples.cancerType{iSample}, replData.tableAllSamples.sampleName{iSample}), '_', ' '),...
                'genome-scaled # mutations', true, 14, 'Helvetica', max(mfVectorForRegression), 1.2);
            subplot(2,1,2); bar(currentExposures); text(find(currentExposures>0), currentExposures(currentExposures>0), signatureNamesTwice(currentExposures>0), 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center');
            
            %
            nPerturbations = 100;
            mfVectorForRegressionPerturbed = repmat(mfVectorForRegression, 1, nPerturbations);
            for iType = 1:length(mfVectorForRegression)
                mfVectorForRegressionPerturbed(iType,:) = mfVectorForRegression(iType) + randn(1,nPerturbations)*mfVectorForRegression(iType)*0.05; % sd: 5% of the original value
            end
            currentExposuresPerturbed = repmat(NaN*currentExposures, 1, nPerturbations);
            for iPerturbation = 1:nPerturbations
                %currentExposuresPerturbed(:,iPerturbation) = lsqnonneg(matrixForRegression, mfVectorForRegressionPerturbed(:,iPerturbation)); %,residual,exitflag,output,lambda
                currentExposuresPerturbed(:,iPerturbation) = runRegressionToExposures(matrixForRegression, mfVectorForRegressionPerturbed(:,iPerturbation), nSignatures, scalingThresholdErrorIncrease, printInfo, iSample);
            end
            %
            fig = createMaximisedFigure(8); nR = 2; nC = 2; iS = 1; maxMut = max([mfVectorForRegression; nanmean(mfVectorForRegressionPerturbed, 2)]);
            subplot(nR,nC,iS); iS=iS+1; plot_signature(mfVectorForRegression/100, decomposedSignatures.subtypes.strandSpec, decomposedSignatures.types.strandSpec, true, 'normal', ...
                strrep(sprintf('%d: %s | %s', iSample, replData.tableAllSamples.cancerType{iSample}, replData.tableAllSamples.sampleName{iSample}), '_', ' '),...
                'genome-scaled # mutations', true, 14, 'Helvetica', maxMut, 1.2); ylim([0, maxMut*1.02]); grid on;
            subplot(nR,nC,iS); iS=iS+1; tmp = currentExposures(currentExposures>0); bar(tmp); 
            text(find(tmp>0), tmp(tmp>0), signatureNamesTwice(currentExposures>0), 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center'); grid on;
            
            subplot(nR,nC,iS); iS=iS+1; plot_signature(nanmean(mfVectorForRegressionPerturbed, 2)/100, decomposedSignatures.subtypes.strandSpec, decomposedSignatures.types.strandSpec, true, 'normal', ...
                strrep(sprintf('%d: %s | %s', iSample, replData.tableAllSamples.cancerType{iSample}, replData.tableAllSamples.sampleName{iSample}), '_', ' '),...
                'genome-scaled # mutations', true, 14, 'Helvetica', maxMut, 1.2); ylim([0, maxMut*1.02]); grid on;
            subplot(nR,nC,iS); iS=iS+1; maxValue = nanmax(currentExposuresPerturbed,[],2); tmp = nanmean(currentExposuresPerturbed, 2)./currentExposures; tmp=tmp(maxValue>0); tmp(isinf(tmp)) = 1; 
            boxplot(currentExposuresPerturbed(maxValue>0,:)'./currentExposures(maxValue>0)'); text(find(tmp>0), tmp(tmp>0), signatureNamesTwice(maxValue>0), 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'center'); 
            hold on; xLimVal=get(gca,'XLim'); plot(xLimVal, 0.9*[1,1], '-', xLimVal, 1.1*[1,1], '-'); ylim([0.5, 1.5]); grid on;
            %
        end
        signatureExposures.type1Minustype2 = (signatureExposures.type1 - signatureExposures.type2);
        signatureExposures.meanType1Type2 = (signatureExposures.type1 + signatureExposures.type2);
        mySaveAs(fig, imagesPath, ['validation_', replData.tableAllSamples.sampleName{iSample}]);
    end
end